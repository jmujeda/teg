package internal

import (
	"fmt"
	"math/rand"

	"github.com/coder/websocket"
)

type UserManager interface {
	createGuestUser(conn *websocket.Conn) *User
	remove(user *User)
}

type userManager struct {
	users map[UserId]*User
}

func NewUserManager() *userManager {
	return &userManager{users: map[UserId]*User{}}
}

func (u *userManager) createGuestUser(conn *websocket.Conn) *User {
	user := &User{Id: UserId(fmt.Sprint(rand.Uint64())), conn: conn}
	_, ok := u.users[user.Id]
	if ok {
		panic("user override error")
	}
	u.users[user.Id] = user
	return user
}

func (u *userManager) remove(user *User) {
	delete(u.users, user.Id)
}
