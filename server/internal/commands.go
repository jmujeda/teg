package internal

import (
	"context"
	"errors"

	"github.com/codecat/go-libs/log"
	teg "gitlab.com/jmujeda/teg/teg"
)

type messageSender interface {
	sendResponse(ctx context.Context, err error) error
}

type roomMessageSender interface {
	sendResponse(ctx context.Context, room *Room, user *User, err error) error
	sendMessageToRoom(ctx context.Context, room *Room, err error) error
	sendUserLeft(ctx context.Context, user *User, room *Room, err error) error
	sendMessageToOpponents(ctx context.Context, user *User, room *Room, err error) error
	sendLoadUserGames(ctx context.Context, user *User, games map[teg.GameId]teg.UserGame)
}

func createAccount(ctx context.Context, user *User, server ServerManager, loginData teg.AccountData, messager messageSender) error {
	err := server.createAccount(loginData)
	if err == nil {
		server.loginUser(user, loginData)
	}
	messager.sendResponse(ctx, err)
	return err
}

func loginUser(ctx context.Context, user *User, server ServerManager, loginData teg.AccountData, messager messageSender) error {
	err := server.loginUser(user, loginData)
	if err != nil {
		if err == errUserNotFound {
			return createAccount(ctx, user, server, loginData, messager)
		}
		return err
	}
	messager.sendResponse(ctx, err)
	return nil
}

func createRoom(ctx context.Context, user *User, server ServerManager, startMenuData teg.AccountData, messager roomMessageSender) error {
	room := server.createRoom(startMenuData.Id, startMenuData.Password)
	err := server.addUserToRoom(user, room)
	if err != nil {
		return err
	}
	log.Info("User added to room: %s", user.RoomId)
	messager.sendMessageToRoom(ctx, room, err)
	return nil
}

func joinRoom(ctx context.Context, user *User, server ServerManager, startMenuData teg.AccountData, messager roomMessageSender) error {
	room, err := server.getRoomByName(startMenuData.Id)
	if err != nil {
		return err
	}
	if startMenuData.Password != room.Password {
		return errors.New("password incorrect")
	}
	err = server.addUserToRoom(user, room)
	if err != nil {
		return err
	}
	log.Info("User added to room: %s", user.RoomId)
	messager.sendMessageToRoom(ctx, room, err)

	return nil
}

func loadGame(ctx context.Context, user *User, server ServerManager, startMenuData teg.AccountData, messager roomMessageSender) error {
	var err error
	room := server.loadGame(startMenuData.Id)
	if room == nil {
		err = errors.New("unable to load game")
	} else {
		err = server.addUserToRoom(user, room)
		for _, player := range room.Game.Players.GetAsArray() {
			if player.Id == string(user.Id) {
				user.Color = player.Color
			}
		}
	}
	if err == nil {
		messager.sendMessageToOpponents(ctx, user, room, err)
	}
	messager.sendResponse(ctx, room, user, err)
	return nil
}

func listGames(ctx context.Context, user *User, server ServerManager, startMenuData teg.AccountData, messager roomMessageSender) error {
	messager.sendLoadUserGames(ctx, user, server.getUserGames(user.Id))
	return nil
}

func leaveRoom(ctx context.Context, user *User, server ServerManager, startMenuData teg.AccountData, messager roomMessageSender) error {
	room, err := server.getRoom(user.RoomId)
	if err != nil {
		return err
	}
	err = server.removeUserFromRoom(user, room)
	if err != nil {
		return err
	}
	err = messager.sendUserLeft(ctx, user, room, err)
	return err
}

func startGame(ctx context.Context, user *User, server ServerManager, startMenuData teg.AccountData, messager roomMessageSender) error {
	room, err := server.getRoom(user.RoomId)
	if err != nil {
		return err
	}
	if len(room.Users) < 2 {
		err = errors.New("can't start a game with one player")
	} else {
		room.Game = teg.SetUpGame(room.GetUserColors())
		//room.Game = teg.NewTestGame(room.GetUserColors())
		room.Game.Id = teg.GameId(room.Id)
		server.storeGame(room)
		server.storeGameUsers(room)
	}
	messager.sendMessageToRoom(ctx, room, err)

	return nil

}

func trade(ctx context.Context, user *User, room *Room, news teg.ServerMessage, messager messageSender) error {
	err := teg.TradeCards(room.Game, news.Room.Game.CardTrade)
	return messager.sendResponse(ctx, err)
}

func addArmy(ctx context.Context, user *User, room *Room, news teg.ServerMessage, messager messageSender) error {
	err := teg.AddPlayerArmy(user.Color, room.Game, news.Room.Game.Countries)
	return messager.sendResponse(ctx, err)
}

func attack(ctx context.Context, user *User, room *Room, news teg.ServerMessage, messager messageSender) error {
	err := teg.SolveAttackIntent(room.Game, news.Room.Game.AttackIntent, teg.CommonDiceThrow)
	return messager.sendResponse(ctx, err)
}

func conquestRegroup(ctx context.Context, user *User, room *Room, news teg.ServerMessage, messager messageSender) error {
	err := teg.MoveArmyOnConquest(room.Game, news.Room.Game.RegroupMove)
	return messager.sendResponse(ctx, err)
}

func regroup(ctx context.Context, user *User, room *Room, news teg.ServerMessage, messager messageSender) error {
	err := teg.RegroupArmy(room.Game, news.Room.Game.RegroupMove)
	return messager.sendResponse(ctx, err)
}

func endTurn(ctx context.Context, user *User, room *Room, news teg.ServerMessage, messager messageSender) error {
	if room.Game.Turn.Action == teg.Regroup {
		teg.FinishRegroup(room.Game)
	}
	err := teg.EndTurn(room.Game)
	return messager.sendResponse(ctx, err)
}
