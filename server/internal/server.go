package internal

import (
	"context"
	"crypto/sha256"
	"encoding/base64"
	"errors"

	"github.com/coder/websocket"
	"github.com/gofor-little/env"
	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/jmujeda/teg/server/internal/debug"
	"gitlab.com/jmujeda/teg/teg"
)

var errWrongPassword = errors.New("contraseña incorrecta")

type ServerManager interface {
	createRoom(name string, password string) *Room
	getRoom(roomId RoomId) (*Room, error)
	getRoomByName(name string) (*Room, error)
	CreateGuestUser(Conn *websocket.Conn) *User
	DisconnectUser(user *User)
	createAccount(loginData teg.AccountData) error
	addUserToRoom(user *User, room *Room) error
	removeUserFromRoom(user *User, room *Room) error
	loginUser(user *User, data teg.AccountData) error
	storeGame(room *Room)
	loadGame(id string) *Room
	isLoggedIn(user *User, token string) bool
	Run(ctx context.Context, user *User, msg teg.ClientMessage) error
	SetConfig(config *teg.Config)
	storeGameUsers(room *Room)
	updateGameUsers(room *Room)
	getUserGames(id UserId) map[teg.GameId]teg.UserGame
}

type dbManager interface {
	getUser(name string) (userDocument, error)
	createUser(loginData teg.AccountData) error
	storeGame(room *Room)
	storeGameUsers(room *Room)
	updateGameUsers(room *Room)
	getUserGames(id UserId) map[teg.GameId]teg.UserGame
	loadGame(id teg.GameId) *teg.Game
}

type Server struct {
	roomManager RoomManager
	userManager UserManager
	db          dbManager
	salt        string
	jwt_key     string
	router      *router
	config      *teg.Config
}

func (s *Server) SetConfig(config *teg.Config) {
	s.config = config
}

func (s *Server) loadGame(id string) *Room {
	if s.config.ServerConfig.Debug {
		id = s.config.ServerConfig.LoadStateId
	}
	roomId := RoomId(id)
	room, err := s.roomManager.get(roomId)
	if err != nil {
		room = s.roomManager.restoreRoom(roomId)
	}
	//room.Game = s.config.ServerConfig.Game
	if s.config.ServerConfig.Debug {
		room.Game = debug.LoadGame(id)
	} else {
		room.Game = s.db.loadGame(teg.GameId(id))
	}

	return room
}

func (s *Server) createRoom(name string, password string) *Room {
	return s.roomManager.createRoom(name, password)
}

func (s *Server) CreateGuestUser(conn *websocket.Conn) *User {
	user := s.userManager.createGuestUser(conn)
	return user
}

func (s *Server) getRoom(roomId RoomId) (*Room, error) {
	return s.roomManager.get(roomId)
}

func (s *Server) addUserToRoom(user *User, room *Room) error {
	if room == nil {
		return errors.New("unable to add user to room")
	}
	err := room.Add(user)
	if err != nil {
		return err
	}
	err = user.joinRoom(room)
	if err != nil {
		return err
	}
	return nil
}

func (s *Server) removeUserFromRoom(user *User, room *Room) error {
	s.roomManager.remove(user)
	err := user.leaveRoom(room)
	if err != nil {
		return err
	}
	return nil
}

func (s *Server) getRoomByName(name string) (*Room, error) {
	return s.roomManager.getByName(name)
}

func CreateServer() ServerManager {
	if err := env.Load(".env"); err != nil {
		panic(err)
	}
	uri := env.Get("MONGODB", "")
	salt := env.Get("SALT", "")
	jwt_key := env.Get("JWT_KEY", "")
	return &Server{roomManager: NewRoomManager(), userManager: NewUserManager(), db: newMongoDbConnection(uri), salt: salt, jwt_key: jwt_key, router: NewRouter()}
}

func (s *Server) DisconnectUser(user *User) {
	if user != nil {
		s.roomManager.handleDisconnect(user)
		s.userManager.remove(user)
	}
}

func (s *Server) createAccount(loginData teg.AccountData) error {
	loginData.Password = s.encrypt(loginData.Password)
	if loginData.PlayerName == "" {
		loginData.PlayerName = loginData.Id
	}
	err := s.db.createUser(loginData)
	if err != nil {
		return err
	}
	return nil
}

func (s *Server) loginUser(user *User, account teg.AccountData) error {
	userDocument, err := s.db.getUser(account.Id)
	if err != nil {
		println("error getting user: ", err.Error())
		return err
	}
	if userDocument.Password != s.encrypt(account.Password) {
		return errWrongPassword
	}
	err = user.login(userDocument)
	if err != nil {
		println("error on user login: ", err.Error())
	}
	token, err := s.getUserJwt(user)
	if err != nil {
		println("error generating token: ", err.Error())
	}
	user.Token = token
	return err
}

func (s *Server) getUserJwt(user *User) (string, error) {
	if user.Id == "" {
		return "", errors.New("unable to generate jwt for anonymous user")
	}
	jwt := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": user.Id,
	})
	tokenString, err := jwt.SignedString([]byte(s.jwt_key))
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func (s *Server) storeGame(room *Room) {
	s.db.storeGame(room)
}

func (s *Server) storeGameUsers(room *Room) {
	s.db.storeGameUsers(room)
}

func (s *Server) updateGameUsers(room *Room) {
	s.db.updateGameUsers(room)
}

func (s *Server) getUserGames(id UserId) map[teg.GameId]teg.UserGame {
	return s.db.getUserGames(id)
}

func (s *Server) encrypt(text string) string {
	text += s.salt
	return encrypt([]byte(text))
}

func encrypt(bytes []byte) string {
	h := sha256.New()
	h.Write(bytes)
	sha256.New224()
	return base64.URLEncoding.EncodeToString(h.Sum([]byte{}))
}

func (s *Server) isLoggedIn(user *User, tokenString string) bool {

	userJwt, err := s.getUserJwt(user)

	if err == nil && userJwt == tokenString {
		return true
	}
	// token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
	// 	return s.jwt_key, nil
	// }, jwt.WithValidMethods([]string{jwt.SigningMethodHS256.Name}))
	// if err != nil {
	// 	return false
	// }

	// var userClaim string
	// if claims, ok := token.Claims.(jwt.MapClaims); ok {
	// 	userClaim = claims["user"].(string)
	// }

	return false
}

func (s *Server) Run(ctx context.Context, user *User, msg teg.ClientMessage) error {
	controller, err := s.router.GetController(user, s, msg)
	if err != nil {
		return err
	}
	err = controller.Run(ctx)
	return err
}
