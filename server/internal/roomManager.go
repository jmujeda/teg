package internal

import (
	"fmt"

	"github.com/codecat/go-libs/log"
	"github.com/google/uuid"
)

type RoomManager interface {
	createRoom(name string, password string) *Room
	get(roomId RoomId) (*Room, error)
	getByName(name string) (*Room, error)
	remove(user *User)
	handleDisconnect(user *User)
	restoreRoom(id RoomId) *Room
}

type roomStore struct {
	rooms map[RoomId]*Room
}

func NewRoomManager() *roomStore {
	return &roomStore{rooms: map[RoomId]*Room{}}
}

func (r *roomStore) restoreRoom(id RoomId) *Room {
	room := NewRoom(id, "", "")
	r.add(room)
	return room
}

func (r *roomStore) createRoom(name string, password string) *Room {
	roomId := r.createRoomId()
	room := NewRoom(roomId, name, password)
	r.add(room)
	return room
}

func (r roomStore) createRoomId() RoomId {
	id := RoomId(uuid.New().String())
	return id
}

func (r *roomStore) get(roomId RoomId) (*Room, error) {
	room := r.rooms[roomId]
	if room != nil {
		return room, nil
	}
	return room, fmt.Errorf("missing room Id: %s", roomId)
}

func (r *roomStore) getByName(name string) (*Room, error) {
	for _, room := range r.rooms {
		if room.Name == name {
			return room, nil
		}
	}
	return nil, fmt.Errorf("missing room Id: %s", name)
}

func (r *roomStore) add(room *Room) {
	_, ok := r.rooms[room.Id]
	if ok {
		panic("room override error")
	}
	r.rooms[room.Id] = room
}

func (r *roomStore) remove(user *User) {
	if user.RoomId != "" {
		room, ok := r.rooms[user.RoomId]
		if ok {
			room.Remove(user.Id)
			if room.isEmpty() {
				r.closeRoom(room)
			}
		}
		user.RoomId = ""
	}
}

func (r *roomStore) handleDisconnect(disconnectedUser *User) {
	room, err := r.get(disconnectedUser.RoomId)
	if err != nil {
		log.Error("user room doesn't exist for disconnected user %s", disconnectedUser.Id)
	} else {
		for _, user := range room.Users {
			if user.isOnline() {
				return
			}
		}
		r.closeRoom(room)
	}

}

func (r *roomStore) closeRoom(room *Room) {
	for _, user := range room.Users {
		if user.RoomId == room.Id {
			log.Debug("should not have users in room to be closed")
			user.RoomId = ""
		}
	}
	delete(r.rooms, room.Id)
}
