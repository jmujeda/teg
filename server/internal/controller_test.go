package internal

import (
	"context"
	"testing"

	teg "gitlab.com/jmujeda/teg/teg"
)

type userControllerTestCase struct {
	command     userCommand
	user        *User
	accountData teg.AccountData
	action      teg.Action
	expectError bool
	failMessage string
}

func Test_userController(t *testing.T) {
	testCases := []userControllerTestCase{
		{
			createAccount,
			&User{},
			teg.AccountData{},
			teg.CreateAccount,
			true,
			"should return error on empty user or password",
		},
		{
			createAccount,
			&User{},
			teg.AccountData{Id: "somebody"},
			teg.CreateAccount,
			true,
			"should return error on empty user or password",
		},
		{
			createAccount,
			&User{},
			teg.AccountData{Password: "password"},
			teg.CreateAccount,
			true,
			"should return error on empty user or password",
		},
	}

	for _, testCase := range testCases {
		userController := &userController{
			testCase.command,
			testCase.user,
			testCase.accountData,
			testCase.action,
			&userMessager{},
			&Server{},
			teg.ServerMessage{},
		}

		err := userController.Run(context.TODO())
		if (testCase.expectError && err == nil) || (!testCase.expectError && err != nil) {
			t.Error(testCase.failMessage)
		}

	}
}
