package internal

import (
	"errors"
	"slices"

	teg "gitlab.com/jmujeda/teg/teg"
	"golang.org/x/exp/maps"
)

type RoomId string
type UserId string

type Room struct {
	Id              RoomId
	Name            string
	Password        string
	Users           map[UserId]*User
	Game            *teg.Game
	availableColors []teg.Color
}

func (r *Room) GetUserIds() []UserId {
	userIds := []UserId{}
	for userId := range r.Users {
		userIds = append(userIds, userId)
	}
	return userIds
}

func NewRoom(roomId RoomId, name string, password string) *Room {
	room := &Room{Id: roomId, Name: name, Password: password, Users: map[UserId]*User{}, availableColors: teg.Colors}
	return room
}

func NewEmptyRoom() *Room {
	return &Room{
		Users: map[UserId]*User{},
		Game:  teg.NewEmptyGame(),
	}
}

func (r *Room) Add(user *User) error {
	if len(r.Users) > 5 {
		return errors.New("room is full")
	}
	if slices.Contains(maps.Keys(r.Users), user.Id) {
		return errors.New("user already in room")
	}
	if r.Game != nil && r.Game.Status != teg.Creating && !r.userIsPlayer(user) {
		return errors.New("user is not a player of this game")
	}
	r.Users[user.Id] = user
	return nil
}

func (r *Room) userIsPlayer(user *User) bool {
	if r.Game != nil {
		for _, player := range r.Game.Players.GetAsArray() {
			if UserId(player.Id) == user.Id {
				return true
			}
		}
	}
	return false
}

func (r *Room) Remove(userId UserId) error {
	user, ok := r.Users[userId]
	if !ok {
		return errors.New("user not in room")
	}
	r.availableColors = append(r.availableColors, user.Color)
	delete(r.Users, userId)
	return nil
}

func (r *Room) GetUserColors() map[string]teg.Color {
	colors := map[string]teg.Color{}
	for _, user := range r.Users {
		user.Color = r.availableColors[0]
		r.availableColors = r.availableColors[1:]
		colors[string(user.Id)] = user.Color
	}
	return colors
}

func (r *Room) Serialize(roommate User) teg.RoomMessage {
	message := teg.RoomMessage{
		Name:     r.Name,
		Password: r.Password,
	}

	message.Users = []teg.UserMessage{}

	if r.Game != nil {
		message.Game = r.Game.Serialize(roommate.Color)
	}

	for _, roommate := range r.Users {
		message.Users = append(message.Users, teg.UserMessage{Name: roommate.Name, Color: roommate.Color})
	}

	return message
}

func (r *Room) isEmpty() bool {
	return len(r.Users) == 0
}
