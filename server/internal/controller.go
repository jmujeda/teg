package internal

import (
	"context"
	"errors"
	"fmt"

	"github.com/codecat/go-libs/log"
	"github.com/coder/websocket/wsjson"
	teg "gitlab.com/jmujeda/teg/teg"
)

type userCommand func(ctx context.Context, user *User, server ServerManager, loginData teg.AccountData, messager messageSender) error
type roomCommand func(ctx context.Context, user *User, server ServerManager, startMenuData teg.AccountData, messager roomMessageSender) error
type gameCommand func(ctx context.Context, user *User, room *Room, data teg.ServerMessage, messager messageSender) error
type controllerFactory func(user *User, server ServerManager, news teg.ClientMessage) (Controller, error)

type Controller interface {
	Run(ctx context.Context) error
}

type roomController struct {
	command  roomCommand
	user     *User
	server   ServerManager
	news     teg.ServerMessage
	messager roomMessageSender
	action   teg.Action
}

type gameController struct {
	command  gameCommand
	user     *User
	room     *Room
	news     teg.ServerMessage
	messager messageSender
	action   teg.Action
	server   ServerManager
}

type userController struct {
	command     userCommand
	user        *User
	accountData teg.AccountData
	action      teg.Action
	messager    messageSender
	server      ServerManager
	news        teg.ServerMessage
}

func (c *userController) Run(ctx context.Context) error {
	err := c.validateCommand()
	if err == nil {
		return c.command(ctx, c.user, c.server, c.news.StartMenuData, c.messager)
	}
	return err
}

func (u *userController) validateCommand() error {
	if u.action == teg.CreateAccount && (u.accountData.Id == "" || u.accountData.Password == "") {
		return fmt.Errorf("complete account name and password")
	}
	return nil
}

func (c *roomController) Run(ctx context.Context) error {
	err := c.validateCommand()
	if err == nil {
		return c.command(ctx, c.user, c.server, c.news.StartMenuData, c.messager)
	}
	return err
}

func (c *roomController) validateCommand() error {
	if c.action == teg.LeaveRoom || c.action == teg.StartGame {
		if c.user.RoomId == "" {
			return fmt.Errorf("user %s doesn't belong to any room", c.user.Id)
		}
	} else if c.action == teg.JoinRoom || c.action == teg.CreateRoom {
		if c.news.StartMenuData.Id == "" || c.news.StartMenuData.Password == "" {
			return fmt.Errorf("missing room fields")
		}
	}
	return nil
}

func (c *gameController) Run(ctx context.Context) error {
	log.Debug("Game controller run with news")
	teg.JsonPrint(c.news)
	err := c.validateCommand()
	if err == nil {
		c.room.Game.SetupTurn()
		err = c.command(ctx, c.user, c.room, c.news, c.messager)
		if err == nil {
			c.server.storeGame(c.room)
			if c.room.Game.Status == teg.Finished {
				c.server.updateGameUsers(c.room)
			}
		}
	}
	return err
}

func (c *gameController) validateCommand() error {

	if c.user.Color != c.room.Game.TurnOwner {
		return fmt.Errorf("user with color %s cant play when turnowner is %s", c.user.Color, c.room.Game.TurnOwner)
	}

	if c.action == teg.EndTurnAction {
		ok, err := c.room.Game.CanEndTurn()
		if !ok {
			return errors.New(err.Error())
		}
	} else {
		if c.room.Game.Turn.Action != c.action && !(c.room.Game.Turn.Action == teg.AddArmy && c.action == teg.Trade) {
			return errors.New("shouldn't add army when turn != " + string(c.action))
		}
	}

	return nil
}

func newRoomController(user *User, server ServerManager, news teg.ClientMessage) (Controller, error) {
	if !server.isLoggedIn(user, news.News.Token) {
		return nil, fmt.Errorf("user %s not logged in trying to execute %s", user.Id, news.Action)
	}
	cmd := getRoomCommand(news.Action)
	if cmd == nil {
		return nil, fmt.Errorf("command not found for action %s", news.Action)
	}

	return &roomController{cmd, user, server, news.News, &roomMessager{user, news.Action}, news.Action}, nil
}

func newGameController(user *User, server ServerManager, news teg.ClientMessage) (Controller, error) {
	if !server.isLoggedIn(user, news.News.Token) {
		return nil, fmt.Errorf("user %s not logged in trying to execute %s", user.Id, news.Action)
	}
	room, err := server.getRoom(user.RoomId)
	if err != nil {
		return nil, err
	}
	cmd := getGameCommand(news.Action)
	if cmd == nil {
		return nil, fmt.Errorf("command not found for action %s", news.Action)
	}

	return &gameController{cmd, user, room, news.News, &gameMessager{user, room, news.Action}, news.Action, server}, nil
}

func newUserController(user *User, server ServerManager, news teg.ClientMessage) (Controller, error) {
	cmd := getUserCommand(news.Action)
	if cmd == nil {
		return nil, fmt.Errorf("command not found for action %s", news.Action)
	}
	println("new user controller action: ", news.Action)
	return &userController{cmd, user, news.News.StartMenuData, news.Action, &userMessager{user, news.Action}, server, news.News}, nil
}

func getUserCommand(action teg.Action) userCommand {
	switch action {
	case teg.CreateAccount:
		return createAccount
	case teg.LoginAccount:
		return loginUser
	}
	return nil
}

func getRoomCommand(action teg.Action) roomCommand {
	switch action {
	case teg.LeaveRoom:
		return leaveRoom
	case teg.StartGame:
		return startGame
	case teg.CreateRoom:
		return createRoom
	case teg.JoinRoom:
		return joinRoom
	case teg.LoadGame:
		return loadGame
	case teg.ListGames:
		return listGames
	}
	return nil
}

func getGameCommand(action teg.Action) gameCommand {
	log.Debug("Game command for action %s", string(action))
	switch action {
	case teg.AddArmy:
		return addArmy
	case teg.Add5Army:
		return addArmy
	case teg.Add3Army:
		return addArmy
	case teg.Attack:
		return attack
	case teg.Regroup:
		return regroup
	case teg.ConquestRegroup:
		return conquestRegroup
	case teg.DrawCard:
		return regroup
	case teg.Trade:
		return trade
	case teg.EndTurnAction:
		return endTurn
	}

	return nil
}

type gameMessager struct {
	user   *User
	room   *Room
	action teg.Action
}

type roomMessager struct {
	user   *User
	action teg.Action
}

type userMessager struct {
	user   *User
	action teg.Action
}

func (m *roomMessager) sendMessageToOpponents(ctx context.Context, player *User, room *Room, err error) error {
	for _, user := range room.Users {
		if user.Id != player.Id {
			if user.conn != nil {
				message := teg.ServerUpdate{Action: teg.UserJoined, News: teg.ServerMessage{Token: user.Token, Room: room.Serialize(*user)}}
				sendMessage(ctx, user, message)
			} else {
				log.Error("error getting peer for user %s", user.Id)
			}
		}
	}
	return nil
}

func (m *roomMessager) sendLoadUserGames(ctx context.Context, user *User, games map[teg.GameId]teg.UserGame) {
	message := teg.ServerUpdate{Action: teg.ListGames, News: teg.ServerMessage{UserGames: teg.UserGamesMessage{UserGames: games, Updated: true}}}
	sendMessage(ctx, m.user, message)
}

func (m *roomMessager) sendMessageToRoom(ctx context.Context, room *Room, err error) error {
	if err == nil {
		sendMessageToRoom(ctx, room, m.action)
	} else {
		message := teg.ServerUpdate{Action: teg.ErrorResponse, News: teg.ServerMessage{Token: m.user.Token, Room: room.Serialize(*m.user), Error: err.Error()}}
		sendMessage(ctx, m.user, message)
	}
	return nil
}

func (m *roomMessager) sendUserLeft(ctx context.Context, user *User, room *Room, err error) error {
	if err == nil {
		sendMessageToRoom(ctx, room, m.action)
		message := teg.ServerUpdate{Action: m.action, News: teg.ServerMessage{Token: m.user.Token}}
		err = sendMessage(ctx, user, message)
	} else {
		message := teg.ServerUpdate{Action: m.action, News: teg.ServerMessage{Token: m.user.Token, Error: err.Error()}}
		err = sendMessage(ctx, user, message)
	}
	return err
}

func sendMessage(ctx context.Context, user *User, message teg.ServerUpdate) error {

	teg.JsonPrint(message)
	if user != nil {
		err := wsjson.Write(ctx, user.conn, message)

		if err != nil {
			log.Error("error sending message: %s", err.Error())
		}

		return err
	}
	return fmt.Errorf("user not available")
}

func sendMessageToRoom(ctx context.Context, room *Room, action teg.Action) {
	for _, user := range room.Users {
		if user.conn != nil {
			message := teg.ServerUpdate{Action: action, News: teg.ServerMessage{Token: user.Token, Room: room.Serialize(*user)}}
			sendMessage(ctx, user, message)
		} else {
			log.Error("error getting peer for user %s", user.Id)
		}
	}
}

func (m *roomMessager) sendResponse(ctx context.Context, room *Room, user *User, err error) error {
	if err == nil {
		message := teg.ServerUpdate{Action: teg.InitBoard, News: teg.ServerMessage{Token: m.user.Token, Room: room.Serialize(*user)}}
		err = sendMessage(ctx, m.user, message)
	} else {
		message := teg.ServerUpdate{Action: teg.ErrorResponse, News: teg.ServerMessage{Token: m.user.Token, Error: err.Error()}}
		err = sendMessage(ctx, m.user, message)
	}
	return err
}

func (m *gameMessager) sendResponse(ctx context.Context, err error) error {
	if err == nil {
		sendMessageToRoom(ctx, m.room, teg.UpdateBoard)
	} else {
		roommate := m.room.Users[m.user.Id]
		message := teg.ServerUpdate{Action: teg.UpdateBoard, News: teg.ServerMessage{Token: m.user.Token, Room: m.room.Serialize(*roommate), Error: err.Error()}}
		sendMessage(ctx, m.user, message)
	}
	return nil
}

func (u *userMessager) sendResponse(ctx context.Context, err error) error {
	message := teg.ServerUpdate{Action: u.action, News: teg.ServerMessage{Token: u.user.Token}}
	if err != nil {
		message.News.Error = err.Error()
	}
	sendMessage(ctx, u.user, message)
	return nil
}
