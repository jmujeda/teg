package internal

import (
	"errors"

	"gitlab.com/jmujeda/teg/teg"
)

type router struct {
	controllers map[teg.Action]controllerFactory
}

func (r *router) GetController(user *User, server ServerManager, message teg.ClientMessage) (Controller, error) {
	factory, ok := r.controllers[message.Action]
	if !ok {
		return nil, errors.New("cannot create controller from clientMessage action")
	}

	controller, err := factory(user, server, message)
	if err != nil {
		return nil, errors.New(err.Error())
	}
	return controller, nil

}

func NewRouter() *router {

	return &router{
		controllers: map[teg.Action]controllerFactory{
			teg.CreateRoom:      newRoomController,
			teg.JoinRoom:        newRoomController,
			teg.LeaveRoom:       newRoomController,
			teg.LoadGame:        newRoomController,
			teg.StartGame:       newRoomController,
			teg.ListGames:       newRoomController,
			teg.AddArmy:         newGameController,
			teg.Add5Army:        newGameController,
			teg.Add3Army:        newGameController,
			teg.Trade:           newGameController,
			teg.EndTurnAction:   newGameController,
			teg.Attack:          newGameController,
			teg.Regroup:         newGameController,
			teg.ConquestRegroup: newGameController,
			teg.DrawCard:        newGameController,
			teg.CreateAccount:   newUserController,
			teg.LoginAccount:    newUserController,
			// teg.Ping:            pingReceivedCommand,
			// teg.ClaimCountryCard: claimCountryCardCommand,
		},
	}
}
