package debug

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"gitlab.com/jmujeda/teg/teg"
)

type storedGames struct {
	Id   string
	Game *teg.Game
}

func (s *storedGames) UnmarshalJSON(bytes []byte) error {
	type cloneType storedGames
	clone := &cloneType{Game: teg.NewEmptyGame()}
	err := json.Unmarshal(bytes, clone)
	if err != nil {
		fmt.Println(err.Error())
	}
	s.Id = clone.Id
	s.Game = clone.Game
	return nil
}

func LoadGame(gameId string) *teg.Game {
	fmt.Println("loading game:", gameId)
	bytes, err := os.ReadFile("internal/debug/storedGames.json")
	games := []*storedGames{}
	if err != nil {
		fmt.Println(err.Error())
		log.Fatal(err.Error())
	}
	err = json.Unmarshal(bytes, &games)
	if err != nil {
		fmt.Println(err.Error())
		log.Fatal(err.Error())
	}
	for _, game := range games {
		if game.Id == gameId {
			return game.Game
		}
	}
	return nil
}
