package internal

import "testing"

func Test_NewRoomManager(t *testing.T) {
	roomStore := NewRoomManager()

	if roomStore == nil {
		t.Error("error creating roomStore")
	}
}

func Test_createRoom(t *testing.T) {
	name := "myRoom"
	pass := "myPass"
	roomStore := NewRoomManager()

	room := roomStore.createRoom(name, pass)

	if roomStore.rooms[room.Id] != room {
		t.Error("error creating room")
	}
	if room.Name != name || room.Password != pass {
		t.Error("error with room name/pass")
	}
}

func Test_get(t *testing.T) {
	name := "myRoom"
	pass := "myPass"
	roomStore := NewRoomManager()

	room := roomStore.createRoom(name, pass)

	getRoom, err := roomStore.get(room.Id)

	if err != nil {
		t.Error("error getting room from store")
	}

	if getRoom.Id != room.Id || getRoom.Name != room.Name {
		t.Error("wrong room retrieved from store")
	}
}

func Test_getByName(t *testing.T) {
	name := "myRoom"
	pass := "myPass"
	roomStore := NewRoomManager()

	room := roomStore.createRoom(name, pass)

	getRoom, err := roomStore.getByName(room.Name)

	if err != nil {
		t.Error("error getting room from store")
	}

	if getRoom.Id != room.Id || getRoom.Name != room.Name {
		t.Error("wrong room retrieved from store")
	}
}
