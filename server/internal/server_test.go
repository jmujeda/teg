package internal

import (
	"testing"

	teg "gitlab.com/jmujeda/teg/teg"
)

var validPasswordDocument = userDocument{
	Password: string(encrypt([]byte("valid"))),
	Id:       "id",
}

type dbManagerTester struct {
}

func (db *dbManagerTester) storeGame(room *Room) {
}

func (db *dbManagerTester) storeGameUsers(room *Room) {
}

func (db *dbManagerTester) updateGameUsers(room *Room) {
}

func (db *dbManagerTester) loadGame(id teg.GameId) *teg.Game {
	return nil
}

func (db *dbManagerTester) getUserGames(id UserId) map[teg.GameId]teg.UserGame {
	return map[teg.GameId]teg.UserGame{}
}

func (db *dbManagerTester) getUser(name string) (userDocument, error) {
	if name == "existentUser" {
		return validPasswordDocument, nil
	} else if name == "nonExistentUser" {
		return userDocument{}, errUserNotFound
	}
	return userDocument{}, nil
}

func (db *dbManagerTester) createUser(userData teg.AccountData) error {
	if userData.Id == "nonCreatedUser" {
		return errAccountCreationError
	}
	if userData.Id == "existentUser" {
		return errUserAlreadyExists
	}

	return nil
}

type createAccountTestCase struct {
	data teg.AccountData
	err  error
}

func Test_createAccount(t *testing.T) {

	existentUser := teg.AccountData{
		Id: "existentUser",
	}
	nonExistentUser := teg.AccountData{
		Id:       "nonExistentUser",
		Password: "pass",
	}
	nonCreatedUser := teg.AccountData{
		Id: "nonCreatedUser",
	}

	testCases := []createAccountTestCase{
		{
			existentUser,
			errUserAlreadyExists,
		},
		{
			nonCreatedUser,
			errAccountCreationError,
		},
		{
			nonExistentUser,
			nil,
		},
	}

	server := &Server{db: &dbManagerTester{}}

	for i, testCase := range testCases {
		err := server.createAccount(testCase.data)
		if err != testCase.err {
			t.Errorf("errors don't match for test %d", i)
		}

	}

}

type loginUserTestCase struct {
	user            *User
	loginAttempt    teg.AccountData
	getUserResponse userDocument
	err             error
	isLoggedIn      bool
}

func Test_loginUser(t *testing.T) {
	user1 := &User{}
	user2 := &User{}
	user3 := &User{}
	noUserAccount := teg.AccountData{
		Id: "nonExistentUser",
	}
	wrongPasswordAccount := teg.AccountData{
		Id:       "existentUser",
		Password: "wrong",
	}
	validAccount := teg.AccountData{
		Id:       "existentUser",
		Password: "valid",
	}

	testCases := []loginUserTestCase{
		{user1, noUserAccount, userDocument{}, errUserNotFound, false},
		{user2, wrongPasswordAccount, validPasswordDocument, errWrongPassword, false},
		{user3, validAccount, validPasswordDocument, nil, true},
	}
	server := &Server{db: &dbManagerTester{}}

	for i, testCase := range testCases {
		err := server.loginUser(testCase.user, testCase.loginAttempt)

		if err != testCase.err {
			t.Errorf("wrong error result for test %d", i)
			t.Error(err.Error())
		}

		if err != nil {
			if server.isLoggedIn(testCase.user, testCase.user.Token) {
				t.Error("user should not be logged")
			}
		} else {
			if !server.isLoggedIn(testCase.user, testCase.user.Token) {
				t.Error("user should be logged")
			}
		}

	}
}
