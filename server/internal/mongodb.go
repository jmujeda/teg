package internal

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/codecat/go-libs/log"
	teg "gitlab.com/jmujeda/teg/teg"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var errUserNotFound = errors.New("el usuario no existe")
var errUserAlreadyExists = errors.New("el usuario ya existe")
var errAccountCreationError = errors.New("error creando el usuario")
var errDBConnection = errors.New("error conectando a la base de datos")

type gameDocument struct {
	Id   teg.GameId `bson:"_id"`
	Game *teg.Game
}

type userGameDocument struct {
	UserId  UserId `bson:"userId"`
	GameId  teg.GameId
	Status  teg.GameStatus
	Players map[teg.Color]string
	Date    time.Time
}

type userDocument struct {
	Id       string `bson:"_id"`
	Password string
	Name     string
}

type mongoDb struct {
	client *mongo.Client
}

func newMongoDbConnection(conn string) *mongoDb {
	serverAPI := options.ServerAPI(options.ServerAPIVersion1)
	opts := options.Client().ApplyURI(conn).SetServerAPIOptions(serverAPI)
	// Create a new client and connect to the server
	client, err := mongo.Connect(context.TODO(), opts)
	if err != nil {
		panic(err)
	}
	// defer func() {
	// 	if err = client.Disconnect(context.TODO()); err != nil {
	// 		panic(err)
	// 	}
	// }()
	// Send a ping to confirm a successful connection
	var result bson.M
	if err := client.Database("admin").RunCommand(context.TODO(), bson.D{{"ping", 1}}).Decode(&result); err != nil {
		panic(err)
	}
	fmt.Println("Pinged your deployment. You successfully connected to MongoDB!")
	return &mongoDb{client}
}

func (db *mongoDb) getUser(name string) (userDocument, error) {
	filter := bson.D{{Key: "_id", Value: name}}
	coll := db.client.Database("teg").Collection("users")
	res := coll.FindOne(context.TODO(), filter)
	account := userDocument{}
	err := res.Decode(&account)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return account, errUserNotFound
		}
		return account, returnCustomError(errDBConnection, err)
	}
	return account, nil
}

func (db *mongoDb) createUser(loginData teg.AccountData) error {

	userAccount := userDocument{Id: loginData.Id, Password: loginData.Password, Name: loginData.PlayerName}
	fmt.Print(userAccount)
	_, err := db.getUser(userAccount.Id)
	if err == nil {
		return errUserAlreadyExists
	}

	if err != errUserNotFound {
		return returnCustomError(errAccountCreationError, err)
	}

	coll := db.client.Database("teg").Collection("users")
	res, err := coll.InsertOne(context.TODO(), userAccount)
	if err != nil {
		return returnCustomError(errAccountCreationError, err)
	}
	if res.InsertedID != userAccount.Id || res.InsertedID == nil {
		return returnCustomError(errAccountCreationError, errors.New("wrong user id"))
	}
	return nil

}

func (db *mongoDb) storeGame(room *Room) {
	models := []mongo.WriteModel{}
	filter := bson.D{{Key: "_id", Value: room.Game.Id}}
	replacement := createGameDocument(room)
	model := mongo.NewReplaceOneModel().SetFilter(filter).SetUpsert(true).SetReplacement(replacement)
	models = append(models, model)

	coll := db.client.Database("teg").Collection("games")
	results, err := coll.BulkWrite(context.TODO(), models)
	if err != nil {
		log.Error("%s", err.Error())
	} else {
		log.Debug("%d", results.UpsertedIDs)
	}
}

func (db *mongoDb) loadGame(id teg.GameId) *teg.Game {
	filter := bson.D{{Key: "_id", Value: id}}
	gameDoc := gameDocument{Game: teg.NewEmptyGame()}
	res := db.client.Database("teg").Collection("games").FindOne(context.Background(), filter)
	res.Decode(&gameDoc)
	teg.JsonPrint(gameDoc.Game)

	return gameDoc.Game
}

func (db *mongoDb) updateGameUsers(room *Room) {
	filter := bson.D{{Key: "GameId", Value: room.Game.Id}}
	update := bson.D{{Key: "$set", Value: bson.D{{Key: "Status", Value: room.Game.Status}}}}

	results, err := db.client.Database("teg").Collection("userGames").UpdateMany(context.Background(), filter, update)
	if err != nil {
		log.Error("%s", err.Error())
	} else {
		log.Debug("inserted user games: %d", results.UpsertedID)
	}
}

func (db *mongoDb) storeGameUsers(room *Room) {
	models := []mongo.WriteModel{}
	docs := createUserGameDocuments(room)
	for _, doc := range docs {
		model := mongo.NewInsertOneModel().SetDocument(doc)
		models = append(models, model)
	}
	coll := db.client.Database("teg").Collection("userGames")
	results, err := coll.BulkWrite(context.TODO(), models)
	if err != nil {
		log.Error("%s", err.Error())
	} else {
		log.Debug("inserted user games: %d", results.InsertedCount)
	}
}

func (db *mongoDb) getUserGames(id UserId) map[teg.GameId]teg.UserGame {
	userGames := map[teg.GameId]teg.UserGame{}
	ctx := context.Background()
	filter := bson.D{{Key: "userId", Value: string(id)}}
	sort := bson.D{{Key: "date", Value: 1}}
	results, err := db.client.Database("teg").Collection("userGames").Find(ctx, filter, options.Find().SetSort(sort))
	if err != nil {
		log.Error("%s", err.Error())
	} else {
		docs := []userGameDocument{}
		results.All(ctx, &docs)
		for _, doc := range docs {
			userGames[doc.GameId] = teg.UserGame{
				Id:      doc.GameId,
				Status:  doc.Status,
				Players: doc.Players,
				Date:    doc.Date.Local(),
			}
		}
	}
	return userGames
}

func createUserGameDocuments(room *Room) []userGameDocument {
	docs := []userGameDocument{}
	players := map[teg.Color]string{}
	for _, player := range room.Game.Players.GetAsArray() {
		players[player.Color] = player.Id
	}
	for _, player := range room.Game.Players.GetAsArray() {
		docs = append(docs, userGameDocument{UserId: UserId(player.Id), GameId: room.Game.Id, Status: room.Game.Status, Players: players, Date: time.Now()})
	}
	return docs
}

func createGameDocument(room *Room) gameDocument {
	return gameDocument{room.Game.Id, room.Game}
}

func returnCustomError(toReturn error, toLog error) error {
	log.Debug("%s", toLog.Error())
	return toReturn
}
