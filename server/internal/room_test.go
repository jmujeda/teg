package internal

import (
	"encoding/json"
	"testing"

	"github.com/MakeNowJust/heredoc/v2"
	teg "gitlab.com/jmujeda/teg/teg"
)

func Test_NewRoom(t *testing.T) {
	id := RoomId("123")
	name := "myRoom"
	pass := "pass"
	room := NewRoom(id, name, pass)
	if room == nil {
		t.Error("error creating new room")
	} else {
		if room.Id != id {
			t.Error("error creating new room")
		}
		if room.Password != pass {
			t.Error("error creating new room")
		}
		if room.Name != name {
			t.Error("error creating new room")
		}
	}
}

func Test_Add(t *testing.T) {
	room := NewRoom("1", "name", "pass")
	roommate1 := &User{Id: "1"}
	roommate2 := &User{Id: "2"}
	roommate3 := &User{Id: "3"}
	roommate4 := &User{Id: "4"}
	roommate5 := &User{Id: "5"}
	roommate6 := &User{Id: "6"}
	roommate7 := &User{Id: "7"}
	err := room.Add(roommate1)
	if err != nil {
		t.Error("error adding roommate to room")
	}
	err = room.Add(roommate1)
	if err == nil {
		t.Error("shouldn't add twice the same roommate")
	}
	room.Add(roommate2)
	room.Add(roommate3)
	room.Add(roommate4)
	room.Add(roommate5)
	room.Add(roommate6)
	err = room.Add(roommate7)
	if err == nil {
		t.Error("shouldn't add more than 6 roommates to a room")
	}

}

func Test_Remove(t *testing.T) {
	room := NewRoom("1", "name", "pass")
	roommate1 := &User{Id: "1"}
	room.Add(roommate1)
	room.Remove(roommate1.Id)
	if len(room.Users) != 0 {
		t.Error("error removing roommate from room")
	}
}

func Test_GetRoommateList(t *testing.T) {
	room := NewRoom("1", "name", "pass")
	roommate1 := &User{Id: "1", Name: "roommate1"}
	room.Add(roommate1)
	roomMessage := room.Serialize(*roommate1)
	roommates := roomMessage.GetRoommatesList()
	if roommates[0] != roommate1.Name {
		t.Error("error retrieving roommate name from room.GetRoommateList")
	}

}

func Test_Serialize(t *testing.T) {
	userId1 := UserId("1")
	userId2 := UserId("2")
	roommate1 := &User{Id: userId1, Name: "player1"}
	roommate2 := &User{Id: userId2, Name: "player2"}

	//room := Room{Users: roommates}
	room := NewRoom("1", "", "")
	room.Add(roommate1)
	room.Add(roommate2)
	game := teg.NewGame(room.GetUserColors())
	game.Players.GetByColor(roommate1.Color).Cards = *teg.NewCountriesDeck()
	game.Players.GetByColor(roommate2.Color).Cards = *teg.NewCountriesDeck()

	room.Game = game

	roommate1MessagePlayers := room.Serialize(*roommate1).Game.Players

	for _, player := range roommate1MessagePlayers {
		if player.Color == roommate1.Color {
			for _, card := range player.Cards.Cards {
				if card.Country == "" || card.Logo == "" {
					t.Error("self cards shouldn't be masked")
				}
			}

			if player.Objective == "" {
				t.Error("self objective shouldn't be masked")
			}

		} else {

			for _, card := range player.Cards.Cards {
				if card.Country != "" || card.Logo != "" {
					t.Error("opponent cards should be masked")
				}
			}

			if player.Objective != "" {
				t.Error("opponent objective should be masked")
			}
		}
	}

}

func Test_UnmarshalRoom(t *testing.T) {
	jsonString := heredoc.Doc(`
	{
   "Id":"4e632517-aa70-4290-af79-a11b53fbc73c",
   "Game":{
      "Countries":[
         {
            "Army":6,
            "Owner":"negro",
            "Name":"Uruguay",
            "Continent":"america del sur"
         },
         {
            "Army":1,
            "Owner":"negro",
            "Name":"Argentina",
            "Continent":"america del sur"
         },
         {
            "Army":1,
            "Owner":"negro",
            "Name":"Chile",
            "Continent":"america del sur"
         },
         {
            "Army":1,
            "Owner":"amarillo",
            "Name":"Brasil",
            "Continent":"america del sur"
         },
         {
            "Army":6,
            "Owner":"amarillo",
            "Name":"Peru",
            "Continent":"america del sur"
         },
         {
            "Army":1,
            "Owner":"amarillo",
            "Name":"Colombia",
            "Continent":"america del sur"
         }
      ],
      "Players":[
         {
            "Cards":{
               "Cards":null
            },
            "Objective":{
               "Id":2
            },
            "Color":"negro",
            "Chips":0,
            "Candrawcard":false,
            "Cardtrades":0
         },
         {
            "Cards":{
               "Cards":null
            },
            "Objective":{
               "Id":1
            },
            "Color":"amarillo",
            "Chips":3,
            "Candrawcard":false,
            "Cardtrades":0
         }
      ],
      "step":10,
      "deck":{
         "cards":[
            {
               "country":"Argentina",
               "logo":"globe",
               "claimed":false
            },
            {
               "country":"Brasil",
               "logo":"ship",
               "claimed":false
            },
            {
               "country":"Chile",
               "logo":"globe",
               "claimed":false
            },
            {
               "country":"Uruguay",
               "logo":"globe",
               "claimed":false
            },
            {
               "country":"Colombia",
               "logo":"globe",
               "claimed":false
            },
            {
               "country":"Peru",
               "logo":"cannon",
               "claimed":false
            }
         ]
      },
      "discarddeck":{
         "cards":null
      },
      "regroupmove":{
         "origin":"",
         "destination":"",
         "count":0
      },
      "round":0,
      "restrictedregroupment":{
         "regroupments":{
            
         }
      },
      "kills":{
         
      },
      "turn":{
         "turnowner":"amarillo",
         "action":"Add3Army",
         "roundorder":[
            "amarillo",
            "negro"
         ]
      },
      "status":"playing",
      "attackintent":{
         "attacker":"",
         "defender":"",
         "attackerdice":null,
         "defenderdice":null,
         "conquest":false
      }
   }
}`)

	jsonBytes := []byte(jsonString)
	game := teg.NewEmptyGame()
	gameDoc := gameDocument{Game: game}
	err := json.Unmarshal(jsonBytes, &gameDoc)
	if err != nil {
		t.Error(err)
	}

	if gameDoc.Id != "4e632517-aa70-4290-af79-a11b53fbc73c" {
		t.Error("unmarshaled room id not matching")
	}

	blackPlayerChips := gameDoc.Game.Players.GetByColor(teg.Black).Chips
	if blackPlayerChips != 0 {
		t.Errorf("expected 0, got %d", blackPlayerChips)
	}

	yellowPlayerChips := gameDoc.Game.Players.GetByColor(teg.Yellow).Chips

	if yellowPlayerChips != 3 {
		t.Errorf("expected 3, got %d", yellowPlayerChips)
	}

	if gameDoc.Game.Step != 10 {
		t.Errorf("expected 10, got %d", gameDoc.Game.Step)
	}

}
