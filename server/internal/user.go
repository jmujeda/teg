package internal

import (
	"encoding/json"
	"fmt"

	"github.com/coder/websocket"
	teg "gitlab.com/jmujeda/teg/teg"
)

type User struct {
	Id     UserId
	RoomId RoomId
	Name   string
	Color  teg.Color
	Logged bool
	Token  string
	conn   *websocket.Conn
}

func (u *User) ToBytes() []byte {
	user := User{
		Id:     u.Id,
		RoomId: u.RoomId,
		Name:   u.Name,
		Color:  u.Color,
		Logged: u.Logged,
	}
	bytes, _ := json.Marshal(user)
	return bytes
}

func (u *User) joinRoom(room *Room) error {
	if room == nil {
		return fmt.Errorf("room not found")
	}
	u.RoomId = room.Id
	return nil
}

func (u *User) leaveRoom(room *Room) error {
	if room == nil {
		return fmt.Errorf("room not found")
	}
	u.RoomId = ""
	return nil
}

func (u *User) login(userDoc userDocument) error {
	u.Logged = true
	u.Id = UserId(userDoc.Id)
	u.Name = userDoc.Name
	if u.Name == "" {
		u.Name = string(u.Id)
	}
	return nil
}

func (u *User) isOnline() bool {
	return u.conn != nil
}
