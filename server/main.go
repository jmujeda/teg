package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/coder/websocket"
	"gitlab.com/jmujeda/teg/server/internal"
	"gitlab.com/jmujeda/teg/teg"
	"golang.org/x/time/rate"
)

type Connection struct {
	Conn *websocket.Conn
}

func main() {
	log.SetFlags(0)

	err := run()
	if err != nil {
		log.Fatal(err)
	}
}

// tegServer is the WebSocket echo server implementation.
// It ensures the client speaks the echo subprotocol and
// only allows one message every 100ms with a 10 message burst.
type tegServer struct {
	// logf controls where logs are sent.
	logf   func(f string, v ...interface{})
	server internal.ServerManager
}

func (s tegServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	test := true
	ctx := r.Context()
	config := teg.NewConfig(ctx, test)
	s.server.SetConfig(config)

	c, err := websocket.Accept(w, r, &websocket.AcceptOptions{
		OriginPatterns: []string{"localhost:8081"},
	})
	if err != nil {
		s.logf("%v", err)
		return
	}
	user := s.server.CreateGuestUser(c)

	defer c.CloseNow()

	l := rate.NewLimiter(rate.Every(time.Millisecond*100), 10)
	for {

		clientMessage, err := readMessages(ctx, c, l)
		println("message received:")
		teg.JsonPrint(clientMessage)
		// handle all types of disconnect
		closeStatus := websocket.CloseStatus(err)
		if closeStatus == websocket.StatusNormalClosure || closeStatus == websocket.StatusAbnormalClosure {
			s.server.DisconnectUser(user)
			fmt.Print(err.Error())
			return
		}
		if err != nil {
			s.logf(closeStatus.String())
			s.logf("failed to echo with %v: %v", r.RemoteAddr, err)
			return
		}

		err = s.server.Run(ctx, user, clientMessage)
		if err != nil {
			fmt.Print(err.Error())
		}
	}

}

func readMessages(ctx context.Context, c *websocket.Conn, l *rate.Limiter) (teg.ClientMessage, error) {
	ctx, cancel := context.WithTimeout(ctx, time.Minute*30)
	defer cancel()
	err := l.Wait(ctx)
	if err != nil {
		return teg.ClientMessage{}, err
	}

	_, data, err := c.Read(ctx)
	if err != nil {
		return teg.ClientMessage{}, err
	}
	clientMessage, err := teg.GetClientMessage(data)

	return clientMessage, err
}

// echo reads from the WebSocket connection and then writes
// the received message back to it.
// The entire function has 10s to complete.
// func echo(ctx context.Context, c *websocket.Conn, l *rate.Limiter) error {
// 	ctx, cancel := context.WithTimeout(ctx, time.Second*10)
// 	defer cancel()

// 	err := l.Wait(ctx)
// 	if err != nil {
// 		return err
// 	}

// 	typ, r, err := c.Reader(ctx)
// 	if err != nil {
// 		return err
// 	}

// 	w, err := c.Writer(ctx, typ)
// 	if err != nil {
// 		return err
// 	}

// 	_, err = io.Copy(w, r)
// 	if err != nil {
// 		return fmt.Errorf("failed to io.Copy: %w", err)
// 	}

// 	err = w.Close()
// 	return err
// }

// run starts a http.Server for the passed in address
// with all requests handled by echoServer.
func run() error {
	server := internal.CreateServer()
	if len(os.Args) < 2 {
		return errors.New("please provide an address to listen on as the first argument")
	}
	l, err := net.Listen("tcp", os.Args[1])
	if err != nil {
		return err
	}
	log.Printf("listening on ws://%v", l.Addr())

	s := &http.Server{
		Handler: tegServer{
			logf:   log.Printf,
			server: server,
		},
		ReadTimeout:  time.Second * 10,
		WriteTimeout: time.Second * 10,
	}
	errc := make(chan error, 1)
	go func() {
		errc <- s.Serve(l)
	}()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt)
	select {
	case err := <-errc:
		log.Printf("failed to serve: %v", err)
	case sig := <-sigs:
		log.Printf("terminating: %v", sig)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	return s.Shutdown(ctx)
}
