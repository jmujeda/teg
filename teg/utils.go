package teg

import (
	"encoding/json"
	"fmt"
	"math/rand/v2"
)

func Shuffle[T any](a []T) []T {
	for i := range a {
		j := rand.IntN(i + 1)
		a[i], a[j] = a[j], a[i]
	}
	return a
}

func JsonPrint(object any) {
	b, err := json.MarshalIndent(object, "", "  ")
	if err != nil {
		fmt.Println("error marshalling for printer")
	} else {
		fmt.Println(string(b))
	}
}

type log struct {
	debugging bool
}

type Logger interface {
	Debug(message any)
}

func NewLogger(debug bool) Logger {
	return &log{debug}
}

func (l log) Debug(object any) {
	if l.debugging {
		JsonPrint(object)
	}
}
