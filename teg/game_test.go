package teg

import (
	"encoding/json"
	"fmt"
	"strings"
	"testing"
)

func Test_Regroup(t *testing.T) {

	game := &Game{}
	game.RestrictedRegroupment = NewRegroupedArmy()
	game.Countries = newCountriesList()
	arg, _ := game.Countries.getByName("Argentina")
	bra, _ := game.Countries.getByName("Brasil")
	uru, _ := game.Countries.getByName("Uruguay")
	err := game.regroup(arg, bra)

	if err == nil {
		t.Error("shouldn't regroup if origin army is less than 2")
	}

	bra.Army = 1
	arg.Army = 2
	uru.Army = 1

	err = game.regroup(arg, bra)

	if err != nil {
		t.Error(err)
	} else {
		err = validateArmies(game, []*Country{arg, bra}, []int{1, 1}, []int{1, 2})
		if err != nil {
			t.Error(err.Error())
		}
	}

	err = game.regroup(bra, uru)
	if err == nil {
		t.Error("shouldn't regroup when army is foreign")
	}

	err = game.regroup(bra, arg)
	if err != nil {
		t.Error("should regroup if foreign army belongs to destination")
	}
}

func validateArmies(game *Game, countries []*Country, armies []int, totalArmy []int) error {
	for i, country := range countries {
		if country.Army != armies[i] {
			return fmt.Errorf("%s army should be %d, got %d", string(country.Name), armies[i], country.Army)
		}
	}

	for i, country := range countries {
		if army, _ := game.getArmy(country.Name); army != totalArmy[i] {
			return fmt.Errorf("%s army should be %d, got %d", string(country.Name), totalArmy[i], country.Army)
		}
	}

	return nil
}

func Test_GameHasFinished(t *testing.T) {
	game := NewGame(map[string]Color{"1": Black, "2": Yellow})
	turnOwner := game.Players.GetByColor(game.TurnOwner)
	turnOwner.Objective = NewCommonObjective()
	finished := isGameFinished(game)

	if finished {
		t.Error("should return false objective isn't fulfilled")
	}

	countries := &Countries{
		array:  []*Country{},
		byName: map[CountryName]*Country{},
	}
	for i := range 30 {
		arg := &Country{Name: CountryName("Argentina" + fmt.Sprint(i)), Owner: turnOwner.Color}
		countries.array = append(countries.array, arg)
		countries.byName[arg.Name] = arg
	}
	game.Countries = countries
	finished = isGameFinished(game)

	if !finished {
		t.Error("game should be finished")
	}

}

func Test_TwoPlayersGame(t *testing.T) {
	game := NewGame(map[string]Color{"1": Black, "2": Yellow})

	blackPlayer := game.Players.GetByColor(Black)
	yellowPlayer := game.Players.GetByColor(Yellow)
	if !strings.Contains(blackPlayer.Objective.getText(), string(yellowPlayer.Color)) {
		t.Error("destroy objective should have other player as target")
	}

	if !strings.Contains(yellowPlayer.Objective.getText(), string(blackPlayer.Color)) {
		t.Error("destroy objective should have other player as target")
	}
}

func Test_GameMarshallJSON(t *testing.T) {
	game := SetUpGame(map[string]Color{"1": Black, "2": Yellow})

	arg, _ := game.Countries.getByName("Argentina")
	arg.Army = 10
	bytes, err := json.Marshal(game)

	blackObjective := game.Players.GetByColor(Black).Objective.getId()
	if err != nil {
		t.Error("error marshalling game", err)
	}

	countries := Countries{}
	playerList := playerList{}
	game2 := &Game{
		Countries:             &countries,
		Players:               &playerList,
		RestrictedRegroupment: &RegroupedArmy{},
	}

	err = json.Unmarshal(bytes, game2)

	arg2, _ := game2.Countries.getByName("Argentina")

	if err != nil {
		t.Error("error unmarshalling json", err)
	}

	if game.TurnOwner != game2.TurnOwner {
		t.Error("turnowner error")
	}
	if game.Action != game2.Action {
		t.Error("Action error")
	}
	if game.RoundOrder[0] != game2.RoundOrder[0] {
		t.Error("RoundOrder error")
	}
	if arg2.Army != 10 {
		t.Error("Army error")
	}
	if game.Players.GetByColor(Black).Objective.getId() != blackObjective {
		t.Error("objective error")
	}
}
