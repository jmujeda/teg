package teg

import "testing"

func Test_AddPlayerArmy(t *testing.T) {

	game := SetUpGame(map[string]Color{"1": Black, "2": Yellow})
	turnOwner := game.TurnOwner
	arg, _ := game.Countries.getByName("Argentina")
	arg.Owner = turnOwner
	color := turnOwner

	validCountriesUpdate := game.Serialize(color).Countries
	country := validCountriesUpdate["Argentina"]
	country.Army += 5
	validCountriesUpdate["Argentina"] = country

	err := AddPlayerArmy(color, game, validCountriesUpdate)

	if err != nil {
		t.Error(err)
	} else {
		t.Log(game.Players.GetByColor(color).Chips)

		for _, country := range game.Countries.getByColor(color) {
			if country.Army != validCountriesUpdate[country.Name].Army {
				t.Error("invalid game state after addPlayerArmy")
			}
		}
		if game.Players.GetByColor(color).Chips > 0 {
			t.Error("player should have 0 chips left")
		}
	}

}

func Test_CommonDiceThrow(t *testing.T) {
	attackerArmy4 := 5
	defenderArmy3 := 3
	attackerArmy3 := 3

	result := CommonDiceThrow(attackerArmy4, true)
	if len(result) != 3 {
		t.Error("there should be 3 dice for army > 3")
	}

	prev := 6
	for _, x := range result {
		if x > prev {
			t.Error("dice result should be sorted in descending order")
		} else {
			x = prev
		}
	}

	result = CommonDiceThrow(defenderArmy3, false)
	if len(result) != 3 {
		t.Error("there should be 3 dice for defender with army = 3")
	}
	prev = 6
	for _, x := range result {
		if x > prev {
			t.Error("dice result should be sorted in descending order")
		} else {
			prev = x
		}
	}

	result = CommonDiceThrow(attackerArmy3, true)
	if len(result) != 2 {
		t.Error("there should be 2 dice for army > 2")
	}
	prev = 6
	for _, x := range result {
		if x > prev {
			t.Error("dice result should be sorted in descending order")
		} else {
			prev = x
		}
	}

}

func Test_UpdateArmies(t *testing.T) {

	attacker := &Country{Army: 5, Owner: Black}
	defender := &Country{Army: 5, Owner: Yellow}

	attackResult := AttackIntent{
		AttackerDice: []int{6, 4, 2},
		DefenderDice: []int{6, 4, 2},
	}

	updateArmies(attacker, defender, attackResult)

	if attacker.Army != 2 {
		t.Error("attacker should have lost 3 armies")
	}

	if defender.Army != 5 {
		t.Error("defender shouldn't lost any army")
	}

	attackResult = AttackIntent{
		AttackerDice: []int{6},
		DefenderDice: []int{1, 1},
	}

	updateArmies(attacker, defender, attackResult)

	if attacker.Army != 2 {
		t.Error("attacker should have lost 0 armies")
	}

	if defender.Army != 4 {
		t.Error("defender should lost 1 army")
	}

}

func Test_MoveArmyOnConquest(t *testing.T) {
	origin := CountryName("Argentina")
	destination := CountryName("Brasil")

	//game := &Game{}
	game := SetUpGame(map[string]Color{"1": Black, "2": Yellow})
	game.Countries = newCountriesList()
	game.Turn.Action = Attack
	arg, _ := game.Countries.getByName(origin)
	bra, _ := game.Countries.getByName(destination)
	arg.Owner = Black
	bra.Owner = Black
	regroupMove := RegroupMove{
		Origin: origin,
		Count:  4,
	}
	err := MoveArmyOnConquest(game, regroupMove)

	if err == nil {
		t.Error("should return error on count > 3")
	}
	regroupMove = RegroupMove{
		Origin: origin,
		Count:  0,
	}
	err = MoveArmyOnConquest(game, regroupMove)

	if err == nil {
		t.Error("should return error on count < 1")
	}
	regroupMove = RegroupMove{
		Origin: origin,
		Count:  3,
	}
	err = MoveArmyOnConquest(game, regroupMove)
	if err == nil {
		t.Error("should return error on destination.army != 1")
	}

	country, _ := game.Countries.getByName(origin)
	country.Army = 2
	regroupMove = RegroupMove{
		Origin:      origin,
		Count:       3,
		Destination: destination,
	}
	err = MoveArmyOnConquest(game, regroupMove)

	if err == nil {
		t.Error("should return error on count > country army")
	}

	regroupMove = RegroupMove{
		Origin:      origin,
		Count:       1,
		Destination: destination,
	}

	countryDest, _ := game.Countries.getByName(destination)
	country.Army = 2
	countryDest.Army = 0

	err = MoveArmyOnConquest(game, regroupMove)

	if err == nil {
		if countryDest.Army != 1 || country.Army != 1 {
			t.Error("shouldn't move armies")
		}
	} else {
		t.Error(err)
	}

	regroupMove = RegroupMove{
		Origin:      origin,
		Count:       2,
		Destination: destination,
	}

	country.Army = 4
	countryDest.Army = 0

	err = MoveArmyOnConquest(game, regroupMove)

	if err == nil {
		if countryDest.Army != 2 || country.Army != 2 {
			t.Error("should move correct amount of army")
		}
	} else {
		t.Error(err)
	}

}

func Test_SolveAttackIntent(t *testing.T) {
	origin := CountryName("Argentina")
	destination := CountryName("Brasil")
	roundOrder := []Color{Black, Yellow}
	game := NewGame(map[string]Color{"1": Black, "2": Yellow})

	game.Turn = Turn{
		TurnOwner:  Black,
		Action:     Attack,
		RoundOrder: roundOrder,
	}
	attacker, _ := game.Countries.getByName(origin)
	defender, _ := game.Countries.getByName(destination)
	attacker.Army = 3
	attacker.Owner = Black
	defender.Army = 1
	defender.Owner = Yellow
	attackIntent := AttackIntent{
		Attacker: origin,
		Defender: destination,
	}

	err := SolveAttackIntent(game, attackIntent, AttackerLoadedDiceThrow)

	if err != nil {
		t.Error("should work")
	} else {
		if attacker.Owner != Black || defender.Owner != Black {
			t.Error("both should be black owned countries")
		}

		if game.RegroupMove.Origin != attacker.Name || game.RegroupMove.Destination != destination {
			t.Error("error creating RegroupMove")
		}
	}
}

func AttackerLoadedDiceThrow(dice int, attacker bool) []int {
	throw := []int{}
	if attacker {
		throw = append(throw, []int{6, 6, 6}...)
	} else {
		throw = append(throw, []int{1, 1, 1}...)
	}
	return throw[0:dice]
}

func Test_RegroupArmy(t *testing.T) {
	origin := CountryName("Argentina")
	destination := CountryName("Brasil")
	player := Black
	game := &Game{}
	game.Countries = newCountriesList()
	game.RestrictedRegroupment = NewRegroupedArmy()
	roundOrder := []Color{Black}
	game.Turn = Turn{
		TurnOwner:  Black,
		Action:     Regroup,
		RoundOrder: roundOrder,
	}

	country, _ := game.Countries.getByName(origin)
	dest, _ := game.Countries.getByName(destination)

	regroupMove := RegroupMove{
		Origin:      origin,
		Destination: destination,
	}
	country.Army = 2

	err := RegroupArmy(game, regroupMove)

	if err == nil {
		t.Error("origin country should belong to player")
	}
	regroupMove = RegroupMove{
		Origin:      origin,
		Destination: destination,
	}

	country.Owner = player
	country.Army = 1

	err = RegroupArmy(game, regroupMove)

	if err == nil {
		t.Error("shouldn't regroup when origin army is less than 2")
	}

	regroupMove = RegroupMove{
		Origin:      origin,
		Destination: destination,
	}

	country.Owner = player
	dest.Owner = player
	country.Army = 4
	dest.Army = 1
	err = RegroupArmy(game, regroupMove)

	if err == nil {
		oArmy, _ := game.getArmy(country.Name)
		dArmy, _ := game.getArmy(dest.Name)
		if oArmy != 3 || dArmy != 2 {
			t.Error("should move one army from origin to destination")
		}
	} else {
		t.Error(err)
	}
}

func Test_TradeCards(t *testing.T) {
	card1 := Card{Country: "Argentina", Logo: Cannon}
	card2 := Card{Country: "Brasil", Logo: Cannon}
	card3 := Card{Country: "Chile", Logo: Cannon}
	cardTrade := CardTrade{Selection: []Card{card1, card2, card3}}
	game := NewGame(map[string]Color{"1": Black})
	player := game.Players.GetAsArray()[0]
	player.Chips = 0
	player.Cards.push(card1)
	player.Cards.push(card2)
	player.Cards.push(card3)
	TradeCards(game, cardTrade)

	if player.Chips != 4 {
		t.Error("should have 4 chips")
	}

	if !game.DiscardDeck.contains(card1) {
		t.Error("card should be discarded")
	}

	if !game.DiscardDeck.contains(card2) {
		t.Error("card should be discarded")
	}

	if !game.DiscardDeck.contains(card3) {
		t.Error("card should be discarded")
	}

	if player.Cards.contains(card1) {
		t.Error("card should be removed from player")
	}

	if player.Cards.contains(card2) {
		t.Error("card should be removed from player")
	}

	if player.Cards.contains(card3) {
		t.Error("card should be removed from player")
	}

	if player.CardTrades != 1 {
		t.Error("should have 1 card trade")
	}

	player.Cards.push(card1)
	player.Cards.push(card2)
	player.Cards.push(card3)
	TradeCards(game, cardTrade)

	if player.Chips != 11 {
		t.Error("should have 11 chips")
	}

	player.Cards.push(card1)
	player.Cards.push(card2)
	player.Cards.push(card3)
	TradeCards(game, cardTrade)

	if player.Chips != 21 {
		t.Error("should have 21 chips")
	}

	player.Cards.push(card1)
	player.Cards.push(card2)
	player.Cards.push(card3)
	TradeCards(game, cardTrade)

	if player.Chips != 36 {
		t.Error("should have 26 chips")
	}
}

func Test_InvalidTrade(t *testing.T) {
	card1 := Card{Country: "Argentina", Logo: Cannon}
	card2 := Card{Country: "Brasil", Logo: Cannon}
	card3 := Card{Country: "Chile", Logo: Cannon}
	cardTrade := CardTrade{Selection: []Card{card1, card2, card3}}
	game := NewGame(map[string]Color{"1": Black})
	player := game.Players.GetAsArray()[0]
	player.Chips = 0
	player.Cards.push(card1)
	player.Cards.push(card2)
	err := TradeCards(game, cardTrade)

	if err == nil {
		t.Error("should return invalid card trade")
	}
}
