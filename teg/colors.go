package teg

import "image/color"

type Color string

const (
	Black   Color = "negro"
	Yellow  Color = "amarillo"
	Red     Color = "rojo"
	Green   Color = "verde"
	Blue    Color = "azul"
	Magenta Color = "magenta"
)

var Colors = []Color{Black, Yellow, Red, Green, Blue, Magenta}

var ColorRGBA = map[Color]color.Color{
	Black:   color.RGBA{R: 0, G: 0, B: 0, A: 255},
	Blue:    color.RGBA{R: 0, G: 0, B: 255, A: 255},
	Red:     color.RGBA{R: 255, G: 0, B: 0, A: 255},
	Magenta: color.RGBA{R: 255, G: 0, B: 255, A: 255},
	Yellow:  color.RGBA{R: 255, G: 255, B: 0, A: 255},
	Green:   color.RGBA{R: 0, G: 255, B: 0, A: 255},
}
