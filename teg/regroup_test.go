package teg

import "testing"

func Test_RegroupedArmy(t *testing.T) {
	regroups := NewRegroupedArmy()
	regroups.Regroup("Argentina", "Brasil")

	army := regroups.GetCountryArmy("Argentina")

	if army != 0 {
		t.Error("should be 0")
	}

	army = regroups.GetCountryArmy("Brasil")

	if army != 1 {
		t.Error("should be 1")
	}

	ok := regroups.RegroupBack("Argentina", "Brasil")

	if ok {
		t.Error("shouldn't regroup back")
	}

	ok = regroups.RegroupBack("Brasil", "Argentina")

	if !ok {
		t.Error("should regroup back")
	}

	army = regroups.GetCountryArmy("Argentina")

	if army != 0 {
		t.Error("should be 0")
	}
	army = regroups.GetCountryArmy("Brasil")

	if army != 0 {
		t.Error("should be 0")
	}

	regroups.Regroup("Brasil", "Argentina")
	regroups.Regroup("Chile", "Argentina")
	regroups.Regroup("Uruguay", "Argentina")
	army = regroups.GetCountryArmy("Brasil")
	if army != 0 {
		t.Error("should be 0")
	}
	army = regroups.GetCountryArmy("Argentina")
	if army != 3 {
		t.Error("should be 3")
	}
}

func Test_ForeignArmy(t *testing.T) {
	foreignArmy := NewForeignArmy()
	foreignArmy.Add("Argentina")

	if foreignArmy.Count() != 1 {
		t.Error("should be 1")
	}

	foreignArmy.Add("Argentina")

	if foreignArmy.Count() != 2 {
		t.Error("should be 2")
	}
	foreignArmy.Remove("Argentina")
	if foreignArmy.Count() != 1 {
		t.Error("should be 1")
	}

	foreignArmy.Remove("Argentina")
	if foreignArmy.Count() != 0 {
		t.Error("should be 0")
	}

	err := foreignArmy.Remove("Argentina")
	if err == nil {
		t.Error("should return error")
	}

	foreignArmy.Add("Argentina")
	foreignArmy.Add("Brasil")
	if foreignArmy.Count() != 2 {
		t.Error("should be 2")
	}

}

func Test_ForeignArmySerialize(t *testing.T) {
	foreignArmy := NewForeignArmy()
	foreignArmy.Add("Argentina")

	if foreignArmy.Count() != 1 {
		t.Error("should be 1")
	}

	copy := foreignArmy.Serialize()

	if copy.Count() != 1 {
		t.Error("should be 1")
	}
}

func Test_RegroupedArmySerialize(t *testing.T) {
	regroups := NewRegroupedArmy()
	regroups.Regroup("Argentina", "Brasil")

	army := regroups.GetCountryArmy("Brasil")
	if army != 1 {
		t.Error("should be 1")
	}

	copy := regroups.Serialize()
	if copy.GetCountryArmy("Brasil") != 1 {
		t.Error("should be 1")
	}
}
