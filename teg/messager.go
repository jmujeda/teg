package teg

import (
	"encoding/json"
	"fmt"
	"time"
)

type Action string

type ClientMessage struct {
	Action
	News ServerMessage
}

type GameplayMessage struct {
	CardTrade    CardTrade
	AttackIntent AttackIntent
	RegroupMove  RegroupMove
	Countries    map[CountryName]Country
}

type ServerUpdate struct {
	Action
	News ServerMessage
}

type ServerMessage struct {
	StartMenuData AccountData
	Room          RoomMessage
	Error         string
	Token         string
	UserGames     UserGamesMessage
}

type UserGamesMessage struct {
	UserGames map[GameId]UserGame
	Updated   bool
}

type UserGame struct {
	Id      GameId
	Status  GameStatus
	Players map[Color]string
	Date    time.Time
}

const (
	Ping             Action = "Ping"
	Pong             Action = "Pong"
	ErrorResponse    Action = "ErrorResponse"
	CreateRoom       Action = "CreateRoom"
	JoinRoom         Action = "JoinRoom"
	UserJoined       Action = "UserJoined"
	StartGame        Action = "StartGame"
	LeaveRoom        Action = "LeaveRoom"
	LoadGame         Action = "LoadGame"
	UpdateRoom       Action = "UpdateRoom"
	InitBoard        Action = "InitBoard"
	AddArmy          Action = "AddArmy"
	Add3Army         Action = "Add3Army"
	Add5Army         Action = "Add5Army"
	Attack           Action = "Attack"
	ConquestRegroup  Action = "ConquestRegroup"
	Regroup          Action = "Regroup"
	DrawCard         Action = "DrawCard"
	ClaimCountry     Action = "ClaimCountry"
	ClaimCountryCard Action = "ClaimCountryCard"
	UpdateBoard      Action = "UpdateBoard"
	InvalidAction    Action = "InvalidAction"
	CreateAccount    Action = "CreateAccount"
	LoginAccount     Action = "LoginAccount"
	Trade            Action = "Trade"
	ListGames        Action = "ListGames"
	EndTurnAction    Action = "EndTurn"
)

type ActionMessage struct {
	Action
	Data any
}

type StartMenuMessage struct {
	Action Action
	Data   AccountData
}

type AccountData struct {
	Id         string
	Password   string
	PlayerName string
}

type LeaveRoomMessage struct {
	Action Action
}

type PingMessage struct {
	Action Action
}

type PongMessage struct {
	Action Action
}

func GetServerUpdate(bytes []byte) (ServerUpdate, error) {
	return GetMessageStruct(bytes, ServerUpdate{})
}

func GetClientMessage(bytes []byte) (ClientMessage, error) {
	return GetMessageStruct(bytes, ClientMessage{})
}

func GetActionCommand(message []byte, action ActionMessage) (Action, error) {
	msg, err := GetMessageStruct(message, action)
	return msg.Action, err
}

func GetPingMessage(packet []byte) (PingMessage, error) {
	return GetMessageStruct(packet, PingMessage{})
}

func GetPongMessage(packet []byte) (PongMessage, error) {
	return GetMessageStruct(packet, PongMessage{})
}

func GetMessageStruct[T any](packetBytes []byte, message T) (T, error) {
	err := json.Unmarshal(packetBytes, &message)
	if err != nil {
		fmt.Println(err.Error())
		return message, err
	}
	return message, nil
}
