package teg

import (
	"encoding/json"

	"go.mongodb.org/mongo-driver/bson"
)

type playerObjective interface {
	getText() string
	getId() objectiveId
	UnmarshalBSON(data []byte) error
}

type maskedObjective struct{}

func (o maskedObjective) getText() string {
	return ""
}

func (o maskedObjective) getId() objectiveId {
	return 0
}

func (o *maskedObjective) UnmarshalBSON(data []byte) error {
	return bson.Unmarshal(data, o)
}

type player struct {
	Id          string          `json:"id"`
	Cards       Deck            `json:"cards"`
	Objective   playerObjective `json:"objective"`
	Color       Color           `json:"color"`
	Chips       int             `json:"chips"`
	CanDrawCard bool            `json:"candrawcard"`
	CardTrades  int             `json:"cardtrades"`
	DrewCard    bool            `json:"drewcard"`
}

type playerMessage struct {
	Id          string
	Cards       Deck
	Objective   string
	Color       Color
	Chips       int
	CanDrawCard bool
	CardTrades  int
	DrewCard    bool
}

type playerGetter interface {
	GetByColor(color Color) *player
	GetAsArray() []*player
	getColors() []Color
	remove(player *player) playerList
	UnmarshalBSON(data []byte) error
}

type playerList struct {
	byColor map[Color]*player
	asArray []*player
}

func newEmptyPlayerList() *playerList {
	return &playerList{
		byColor: map[Color]*player{},
		asArray: []*player{},
	}
}

func (p playerList) MarshalBSON() ([]byte, error) {
	players := []player{}
	for _, player := range p.asArray {
		players = append(players, *player)
	}
	_, bytes, err := bson.MarshalValue(players)
	return bytes, err
}

func (p *playerList) UnmarshalBSON(data []byte) error {
	players := []player{}
	err := bson.UnmarshalValue(bson.TypeArray, data, &players)
	if err != nil {
		return err
	}
	byColor := map[Color]*player{}
	asArray := []*player{}

	for _, player := range players {
		asArray = append(asArray, &player)
		byColor[player.Color] = &player
		player.Objective = getObjective(player.Objective.getId())
	}
	p.asArray = asArray
	p.byColor = byColor
	return nil
}

func (p *player) UnmarshalBSON(data []byte) error {
	type dup player
	player := &dup{Objective: &idObjective{}}
	err := bson.Unmarshal(data, player)
	if err != nil {
		return err
	}
	p.Cards = player.Cards
	p.Objective = getObjective(player.Objective.getId())
	p.Color = player.Color
	p.Chips = player.Chips
	p.Id = player.Id
	p.CanDrawCard = player.CanDrawCard
	p.CardTrades = player.CardTrades
	return nil
}

func (p player) MarshalBSON() ([]byte, error) {
	type dup player
	copy := dup(p)
	copy.Objective = &idObjective{p.Objective.getId()}
	return bson.Marshal(copy)
}

func (p playerList) MarshalJSON() ([]byte, error) {
	players := []player{}
	for _, player := range p.asArray {
		player.Objective = &idObjective{Id: player.Objective.getId()}
		players = append(players, *player)
	}

	return json.Marshal(players)
}

func (p *playerList) UnmarshalJSON(jsonData []byte) error {

	players := []player{}
	err := json.Unmarshal(jsonData, &players)
	if err != nil {
		return err
	}
	byColor := map[Color]*player{}
	asArray := []*player{}

	for _, player := range players {
		asArray = append(asArray, &player)
		byColor[player.Color] = &player
		player.Objective = getObjective(player.Objective.getId())
	}
	p.asArray = asArray
	p.byColor = byColor
	return nil
}

func (p *player) UnmarshalJSON(data []byte) error {
	type dup player
	player := &dup{Objective: &idObjective{}}
	err := json.Unmarshal(data, player)
	if err != nil {
		return err
	}
	p.Cards = player.Cards
	p.Objective = getObjective(player.Objective.getId())
	p.Color = player.Color
	p.Chips = player.Chips
	p.CanDrawCard = player.CanDrawCard
	p.CardTrades = player.CardTrades
	p.Id = player.Id
	return nil
}

func (p playerList) remove(toRemove *player) playerList {
	delete(p.byColor, toRemove.Color)

	players := []*player{}

	for i, player := range p.asArray {
		if player.Color == toRemove.Color {
			players = append(players, p.asArray[:i]...)
			players = append(players, p.asArray[i+1:]...)
		}
	}
	p.asArray = players
	return p

}

func (p playerList) getColors() []Color {
	colors := []Color{}
	for _, player := range p.asArray {
		colors = append(colors, player.Color)
	}
	return colors
}

func (p playerList) GetAsArray() []*player {
	return p.asArray
}

func (p playerList) GetByColor(color Color) *player {
	return p.byColor[color]
}

func newPlayerList(players map[string]Color) *playerList {
	playerList := playerList{
		byColor: map[Color]*player{},
	}

	for playerId, color := range players {
		player := newPlayer(playerId, color)
		playerList.byColor[player.Color] = player
		playerList.asArray = append(playerList.asArray, player)
	}
	return &playerList
}

func (p *player) setClaimed(card Card) {
	p.Cards.setClaimed(card)
}

func (p *player) Serialize(recipient Color) playerMessage {
	if recipient == p.Color {
		return playerMessage{
			Id:          p.Id,
			Cards:       p.Cards,
			Color:       p.Color,
			Objective:   p.Objective.getText(),
			Chips:       p.Chips,
			CanDrawCard: p.CanDrawCard,
			CardTrades:  p.CardTrades,
			DrewCard:    p.DrewCard,
		}
	}
	return playerMessage{
		Id:         p.Id,
		Cards:      p.Cards.mask(),
		Color:      p.Color,
		Objective:  "",
		CardTrades: p.CardTrades,
	}
}

func (p *player) Mask() player {
	return player{
		Cards:      p.Cards.mask(),
		Color:      p.Color,
		Objective:  &maskedObjective{},
		CardTrades: p.CardTrades,
	}
}

func newPlayer(id string, color Color) *player {
	return &player{Id: id, Color: color}
}
