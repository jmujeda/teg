package teg

import (
	"testing"

	"go.mongodb.org/mongo-driver/bson"
)

func Test_NewObjectivesDeck(t *testing.T) {
	deck := NewObjectivesDeck(true)
	conquestDeck := NewObjectivesDeck(false)

	if len(deck.items) == 0 {
		t.Error("full deck is empty")
	}

	if len(conquestDeck.items) == 0 {
		t.Error("conquest deck is empty")
	}
}

func Test_PickOne(t *testing.T) {
	order := []Color{Black, Yellow}
	deck := NewObjectivesDeck(true)
	deckLen := len(deck.items)
	game := NewGame(map[string]Color{"1": Black, "2": Yellow})
	obj1 := deck.assign(game, Black, order)

	if deckLen == len(deck.items) {
		t.Error("pick one should remove objective from deck")
	}
	if obj1.getText() == "" {
		t.Error("objective text shouldn't be empty")
	}
	obj2 := deck.assign(game, Yellow, order)

	if obj1.getId() == obj2.getId() {
		t.Error("PickOne shouldn't return same objective twice")
	}
}

func Test_ConquestObjective(t *testing.T) {
	obj := conquestObjective{
		Countries: map[ContinentName]int{SouthAmerica: 6},
		Text:      "Conquistar áfrica, 5 países de américa del norte y 4 de europa.",
	}
	game := NewGame(map[string]Color{"1": Black})

	if !obj.isFulfilled(game, Black) {
		t.Error("should return true when all countries belong to player")
	}

	obj2 := conquestObjective{
		Countries: map[ContinentName]int{SouthAmerica: 6},
		Text:      "Conquistar áfrica, 5 países de américa del norte y 4 de europa.",
	}

	game2 := NewGame(map[string]Color{"1": Black, "2": Yellow})

	if obj2.isFulfilled(game2, Black) {
		t.Error("should return false")
	}

}

func Test_DestroyObjective(t *testing.T) {
	game := NewGame(map[string]Color{"1": Black, "2": Yellow})

	game.Kills[Black] = append(game.Kills[Black], Yellow)
	obj := destroyObjective{
		Target: Yellow,
	}

	for _, country := range game.Countries.getAsArray() {
		country.Owner = Black
	}

	if !obj.isFulfilled(game, Black) {
		t.Error("destroy objective should be fulfilled")
	}

}

func Test_BSONMarshalConquestObjective(t *testing.T) {
	conq := conquestObjectives[7]
	bytes, err := bson.Marshal(conq)
	if err != nil {
		t.Error(err)
	}

	t.Log(bytes)

	result := &conquestObjective{}
	err = bson.Unmarshal(bytes, result)
	t.Log(result)
	if err != nil {
		t.Error(err)
	}

	if result.getId() != conq.Id {
		t.Error("codes don't match")
	}

	for country, count := range conq.Countries {
		resultCount, ok := result.Countries[country]
		if !ok {
			t.Error("conquest objective countries don't match")
		} else if resultCount != count {
			t.Error("conquest objective count don't match")
		}

	}
}

func Test_BSONMarshalDestroyObjective(t *testing.T) {
	destroy := destroyObjectives[2]
	bytes, err := bson.Marshal(destroy)
	if err != nil {
		t.Error(err)
	}

	result := &destroyObjective{}
	err = bson.Unmarshal(bytes, result)
	if err != nil {
		t.Error(err)
	}

	if result.getId() != destroy.Id {
		t.Error("codes don't match")
	}

}
