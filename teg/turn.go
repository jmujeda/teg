package teg

import (
	"errors"
)

func getTurnIterator(turn Turn) turnIterator {
	turnIterator := turnIterators[turn.Action]
	turnIterator.setTurn(turn)
	return turnIterator
}

type turnIterator interface {
	next() (Turn, error)
	setTurn(turn Turn)
}

var turnIterators = map[Action]turnIterator{
	Add3Army:        &add3ArmyTurn{},
	Add5Army:        &add5ArmyTurn{},
	Attack:          &attackTurn{},
	Regroup:         &regroupTurn{},
	DrawCard:        &drawCardTurn{},
	AddArmy:         &addArmyTurn{},
	ConquestRegroup: &conquestRegroupTurn{},
}

type add3ArmyTurn struct {
	Turn
}
type add5ArmyTurn struct {
	Turn
}
type attackTurn struct {
	Turn
}
type regroupTurn struct {
	Turn
}
type drawCardTurn struct {
	Turn
}
type addArmyTurn struct {
	Turn
}
type conquestRegroupTurn struct {
	Turn
}

type Turn struct {
	TurnOwner  Color   `json:"turnowner"`
	Action     Action  `json:"action"`
	RoundOrder []Color `json:"roundorder"`
}

func (t *Turn) setTurn(turn Turn) {
	t.TurnOwner = turn.TurnOwner
	t.Action = turn.Action
	t.RoundOrder = turn.RoundOrder
}

func (t *Turn) getTurnOwnerIndex() (int, error) {
	for i, color := range t.RoundOrder {
		if color == t.TurnOwner {
			return i, nil
		}
	}
	return 0, errors.New("turn owner color not found in round order")
}

func (t add3ArmyTurn) next() (Turn, error) {
	next := t.Turn
	sitIndex, err := t.getTurnOwnerIndex()
	if err != nil {
		return next, err
	}

	if t.TurnOwner == t.RoundOrder[len(t.RoundOrder)-1] {
		next.Action = Attack
		next.TurnOwner = t.RoundOrder[0]
	} else {
		next.TurnOwner = t.RoundOrder[sitIndex+1]
	}

	return next, nil
}

func (t add5ArmyTurn) next() (Turn, error) {
	next := t.Turn
	sitIndex, err := t.getTurnOwnerIndex()
	if err != nil {
		return next, err
	}

	if t.TurnOwner == t.RoundOrder[len(t.RoundOrder)-1] {
		next.Action = Add3Army
		next.TurnOwner = t.RoundOrder[0]
	} else {
		next.TurnOwner = t.RoundOrder[sitIndex+1]
	}
	return next, nil
}

func (t attackTurn) next() (Turn, error) {
	return Turn{Action: Regroup, TurnOwner: t.TurnOwner, RoundOrder: t.RoundOrder}, nil
}

func (t regroupTurn) next() (Turn, error) {
	return Turn{Action: DrawCard, TurnOwner: t.TurnOwner, RoundOrder: t.RoundOrder}, nil
}

func (t drawCardTurn) next() (Turn, error) {
	next := t.Turn
	sitIndex, err := t.getTurnOwnerIndex()
	if err != nil {
		return next, err
	}

	if t.TurnOwner == t.RoundOrder[len(t.RoundOrder)-1] {
		next.RoundOrder = append(t.RoundOrder[1:], t.RoundOrder[0])
		next.Action = AddArmy
		next.TurnOwner = next.RoundOrder[0]
	} else {
		next.Action = Attack
		next.TurnOwner = t.RoundOrder[sitIndex+1]
	}

	return next, nil
}

func (t conquestRegroupTurn) next() (Turn, error) {
	return Turn{Action: Attack, RoundOrder: t.RoundOrder, TurnOwner: t.TurnOwner}, nil
}

func (t addArmyTurn) next() (Turn, error) {
	next := t.Turn
	sitIndex, err := t.getTurnOwnerIndex()
	if err != nil {
		return next, err
	}

	if t.TurnOwner == t.RoundOrder[len(t.RoundOrder)-1] {
		next.Action = Attack
		next.TurnOwner = t.RoundOrder[0]
	} else {
		next.TurnOwner = t.RoundOrder[sitIndex+1]
	}
	return next, nil

}
