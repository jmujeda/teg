package teg

import "errors"

type RestrictedArmyRegroupment interface {
	GetCountryArmy(name CountryName) int
	Regroup(origin CountryName, destination CountryName)
	RegroupBack(origin CountryName, destination CountryName) bool
	Serialize() RegroupedArmyMessage
	UnmarshalBSON(data []byte) error
}

type RegroupedArmy struct {
	Regroupments map[CountryName]*ForeignArmy
}

type RegroupedArmyMessage struct {
	Regroupments map[CountryName]ForeignArmy
}

type ForeignArmy struct {
	Army map[CountryName]int
}

func (r RegroupedArmyMessage) GetCountryArmy(name CountryName) int {
	foreignArmy, ok := r.Regroupments[name]
	if !ok {
		return 0
	}
	return foreignArmy.Count()
}

func NewRegroupedArmy() *RegroupedArmy {
	return &RegroupedArmy{Regroupments: map[CountryName]*ForeignArmy{}}
}

func (a *RegroupedArmy) UnmarshalBSON(data []byte) error {
	return nil
}

func (a *RegroupedArmy) Serialize() RegroupedArmyMessage {
	r := RegroupedArmyMessage{Regroupments: map[CountryName]ForeignArmy{}}
	for countryName, foreignArmy := range a.Regroupments {
		r.Regroupments[countryName] = foreignArmy.Serialize()
	}
	return r
}

func (a *RegroupedArmy) GetCountryArmy(name CountryName) int {
	foreignArmy, ok := a.Regroupments[name]
	if !ok {
		return 0
	}
	return foreignArmy.Count()
}

func (a *RegroupedArmy) Regroup(origin CountryName, destination CountryName) {
	foreignArmy, ok := a.Regroupments[destination]
	if !ok {
		a.Regroupments[destination] = NewForeignArmy()
		a.Regroupments[destination].Add(origin)
	} else {
		foreignArmy.Add(origin)
	}
}

func (a *RegroupedArmy) RegroupBack(origin CountryName, destination CountryName) bool {
	foreignArmy, ok := a.Regroupments[origin]
	if !ok {
		return false
	}
	return foreignArmy.Remove(destination) == nil
}

func (f *ForeignArmy) Add(countryName CountryName) {
	f.Army[countryName]++
}

func (f *ForeignArmy) Remove(countryName CountryName) error {
	if f.Army[countryName] > 0 {
		f.Army[countryName]--
		return nil
	}
	return errors.New("no army to remove")
}

func (f *ForeignArmy) BelongsTo(countryName CountryName) bool {
	army, ok := f.Army[countryName]
	if !ok || army == 0 {
		return false
	}
	return true
}

func (f *ForeignArmy) Count() int {
	count := 0

	for _, army := range f.Army {
		count += army
	}
	return count
}

func (f *ForeignArmy) Serialize() ForeignArmy {
	res := ForeignArmy{Army: map[CountryName]int{}}
	for countryName, army := range f.Army {
		res.Army[countryName] = army
	}
	return res
}

func NewForeignArmy() *ForeignArmy {
	return &ForeignArmy{Army: map[CountryName]int{}}
}
