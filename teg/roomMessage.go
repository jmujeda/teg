package teg

type RoomMessage struct {
	Name     string
	Password string
	Game     GameMessage
	Users    []UserMessage
}

type UserMessage struct {
	Name  string
	Color Color
}

func (r *RoomMessage) Clone() RoomMessage {
	room := *r
	room.Game = r.Game.Clone()
	return room
}

func (r *RoomMessage) GetRoommatesList() []string {
	names := []string{}
	for k := range r.Users {
		names = append(names, r.Users[k].Name)
	}
	return names
}
