package teg

import "testing"

type testCase struct {
	sut                Turn
	expectedOwner      Color
	expectedAction     Action
	expectedRoundOrder []Color
}

func Test_EndTurn(t *testing.T) {

	roundOrder := []Color{Black, Yellow}
	shiftedRoundOrder := []Color{Yellow, Black}

	testCases := []testCase{
		{
			sut: Turn{
				Action:     Add5Army,
				RoundOrder: roundOrder,
				TurnOwner:  Black,
			},
			expectedOwner:      Yellow,
			expectedAction:     Add5Army,
			expectedRoundOrder: roundOrder,
		},
		{
			sut: Turn{
				Action:     Add5Army,
				RoundOrder: roundOrder,
				TurnOwner:  Yellow,
			},
			expectedOwner:      Black,
			expectedAction:     Add3Army,
			expectedRoundOrder: roundOrder,
		},
		{
			sut: Turn{
				Action:     Add3Army,
				RoundOrder: roundOrder,
				TurnOwner:  Black,
			},
			expectedOwner:      Yellow,
			expectedAction:     Add3Army,
			expectedRoundOrder: roundOrder,
		},
		{
			sut: Turn{
				Action:     Add3Army,
				RoundOrder: roundOrder,
				TurnOwner:  Yellow,
			},
			expectedOwner:      Black,
			expectedAction:     Attack,
			expectedRoundOrder: roundOrder,
		},
		{
			sut: Turn{
				Action:     Attack,
				RoundOrder: roundOrder,
				TurnOwner:  Black,
			},
			expectedOwner:      Black,
			expectedAction:     Regroup,
			expectedRoundOrder: roundOrder,
		},
		{
			sut: Turn{
				Action:     Attack,
				RoundOrder: roundOrder,
				TurnOwner:  Yellow,
			},
			expectedOwner:      Yellow,
			expectedAction:     Regroup,
			expectedRoundOrder: roundOrder,
		},
		{
			sut: Turn{
				Action:     Regroup,
				RoundOrder: roundOrder,
				TurnOwner:  Yellow,
			},
			expectedOwner:      Yellow,
			expectedAction:     DrawCard,
			expectedRoundOrder: roundOrder,
		},
		{
			sut: Turn{
				Action:     Regroup,
				RoundOrder: roundOrder,
				TurnOwner:  Black,
			},
			expectedOwner:      Black,
			expectedAction:     DrawCard,
			expectedRoundOrder: roundOrder,
		},
		{
			sut: Turn{
				Action:     DrawCard,
				RoundOrder: roundOrder,
				TurnOwner:  Black,
			},
			expectedOwner:      Yellow,
			expectedAction:     Attack,
			expectedRoundOrder: roundOrder,
		},
		{
			sut: Turn{
				Action:     DrawCard,
				RoundOrder: roundOrder,
				TurnOwner:  Yellow,
			},
			expectedOwner:      Yellow,
			expectedAction:     AddArmy,
			expectedRoundOrder: shiftedRoundOrder,
		},
		{
			sut: Turn{
				Action:     AddArmy,
				RoundOrder: shiftedRoundOrder,
				TurnOwner:  Yellow,
			},
			expectedOwner:      Black,
			expectedAction:     AddArmy,
			expectedRoundOrder: shiftedRoundOrder,
		},
		{
			sut: Turn{
				Action:     AddArmy,
				RoundOrder: shiftedRoundOrder,
				TurnOwner:  Black,
			},
			expectedOwner:      Yellow,
			expectedAction:     Attack,
			expectedRoundOrder: shiftedRoundOrder,
		},
	}

	game := SetUpGame(map[string]Color{"1": Black, "2": Yellow})
	game.RoundOrder = roundOrder
	game.TurnOwner = Black
	turn := Turn{
		Action:     Add3Army,
		RoundOrder: roundOrder,
		TurnOwner:  Black,
	}
	game.Turn = turn

	for i, test := range testCases {
		game.Turn = test.sut
		err := EndTurn(game)

		if err != nil {
			t.Error(err)
		}

		if game.TurnOwner != testCases[i].expectedOwner {
			t.Error("turn owner should change for sut", testCases[i].sut)
		}
		if game.Action != testCases[i].expectedAction {
			t.Error("turn action should remain for sut", testCases[i].sut)
		}
		if game.RoundOrder == nil {
			t.Error("round order can't be empty for sut", testCases[i].sut)
		} else {
			if game.RoundOrder[0] != testCases[i].expectedRoundOrder[0] {
				t.Error("round order shouldn't change for sut", testCases[i].sut)
			}
		}
	}

}
