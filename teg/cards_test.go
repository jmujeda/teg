package teg

import "testing"

func Test_ShuffleDeck(t *testing.T) {

	deck := NewCountriesDeck()

	first := deck.Cards[0].Country
	iterations := 10
	matches := 0
	for i := 1; i <= iterations; i++ {
		deck.shuffle()
		if deck.Cards[0].Country == first {
			matches++
		}
	}

	if matches == iterations {
		t.Error("shuffle not shuffling")
	}
}

func Test_DeckPickOne(t *testing.T) {
	deck := NewCountriesDeck()
	size := len(deck.Cards)
	_, err := deck.pickOne()
	if err != nil {
		t.Error("error picking card")
	}

	if size == len(deck.Cards) {
		t.Error("pick one not removing card from deck")
	}

	for range len(deck.Cards) {
		deck.pickOne()
	}

	if len(deck.Cards) > 0 {
		t.Error("deck should be empty")
	}
	_, err = deck.pickOne()
	if err == nil {
		t.Error("PickOne on empty deck should return error")
	}
}

func Test_Mask(t *testing.T) {
	deck := NewCountriesDeck()
	maskedDeck := deck.mask()

	for _, card := range maskedDeck.Cards {
		if card.Country != "" || card.Logo != "" {
			t.Error("card not masked")
		}
	}

	for _, card := range deck.Cards {
		if card.Country == "" || card.Logo == "" {
			t.Error("original deck shouldn't be modified")
		}
	}
}
