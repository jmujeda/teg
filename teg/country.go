package teg

import (
	"encoding/json"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
)

type CountryName string
type ContinentName string

const (
	Africa       ContinentName = "africa"
	SouthAmerica ContinentName = "america del sur"
	NorthAmerica ContinentName = "america del norte"
	Asia         ContinentName = "asia"
	Oceania      ContinentName = "oceania"
	Europe       ContinentName = "europa"
)

type CountryList interface {
	getByName(name CountryName) (*Country, error)
	getByColor(color Color) []*Country
	Serialize() map[CountryName]Country
	getAsArray() []*Country
	UnmarshalBSON(data []byte) error
}

type Country struct {
	Neighbours []CountryName `bson:"omitempty"`
	Army       int
	Owner      Color
	Name       CountryName
	Continent  ContinentName
}

type Countries struct {
	byName map[CountryName]*Country
	array  []*Country
}

func (c Country) IsNeighbour(country Country) bool {
	for _, neighbour := range c.Neighbours {
		if neighbour == country.Name {
			return true
		}
	}
	return false
}

func (c Countries) MarshalBSON() ([]byte, error) {
	countries := []Country{}

	for _, country := range c.array {
		copy := *country
		copy.Neighbours = []CountryName{}
		countries = append(countries, copy)
	}
	_, b, e := bson.MarshalValue(countries)
	return b, e
}

func (c *Countries) loadCountries(countries []Country) {
	fmt.Println(countries)
	c.byName = map[CountryName]*Country{}
	c.array = []*Country{}

	for _, country := range countries {
		country.Neighbours = countryNeighbours[country.Name]
		c.byName[country.Name] = &country
		c.array = append(c.array, &country)
	}
}

func (c *Countries) UnmarshalBSON(data []byte) error {
	countries := []Country{}
	err := bson.UnmarshalValue(bson.TypeArray, data, &countries)
	if err != nil {
		return err
	}
	c.loadCountries(countries)

	return nil
}

func (c Countries) MarshalJSON() ([]byte, error) {
	countries := []Country{}

	for _, country := range c.array {
		countries = append(countries, *country)
	}
	return json.Marshal(countries)
}

func (c *Countries) UnmarshalJSON(jsonData []byte) error {

	countries := []Country{}
	err := json.Unmarshal(jsonData, &countries)
	if err != nil {
		return err
	}
	c.loadCountries(countries)
	return nil
}

func (c *Countries) getByName(name CountryName) (*Country, error) {
	country := c.byName[name]
	if country == nil {
		return nil, fmt.Errorf("country %s not found", name)
	}
	return country, nil
}

func (c *Countries) assignPlayers(players playerGetter) {
	countries := Shuffle(c.array)
	playersArray := players.GetAsArray()
	playersCount := len(playersArray)
	chunkSize := len(countries) / playersCount
	mod := len(countries) % playersCount
	chunkStart := 0
	for i := 0; i < playersCount; i++ {

		slice := []*Country{}
		if mod > 0 {
			slice = append(slice, countries[chunkStart:chunkStart+chunkSize+1]...)
			chunkStart = chunkStart + chunkSize + 1
		} else {
			slice = append(slice, countries[chunkStart:chunkStart+chunkSize]...)
			chunkStart = chunkStart + chunkSize
		}

		for _, country := range slice {
			country.Owner = playersArray[i].Color
		}

		mod--
	}
}

func (c *Countries) getAsArray() []*Country {
	return c.array
}

func (c *Countries) getByColor(color Color) []*Country {
	countries := []*Country{}
	for _, country := range c.array {
		if country.Owner == color {
			countries = append(countries, country)
		}
	}
	return countries
}

func (c *Countries) Serialize() map[CountryName]Country {
	countries := map[CountryName]Country{}
	for name, country := range c.byName {
		countries[name] = *country
	}
	return countries
}

func newCountriesList() *Countries {
	countries := &Countries{
		array:  []*Country{},
		byName: map[CountryName]*Country{},
	}
	countries.setArray()
	countries.setCountriesByName()
	return countries
}

func (c *Countries) setArray() {
	c.array = []*Country{
		{Continent: SouthAmerica, Name: "Argentina", Army: 1, Neighbours: []CountryName{"Brasil", "Chile", "Uruguay", "Peru"}},
		{Continent: SouthAmerica, Name: "Brasil", Army: 1, Neighbours: []CountryName{"Argentina", "Uruguay", "Peru", "Colombia"}},
		{Continent: SouthAmerica, Name: "Uruguay", Army: 1, Neighbours: []CountryName{"Argentina", "Brasil"}},
		{Continent: SouthAmerica, Name: "Peru", Army: 1, Neighbours: []CountryName{"Argentina", "Brasil", "Colombia", "Chile"}},
		{Continent: SouthAmerica, Name: "Chile", Army: 1, Neighbours: []CountryName{"Argentina", "Peru"}},
		{Continent: SouthAmerica, Name: "Colombia", Army: 1, Neighbours: []CountryName{"Peru", "Brasil"}},
	}
}

func (c *Countries) setCountriesByName() {
	for _, country := range c.array {
		c.byName[country.Name] = country
	}
}

var countryNeighbours = map[CountryName][]CountryName{
	"Argentina": {"Brasil", "Chile", "Uruguay", "Peru"},
	"Brasil":    {"Argentina", "Uruguay", "Peru", "Colombia"},
	"Uruguay":   {"Argentina", "Brasil"},
	"Peru":      {"Argentina", "Brasil", "Colombia", "Chile"},
	"Chile":     {"Argentina", "Peru"},
	"Colombia":  {"Peru", "Brasil"},
}
