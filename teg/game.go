package teg

import (
	"errors"
	maps "maps"
	"math/rand/v2"
)

type GameStatus string
type GameId string

const (
	Creating GameStatus = "creating"
	Playing  GameStatus = "playing"
	Finished GameStatus = "finished"
)

type Game struct {
	Id                    GameId
	Countries             CountryList
	Players               playerGetter
	Step                  int                       `json:"step"`
	Deck                  *Deck                     `json:"deck"`
	DiscardDeck           *Deck                     `json:"discarddeck"`
	RegroupMove           RegroupMove               `json:"regroupmove"`
	Round                 int                       `json:"round"`
	RestrictedRegroupment RestrictedArmyRegroupment `json:"restrictedregroupment"`
	Kills                 map[Color][]Color         `json:"kills"`
	Turn                  `json:"turn"`
	Status                GameStatus `json:"status"`
	AttackIntent          `json:"attackintent"`
}

type GameMessage struct {
	Id                    GameId
	Countries             map[CountryName]Country
	Players               map[Color]playerMessage
	Step                  int
	PlayerColor           Color
	BoardChanged          bool
	RegroupMove           RegroupMove
	RestrictedRegroupment RegroupedArmyMessage
	CardTrade             CardTrade
	Status                GameStatus
	Turn
	AttackIntent
}

func NewEmptyGame() *Game {
	return &Game{
		Countries:             newCountriesList(),
		Players:               newEmptyPlayerList(),
		RestrictedRegroupment: NewRegroupedArmy(),
	}
}

func (g GameMessage) Clone() GameMessage {
	clone := g
	countries := maps.Clone(g.Countries)
	clone.Countries = countries

	players := maps.Clone(g.Players)
	clone.Players = players
	return clone
}

func (g *GameMessage) GetArmy(name CountryName) (int, error) {
	country, ok := g.Countries[name]
	if !ok {
		return 0, errors.New("country not found")
	}
	return country.Army + g.RestrictedRegroupment.GetCountryArmy(name), nil
}

func NewTestGame(players map[string]Color) *Game {
	countries := newCountriesList()
	playerList := newPlayerList(players)

	countries.assignPlayers(playerList)
	c, _ := countries.getByName("Brasil")
	c.Army = 4
	c, _ = countries.getByName("Argentina")
	c.Army = 4
	roundOrder := createRoundOrder(playerList)

	game := &Game{
		Players:     playerList,
		Countries:   countries,
		Deck:        NewCountriesDeck(),
		DiscardDeck: &Deck{},
		Kills:       map[Color][]Color{},
		Status:      Playing,
		Turn:        Turn{Action: Add3Army, RoundOrder: roundOrder, TurnOwner: roundOrder[0]},
	}
	game.dealObjectives(true, roundOrder)

	//game.TurnManager = createAdd5ArmyRound(roundOrder)
	//game.TurnManager = createAddArmyRound(roundOrder)

	black := game.Players.GetByColor(Black)
	card, _ := game.Deck.pickOne()
	black.Cards.push(card)
	card, _ = game.Deck.pickOne()
	black.Cards.push(card)
	game.RestrictedRegroupment = NewRegroupedArmy()

	//game.setUpTurn()

	return game
}

func NewGame(players map[string]Color) *Game {

	playerList := newPlayerList(players)
	countries := newCountriesList()
	countries.assignPlayers(playerList)
	roundOrder := createRoundOrder(playerList)

	game := &Game{
		Players:     playerList,
		Countries:   countries,
		Deck:        NewCountriesDeck(),
		DiscardDeck: &Deck{},
		Kills:       map[Color][]Color{},
		Status:      Playing,
		Turn:        Turn{Action: Add5Army, RoundOrder: roundOrder, TurnOwner: roundOrder[0]},
	}
	game.dealObjectives(true, roundOrder)

	game.RestrictedRegroupment = NewRegroupedArmy()

	return game
}

func (g *Game) dealObjectives(includeConquest bool, roundOrder []Color) {
	var objectives ObjectivesDeck
	if len(roundOrder) == 2 {
		player1 := g.Players.GetByColor(roundOrder[0])
		player1Objective, err := getDestroyObjective(roundOrder[1])
		if err == nil {
			player1.Objective = player1Objective
		}
		player2 := g.Players.GetByColor(roundOrder[1])
		player2Objective, err := getDestroyObjective(roundOrder[0])
		if err == nil {
			player2.Objective = player2Objective
		}
	} else {
		objectives = NewObjectivesDeck(includeConquest)
		for _, player := range g.Players.GetAsArray() {
			player.Objective = objectives.assign(g, player.Color, roundOrder)
		}
	}
}

func (g *Game) Serialize(recipient Color) GameMessage {
	message := GameMessage{
		Id:                    g.Id,
		Countries:             g.Countries.Serialize(),
		Players:               map[Color]playerMessage{},
		Step:                  g.Step + 1,
		Turn:                  g.Turn,
		RegroupMove:           g.RegroupMove,
		RestrictedRegroupment: g.RestrictedRegroupment.Serialize(),
		Status:                g.Status,
		AttackIntent:          g.AttackIntent,
	}

	players := map[Color]playerMessage{}
	for _, player := range g.Players.GetAsArray() {
		if player.Color == recipient {
			message.PlayerColor = player.Color
		}
		players[player.Color] = player.Serialize(recipient)
	}
	message.Players = players
	return message
}

func createRoundOrder(players playerGetter) []Color {
	playerColors := []Color{}
	for _, player := range players.GetAsArray() {
		playerColors = append(playerColors, player.Color)
	}

	for i := range playerColors {
		j := rand.IntN(i + 1)
		playerColors[i], playerColors[j] = playerColors[j], playerColors[i]
	}
	return playerColors
}

func (g *Game) getArmy(name CountryName) (int, error) {
	country, err := g.Countries.getByName(name)
	if err != nil {
		return 0, err
	}
	return country.Army + g.RestrictedRegroupment.GetCountryArmy(name), nil
}

func (g *Game) regroup(origin *Country, destination *Country) error {
	if army, _ := g.getArmy(origin.Name); army < 2 {
		return errors.New("can't regroup when army is less than 2")
	}

	if g.RestrictedRegroupment.RegroupBack(origin.Name, destination.Name) {
		destination.Army++
		return nil
	} else if origin.Army > 1 {
		g.RestrictedRegroupment.Regroup(origin.Name, destination.Name)
		origin.Army--
		return nil
	}
	g.RegroupMove = RegroupMove{}

	return errors.New("can't regroup from " + string(origin.Name) + " to " + string(destination.Name))
}

func (g *Game) finishRegroup() {
	for _, country := range g.Countries.getByColor(g.TurnOwner) {
		country.Army, _ = g.getArmy(country.Name)
	}
	g.RestrictedRegroupment = NewRegroupedArmy()
}

func (g *Game) removePlayer(defeated *player, attacker *player) {
	g.Players.remove(defeated)
	g.Kills[attacker.Color] = append(g.Kills[attacker.Color], defeated.Color)
}

func (g *Game) drawCard() Card {
	card, err := g.Deck.pickOne()
	if err != nil {
		g.Deck = g.DiscardDeck
		g.Deck.shuffle()
		g.DiscardDeck = &Deck{}
		card, _ = g.Deck.pickOne()
	}
	return card
}

func (g *Game) CanEndTurn() (bool, error) {
	// if g.Turn.Action == Add3Army || g.Turn.Action == Add5Army || g.Turn.Action == AddArmy {
	// 	player := g.Players.GetByColor(g.TurnOwner)
	// 	if player != nil && player.Chips > 0 {
	// 		return false, errors.New("can't end turn until all armies are placed")
	// 	}
	// }
	return true, nil
}

func (g *Game) SetupTurn() {
	for _, player := range g.Players.GetAsArray() {
		player.DrewCard = false
	}
}
