package teg

import (
	"errors"
	"math/rand/v2"
	"sort"
)

type AttackIntent struct {
	Attacker     CountryName
	Defender     CountryName
	AttackerDice []int
	DefenderDice []int
	Conquest     bool
}

type RegroupMove struct {
	Origin      CountryName
	Destination CountryName
	Count       int
}

type DiceThrow func(dice int, attacker bool) []int

func CommonDiceThrow(dice int, attacker bool) []int {
	if dice > 3 {
		dice = 3
	} else if attacker {
		dice = dice - 1
	}
	result := make([]int, dice)

	for i := range dice {
		result[i] = throwDie()
	}
	sort.Slice(result, func(i, j int) bool {
		return result[i] > result[j]
	})
	return result
}

func SetConquestRegroupTurn(game *Game) {
	game.Turn = Turn{
		TurnOwner:  game.TurnOwner,
		Action:     ConquestRegroup,
		RoundOrder: game.RoundOrder,
	}
}

func AddPlayerArmy(color Color, game *Game, countriesUpdate map[CountryName]Country) error {

	if len(game.Countries.getByColor(color)) < 1 {
		return errors.New("player don't have countries to add army")
	}

	preTurnArmyCount := 0
	postTurnArmyCount := 0

	for _, country := range game.Countries.getByColor(game.TurnOwner) {
		preTurnArmyCount += country.Army
		if countriesUpdate[country.Name].Army < country.Army {
			return errors.New("can't have less army than before")
		}
	}

	for _, country := range countriesUpdate {
		if country.Owner == game.TurnOwner {
			postTurnArmyCount += country.Army
		}
	}

	if game.Players.GetByColor(color).Chips+preTurnArmyCount != postTurnArmyCount {
		return errors.New("wrong amount of chips added by player")
	}

	for _, country := range game.Countries.getByColor(game.TurnOwner) {
		country.Army = countriesUpdate[country.Name].Army
	}

	game.Players.GetByColor(color).Chips = 0
	EndTurn(game)
	return nil
}

func TradeCards(game *Game, cardTrade CardTrade) error {
	if !cardTrade.CanTrade() {
		return errors.New("invalid card trade")
	}
	player := game.Players.GetByColor(game.TurnOwner)

	for _, card := range cardTrade.Selection {
		if !player.Cards.contains(card) {
			return errors.New("invalid card trade")
		}
	}

	switch trades := player.CardTrades; trades {
	case 0:
		player.Chips += 4
	case 1:
		player.Chips += 7
	case 2:
		player.Chips += 10
	default:
		player.Chips += (5 * trades)
	}
	for _, card := range cardTrade.Selection {
		game.DiscardDeck.push(card)
		player.Cards.remove(card)
	}
	player.CardTrades++
	return nil
}

// Validate attacker and defender are neighbours
func SolveAttackIntent(game *Game, attackIntent AttackIntent, diceThrow DiceThrow) error {

	attacker, err := game.Countries.getByName(attackIntent.Attacker)
	defender, err2 := game.Countries.getByName(attackIntent.Defender)

	if err != nil && err2 != nil {
		EndTurn(game)
		return nil
	}
	if err != nil {
		return err
	}
	if err2 != nil {
		return err2
	}
	defenderColor := defender.Owner
	if attacker.Owner != game.TurnOwner {
		return errors.New("attacker should belong to turn owner")
	}

	if defender.Owner == game.TurnOwner {
		return errors.New("defender shouldn't belong to turn owner")
	}

	if attacker.Army < 2 {
		return errors.New("attacker should have more than 1 army")
	}
	attackIntent.AttackerDice = diceThrow(attacker.Army, true)
	attackIntent.DefenderDice = diceThrow(defender.Army, false)

	updateArmies(attacker, defender, attackIntent)

	if attacker.Owner == defender.Owner {

		if len(game.Countries.getByColor(defenderColor)) == 0 {
			solvePlayerDestroyed(game, game.Players.GetByColor(attacker.Owner), game.Players.GetByColor(defenderColor))
			if isGameFinished(game) {
				game.Status = Finished
				return nil
			}
		}
		if attacker.Army > 1 {
			game.RegroupMove = RegroupMove{
				Origin:      attacker.Name,
				Destination: defender.Name,
			}
			SetConquestRegroupTurn(game)
		}
		game.Players.GetByColor(attacker.Owner).CanDrawCard = true

	}

	game.AttackIntent = attackIntent

	return nil
}

func solvePlayerDestroyed(game *Game, attacker *player, defeated *player) {
	for _, card := range defeated.Cards.Cards {
		attacker.Cards.push(card)
	}
	game.removePlayer(defeated, attacker)
	objective := getObjective(attacker.Objective.getId())
	if !objective.isFulfillable(game, attacker.Color) {
		attacker.Objective = NewCommonObjective()
	}

}

func MoveArmyOnConquest(game *Game, regroup RegroupMove) error {
	if regroup.Count > 3 || regroup.Count < 1 {
		return errors.New("count should be a value between 1 and 3")
	}
	origin, err := game.Countries.getByName(regroup.Origin)

	if err != nil {
		return errors.New("origin country not found")
	}
	destination, err := game.Countries.getByName(regroup.Destination)

	if err != nil {
		return errors.New("destination country not found")
	}
	if destination.Army != 0 {
		return errors.New("destination army count should be 0")
	}

	if regroup.Count >= origin.Army {
		return errors.New("cant move more army than existing")
	}

	if regroup.Count > 0 {
		destination.Army = regroup.Count
		origin.Army -= regroup.Count
	}

	game.RegroupMove = RegroupMove{}
	game.AttackIntent = AttackIntent{}
	EndTurn(game)
	return nil
}

func FinishRegroup(game *Game) {
	game.finishRegroup()
	drawConquestCard(game)
}

func RegroupArmy(game *Game, regroup RegroupMove) error {
	origin, err := game.Countries.getByName(regroup.Origin)
	destination, err2 := game.Countries.getByName(regroup.Destination)

	if err != nil {
		return errors.New("origin country not found")
	}

	if err2 != nil {
		return errors.New("destination country not found")
	}

	if origin.Owner != game.TurnOwner || destination.Owner != game.TurnOwner {
		return errors.New("origin and destiny should belong to turn owner")
	}

	if army, _ := game.getArmy(origin.Name); army < 2 {
		return errors.New("shouldn't regroup when origin country has only 1 army")
	}

	err = game.regroup(origin, destination)
	return err
}

func drawConquestCard(game *Game) {
	turnOwner := game.TurnOwner
	player := game.Players.GetByColor(turnOwner)
	if player.CanDrawCard {
		card := game.drawCard()
		player.DrewCard = true
		player.Cards.push(card)

		country, _ := game.Countries.getByName(card.Country)
		if country.Owner == turnOwner {
			country.Army += 2
			player.setClaimed(card)
		}
	}
	player.CanDrawCard = false
	claimUnclaimedCards(game)
}

func claimUnclaimedCards(game *Game) {
	turnOwner := game.TurnOwner
	player := game.Players.GetByColor(turnOwner)

	for _, card := range player.Cards.Cards {
		country, _ := game.Countries.getByName(card.Country)
		if !card.Claimed && country.Owner == player.Color {
			country.Army += 2
			player.setClaimed(card)
		}
	}
}

func throwDie() int {
	return rand.IntN(6) + 1
}

func updateArmies(attacker *Country, defender *Country, attackResult AttackIntent) {

	rollSize := min(len(attackResult.AttackerDice), len(attackResult.DefenderDice))

	attackResult.AttackerDice = attackResult.AttackerDice[0:rollSize]
	attackResult.DefenderDice = attackResult.DefenderDice[0:rollSize]

	wins := 0
	for i, die := range attackResult.AttackerDice {
		if die > attackResult.DefenderDice[i] {
			wins++
		}
	}

	defender.Army -= wins
	attacker.Army -= len(attackResult.DefenderDice) - wins

	if defender.Army <= 0 {
		if attacker.Army == 2 {
			attacker.Army -= 1
			defender.Army = 1
		} else {
			defender.Army = 0
		}
		defender.Owner = attacker.Owner
	}
}

func EndTurn(game *Game) error {
	turnIterator := getTurnIterator(game.Turn)
	var err error
	game.Turn, err = turnIterator.next()
	if err != nil {
		return err
	}

	return setUpTurn(game)
}

func setUpTurn(game *Game) error {
	turnOwner := game.Players.GetByColor(game.Turn.TurnOwner)
	if turnOwner != nil {
		if game.Turn.Action == Add3Army {
			turnOwner.Chips = 3
		}
		if game.Turn.Action == Add5Army {
			turnOwner.Chips = 5
		}
		if game.Turn.Action == AddArmy {
			playerCountries := game.Countries.getByColor(turnOwner.Color)
			chips := len(playerCountries) / 2
			if chips < 3 {
				turnOwner.Chips = 3
			} else {
				turnOwner.Chips = chips
			}
		}
		return nil
	}
	return errors.New("turn owner not found on turn setup")
}

func SetUpGame(players map[string]Color) *Game {
	game := NewGame(players)
	setUpTurn(game)
	return game
}

func isGameFinished(game *Game) bool {
	turnOwnerObjective := getObjective(game.Players.GetByColor(game.TurnOwner).Objective.getId())
	return turnOwnerObjective.isFulfilled(game, game.TurnOwner)
}
