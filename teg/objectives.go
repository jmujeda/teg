package teg

import (
	"fmt"
	"math/rand/v2"

	"go.mongodb.org/mongo-driver/bson"
)

type Objective interface {
	isFulfilled(game *Game, owner Color) bool
	getText() string
	isFulfillable(game *Game, owner Color) bool
	getId() objectiveId
	UnmarshalBSON(data []byte) error
}

type ObjectivesDeck struct {
	items []Objective
}

type objectiveId int

type conquestObjective struct {
	Countries map[ContinentName]int
	Text      string
	Id        objectiveId
}

type destroyObjective struct {
	Target Color
	Id     objectiveId
}

type commonObjective struct {
	Id objectiveId
}

type idObjective struct {
	Id objectiveId
}

func (o *idObjective) UnmarshalBSON(data []byte) error {
	type dup idObjective
	return bson.Unmarshal(data, (*dup)(o))
}

func (o *conquestObjective) UnmarshalBSON(data []byte) error {
	type dup conquestObjective
	return bson.Unmarshal(data, (*dup)(o))
}

func (o *destroyObjective) UnmarshalBSON(data []byte) error {
	type dup destroyObjective
	return bson.Unmarshal(data, (*dup)(o))
}

func (o *commonObjective) UnmarshalBSON(data []byte) error {
	type dup commonObjective
	return bson.Unmarshal(data, (*dup)(o))
}

func (o idObjective) getText() string {
	return ""
}

func (o idObjective) getId() objectiveId {
	return o.Id
}

func getObjective(id objectiveId) Objective {
	var obj Objective
	obj, ok := destroyObjectives[id]
	if ok {
		return obj
	}
	obj, ok = conquestObjectives[id]
	if ok {
		return obj
	}
	return NewCommonObjective()
}

func (d *ObjectivesDeck) assign(game *Game, owner Color, roundOrder []Color) Objective {
	pickIndex := rand.IntN(len(d.items))
	obj := d.items[pickIndex]
	d.items = append(d.items[:pickIndex], d.items[pickIndex+1:]...)

	if !obj.isFulfillable(game, owner) {
		for i, color := range roundOrder {
			if color == owner {
				if i == len(roundOrder)-1 {
					obj, _ = getDestroyObjective(roundOrder[0])
				} else {
					obj, _ = getDestroyObjective(roundOrder[i+1])
				}
			}
		}
	}
	return obj
}

func NewObjectivesDeck(includeDestroy bool) ObjectivesDeck {
	deck := ObjectivesDeck{}

	destroyObjectives := getDestroyObjectives()
	conquestObjectives := getConquestObjectives()
	var cap int
	if includeDestroy {
		cap = len(conquestObjectives) + len(destroyObjectives)
	} else {
		cap = len(conquestObjectives)
	}
	objectives := make([]Objective, len(conquestObjectives), cap)

	for i, d := range conquestObjectives {
		objectives[i] = d
	}
	if includeDestroy {
		dObjs := make([]Objective, len(destroyObjectives))
		for i, d := range destroyObjectives {
			dObjs[+i] = d
		}
		objectives = append(objectives, dObjs...)
	}

	deck.items = objectives
	return deck
}

func (o commonObjective) isFulfilled(game *Game, owner Color) bool {
	playerCountries := game.Countries.getByColor(owner)
	return len(playerCountries) >= 30
}

func (o conquestObjective) isFulfilled(game *Game, owner Color) bool {
	commonObjective := NewCommonObjective()
	if commonObjective.isFulfilled(game, owner) {
		return true
	}

	playerCountries := game.Countries.getByColor(owner)
	countByContinent := map[ContinentName]int{Africa: 0, SouthAmerica: 0, NorthAmerica: 0, Oceania: 0, Asia: 0, Europe: 0}
	for _, country := range playerCountries {
		countByContinent[country.Continent] += country.Army
	}

	for continent, countriesNeeded := range o.Countries {
		if countByContinent[continent] < countriesNeeded {
			return false
		}
	}
	return true
}

func (o destroyObjective) isFulfilled(game *Game, owner Color) bool {
	commonObjective := NewCommonObjective()
	if commonObjective.isFulfilled(game, owner) {
		return true
	}

	kills, ok := game.Kills[owner]
	if ok {
		for _, color := range kills {
			if color == o.Target {
				return true
			}
		}
	}

	return false
}

func (o commonObjective) isFulfillable(game *Game, owner Color) bool {
	return true
}

func (o conquestObjective) isFulfillable(game *Game, owner Color) bool {
	return true
}

func (o destroyObjective) isFulfillable(game *Game, owner Color) bool {
	for _, color := range game.Players.getColors() {
		if o.Target == color && color != owner {
			return true
		}
	}
	return false
}

func (o commonObjective) getText() string {
	return "Conquistar 30 países en cualquier lugar del mapa"
}

func (o conquestObjective) getText() string {
	return o.Text
}

func (o destroyObjective) getText() string {
	return "Destruir al ejército " + string(o.Target) + ", de ser imposible, al jugador de la derecha."
}

func (o commonObjective) getId() objectiveId {
	return o.Id
}

func (o conquestObjective) getId() objectiveId {
	return o.Id
}

func (o destroyObjective) getId() objectiveId {
	return o.Id
}

func NewCommonObjective() *commonObjective {
	return &commonObjective{Id: 0}
}

func getDestroyObjectives() []*destroyObjective {
	objectives := []*destroyObjective{}
	for _, obj := range destroyObjectives {
		objectives = append(objectives, obj)
	}
	return objectives
}

func getConquestObjectives() []*conquestObjective {
	objectives := []*conquestObjective{}
	for _, obj := range conquestObjectives {
		objectives = append(objectives, obj)
	}
	return objectives
}

func getDestroyObjective(target Color) (*destroyObjective, error) {
	for _, obj := range destroyObjectives {
		if obj.Target == target {
			return obj, nil
		}
	}
	return &destroyObjective{}, fmt.Errorf("missing destroy objective for target: %s", target)
}

var destroyObjectives = map[objectiveId]*destroyObjective{
	1: {Target: Black, Id: 1},
	2: {Target: Yellow, Id: 2},
	3: {Target: Green, Id: 3},
	4: {Target: Blue, Id: 4},
	5: {Target: Magenta, Id: 5},
	6: {Target: Red, Id: 6},
}
var conquestObjectives = map[objectiveId]*conquestObjective{
	7: {
		Countries: map[ContinentName]int{SouthAmerica: 4},
		Text:      "Conquistar áfrica, 5 países de américa del norte y 4 de europa.",
		Id:        7},
	8: {
		Countries: map[ContinentName]int{SouthAmerica: 5},
		Text:      "Conquistar 5 paises de sudamerica",
		Id:        8},
	9: {
		Countries: map[ContinentName]int{SouthAmerica: 6},
		Text:      "Conquistar 6 paises de sudamerica",
		Id:        9},
}

// var conquestObjectives = []ConquestObjective{
// 	{
// 		countries: map[ContinentName]int{Africa: 6, NorthAmerica: 5, Europe: 4},
// 		text:      "Conquistar áfrica, 5 países de américa del norte y 4 de europa.",
// 	},
// 	{
// 		countries: map[ContinentName]int{Asia: 15, SouthAmerica: 2},
// 		text:      "Conquistar asia y 2 países de américa del sur.",
// 	},
// }
