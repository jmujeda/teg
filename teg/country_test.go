package teg

import (
	"encoding/json"
	"math"
	"testing"

	"go.mongodb.org/mongo-driver/bson"
)

func Test_AssignPlayers(t *testing.T) {
	// user1 := UserId(1)
	// user2 := UserId(2)

	// roommates := []UserId{UserId(1), UserId(2)}

	countries := newCountriesList()

	playerList := newPlayerList(map[string]Color{"1": Black, "2": Red})

	countries.assignPlayers(playerList)

	for _, country := range countries.getAsArray() {
		if country.Owner == "" {
			t.Error("all countries should have an owner")
		}
	}

	p1 := playerList.GetByColor(Black)
	p2 := playerList.GetByColor(Red)
	if math.Abs(float64(len(countries.getByColor(p1.Color))-len(countries.getByColor(p2.Color)))) > 1 {
		t.Error("countries are not dealt equally")
	}

}

func Test_CountriesMarshal(t *testing.T) {
	countries := newCountriesList()
	bytes, err := json.Marshal(countries)

	if err != nil {
		t.Error(err)
	}

	countries2 := &Countries{
		byName: map[CountryName]*Country{},
		array:  []*Country{},
	}
	err = json.Unmarshal(bytes, countries2)

	if err != nil {
		t.Error(err)
	}

	for name, country := range countries.byName {
		countryCmp := countries2.byName[name]
		if countryCmp.Army != country.Army {
			t.Error("army should be the same after marshalling and unmarshalling")
		}
	}
}

func Test_CountriesMarshalBSON(t *testing.T) {
	countries := newCountriesList()
	bytes, err := bson.Marshal(countries)

	if err != nil {
		t.Error(err)
	}
	countries2 := &Countries{
		byName: map[CountryName]*Country{},
		array:  []*Country{},
	}
	err = bson.Unmarshal(bytes, countries2)

	if err != nil {
		t.Error(err)
	}
	for name, country := range countries.byName {
		countryCmp := countries2.byName[name]
		if countryCmp.Army != country.Army {
			t.Error("army should be the same after marshalling and unmarshalling")
		}

		for i, neighbour := range country.Neighbours {
			if countryCmp.Neighbours[i] != neighbour {
				t.Error("neighbours don't match")
			}
		}
	}
}
