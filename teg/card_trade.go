package teg

import (
	"errors"
)

type CardTrader interface {
	Add(card Card) error
	CanTrade() bool
	Remove(card Card) error
	HasCard(card Card) bool
}

type CardTrade struct {
	Selection []Card
}

func (c *CardTrade) HasCard(selectedCard Card) bool {
	for _, card := range c.Selection {
		if card.Country == selectedCard.Country {
			return true
		}
	}
	return false
}

func (c *CardTrade) Add(card Card) error {
	if !c.HasCard(card) {
		c.Selection = append(c.Selection, card)
		return nil
	}
	return errors.New("card already selected")
}

func (c *CardTrade) CanTrade() bool {
	if len(c.Selection) != 3 {
		return false
	}

	if c.Selection[0].Logo == c.Selection[1].Logo && c.Selection[1].Logo == c.Selection[2].Logo {
		return true
	}

	if c.Selection[0].Logo != c.Selection[1].Logo && c.Selection[1].Logo != c.Selection[2].Logo && c.Selection[0].Logo != c.Selection[2].Logo {
		return true
	}
	return false
}

func (c *CardTrade) Remove(card Card) error {
	if len(c.Selection) == 0 {
		return errors.New("no cards to remove")
	}
	for i, selected := range c.Selection {
		if selected.Country == card.Country {
			result := c.Selection[0:i]
			result = append(result, c.Selection[i+1:]...)
			c.Selection = result
			return nil
		}
	}
	return errors.New("card not present")
}
