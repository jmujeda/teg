package teg

import "testing"

func Test_Shuffle(t *testing.T) {
	countries := newCountriesList()
	countriesArray := countries.getAsArray()

	countriesCopy := make([]*Country, len(countriesArray))
	copy(countriesCopy, countriesArray)
	shuffled := Shuffle(countriesArray)

	if len(shuffled) != len(countriesArray) {
		t.Error("len should be the same")
	}

	matches := 0
	for i := range countriesArray {
		if shuffled[i].Name == countriesCopy[i].Name {
			matches++
		}
	}
	if matches == len(countriesArray) {
		t.Error("OG and shuffled are equal")
	}
}
