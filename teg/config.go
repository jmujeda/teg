package teg

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/MakeNowJust/heredoc/v2"
)

type Config struct {
	ClientConfig *UIConfig
	Version      string
	Logger       Logger
	ServerConfig *ServerConfig
}

type ServerConfig struct {
	Debug       bool
	LoadStateId string
}

type ButtonData struct {
	ImagePath
	Size   string
	Corner string
}

type ImagePath struct {
	Idle        string
	Hover       string
	Disabled    string
	Transparent string
}

type BoardNavButtonIcon struct {
	Objective ImagePath
	Chat      ImagePath
	Cards     ImagePath
	TradeIcon ImagePath
	Advance   ImagePath
	Regroup1  ImagePath
	Regroup2  ImagePath
	Regroup3  ImagePath
}

type BoardNavButton struct {
	ButtonData
	IconSize string
	Icons    BoardNavButtonIcon
}

type Size struct {
	Height int
	Width  int
}

type UIConfig struct {
	Window         Size
	Button         ButtonData
	BoardNavButton BoardNavButton
	TopIcon        ButtonData
	BoardStats     BoardStats
	Card           CardAsset
	Dice           Dice
	BottomNav      BottomNav
	Debug          bool
	Load           string
}

type BoardStats struct {
	Black   string
	Blue    string
	Green   string
	Red     string
	Yellow  string
	Magenta string
	Size
	PaddingX      float64
	PaddingY      float64
	IconSize      float64
	FontSize      int
	CountriesIcon string
	CardsIcon     string
	TradesIcon    string
	TurnOwnerIcon string
}

type CardAsset struct {
	Size
	Background string
	Cannon     string
	Ship       string
	Globe      string
}

type Dice struct {
	Dice_1 string
	Dice_2 string
	Dice_3 string
	Dice_4 string
	Dice_5 string
	Dice_6 string
	Size
	Spacing          int
	Padding          int
	ContainerPadding int
}

type BottomNav struct {
	Height int
}

func NewConfig(ctx context.Context, allowDebug bool) *Config {
	logger := NewLogger(allowDebug)

	configData := &UIConfig{}
	config := &Config{
		ClientConfig: configData,
		Version:      "",
		Logger:       logger,
		ServerConfig: &ServerConfig{},
	}

	if allowDebug {
		var err error

		file, err := os.Open("config.json")

		if err != nil {
			panic(err)
		}

		err = loadConfig(file, config)
		if err != nil {
			panic(err.Error())
		}
		go changesListener(ctx, file, config)
	} else {
		err := json.Unmarshal([]byte(getDefaultConfig()), config.ClientConfig)
		if err != nil {
			panic("error unmarshalling default config")
		}
	}

	return config
}

func loadConfig(file *os.File, config *Config) error {
	stats, err := os.Stat("config.json")
	if err != nil {
		panic(err)
	}
	size := stats.Size()
	bytes := make([]byte, size)
	_, err = file.ReadAt(bytes, 0)
	if err != nil {
		return err
	}

	err = json.Unmarshal(bytes, config)
	if err != nil {
		return err
	}
	config.Version = stats.ModTime().String()

	fmt.Println("LOAD CONFIG:")
	JsonPrint(config.ServerConfig)
	return nil
}

func changesListener(ctx context.Context, file *os.File, config *Config) {
	defer file.Close()
	for {
		select {
		case <-ctx.Done():
			return
		default:
			time.Sleep(time.Second * 1)
			stats, _ := os.Stat("config.json")
			modTime := stats.ModTime().String()
			if modTime != config.Version {
				err := loadConfig(file, config)
				if err != nil {
					panic(err.Error())
				}
			}
		}
	}
}

func getDefaultConfig() string {
	return heredoc.Doc(`{
    "Debug": true,
    "Window": {
        "Height": 960,
        "Width": 1280
    },
    "Button": {
        "Idle": "assets/panel-transparent-center-027d black3.png",
        "Hover": "assets/panel-027d black3.png",
        "Disabled": "assets/skull.png",
        "Transparent": "assets/transparent.png",
        "Size": "32",
        "Corner": "32"
    },
    "BoardNavButton": {
        "Idle": "assets/panel-transparent-center-027d.png",
        "Hover": "assets/panel-transparent-center-027d black3.png",
        "Disabled": "assets/panel-transparent-center-027d black3.png",
        "Size": "18",
        "Corner": "40",
        "IconSize": "128",
        "Icons": {
            "Objective": {
                "Idle": "assets/book_closed.png",
                "Hover": "assets/book_closed.png",
                "Disabled": "assets/skull.png"
            },
            "Chat": {
                "Idle": "assets/notepad_write.png",
                "Hover": "assets/notepad_write.png",
                "Disabled": "assets/skull.png"
            },
            "Cards": {
                "Idle": "assets/cards_seek_top.png",
                "Hover": "assets/cards_seek_top.png",
                "Disabled": "assets/skull.png"
            },
            "Advance": {
                "Idle": "assets/play.png",
                "Hover": "assets/play.png",
                "Disabled": "assets/skull.png"
            },
            "TradeIcon": {
                "Idle": "assets/card_rotate.png",
                "Hover": "assets/card_rotate.png",
                "Disabled": "assets/skull.png"
            },
            "Regroup1": {
                "Idle": "assets/regroup1.png",
                "Hover": "assets/regroup1.png",
                "Disabled": "assets/transparent.png"
            },
            "Regroup2": {
                "Idle": "assets/regroup2.png",
                "Hover": "assets/regroup2.png",
                "Disabled": "assets/transparent.png"
            },
            "Regroup3": {
                "Idle": "assets/regroup3.png",
                "Hover": "assets/regroup3.png",
                "Disabled": "assets/transparent.png"
            }
        }
    },
    "BottomNav": {
        "Height": 280
    },
    "BoardStats": {
        "Black": "assets/button_round_depth_gradient_black.png",
        "Blue": "assets/button_round_gradient_blue.png",
        "Green": "assets/button_round_gradient_green.png",
        "Red": "assets/button_round_gradient_red.png",
        "Yellow": "assets/button_round_gradient_yellow.png",
        "Magenta": "assets/button_square_depth_line.png",
        "TradesIcon": "assets/card_rotate.png",
        "CardsIcon":"assets/cards_fan_outline.png",
        "CountriesIcon": "assets/structure_house.png",
        "TurnOwnerIcon": "assets/hourglass.png",
        "Height": 100,
        "Width": 370,
        "PaddingX": 5,
        "PaddingY": 5,
        "FontSize": 14
    },
    "Card": {
        "Height": 200,
        "Width": 150,
        "Background": "assets/card1.png",
        "Cannon":"assets/cannon_card.png",
        "Ship": "assets/ship_card.png",
        "Globe": "assets/globe_card.png"
    },
    "Dice": {
        "Dice_1": "assets/dice_1.png",
        "Dice_2": "assets/dice_2.png",
        "Dice_3": "assets/dice_3.png",
        "Dice_4": "assets/dice_4.png",
        "Dice_5": "assets/dice_5.png",
        "Dice_6": "assets/dice_6.png",
        "Height": 64, 
        "Width": 64,
        "Spacing": 10,
        "Padding": 5,
        "ContainerPadding": 10
    },
    "State": {
        "Name": "roomName",
        "Password": "roomPass",
        "Game": {
          "Countries": {
            "Argentina": {
              "Neighbours": [
                "Brasil",
                "Chile",
                "Uruguay",
                "Peru"
              ],
              "Army": 4,
              "Owner": "azul",
              "Name": "Argentina",
              "Continent": "america del sur"
            },
            "Brasil": {
              "Neighbours": [
                "Argentina",
                "Uruguay",
                "Peru",
                "Colombia"
              ],
              "Army": 4,
              "Owner": "rojo",
              "Name": "Brasil",
              "Continent": "america del sur"
            },
            "Chile": {
              "Neighbours": [
                "Argentina",
                "Peru"
              ],
              "Army": 1,
              "Owner": "amarillo",
              "Name": "Chile",
              "Continent": "america del sur"
            },
            "Colombia": {
              "Neighbours": [
                "Peru",
                "Brasil"
              ],
              "Army": 1,
              "Owner": "verde",
              "Name": "Colombia",
              "Continent": "america del sur"
            },
            "Peru": {
              "Neighbours": [
                "Argentina",
                "Brasil",
                "Colombia",
                "Chile"
              ],
              "Army": 1,
              "Owner": "magenta",
              "Name": "Peru",
              "Continent": "america del sur"
            },
            "Uruguay": {
              "Neighbours": [
                "Argentina",
                "Brasil"
              ],
              "Army": 1,
              "Owner": "negro",
              "Name": "Uruguay",
              "Continent": "america del sur"
            }
          },
          "Players": {
            "amarillo": {
              "Cards": {
                "Cards": []
              },
              "Objective": "",
              "Color": "amarillo",
              "Chips": 0,
              "CanDrawCard": false,
              "CardTrades": 0
            },
            "azul": {
              "Cards": {
                "Cards": []
              },
              "Objective": "",
              "Color": "azul",
              "Chips": 0,
              "CanDrawCard": false,
              "CardTrades": 0
            },
            "magenta": {
              "Cards": {
                "Cards": []
              },
              "Objective": "",
              "Color": "magenta",
              "Chips": 0,
              "CanDrawCard": false,
              "CardTrades": 0
            },
            "negro": {
              "Cards": {
                "Cards": [
                  {
                    "Country": "Chile",
                    "Logo": "globe",
                    "Claimed": false
                  },
                  {
                    "Country": "Brasil",
                    "Logo": "ship",
                    "Claimed": false
                  },
                  {
                    "Country": "Argentina",
                    "Logo": "cannon",
                    "Claimed": false
                  },
                  {
                    "Country": "Uruguay",
                    "Logo": "ship",
                    "Claimed": false
                  }
                ]
              },
              "Objective": "Destruir al ejército amarillo, de ser imposible, al jugador de la derecha.",
              "Color": "negro",
              "Chips": 0,
              "CanDrawCard": false,
              "CardTrades": 0
            },
            "rojo": {
              "Cards": {
                "Cards": []
              },
              "Objective": "",
              "Color": "rojo",
              "Chips": 0,
              "CanDrawCard": false,
              "CardTrades": 0
            },
            "verde": {
              "Cards": {
                "Cards": []
              },
              "Objective": "",
              "Color": "verde",
              "Chips": 0,
              "CanDrawCard": false,
              "CardTrades": 0
            }
          },
          "Step": 1,
          "PlayerColor": "negro",
          "BoardChanged": false,
          "RegroupMove": {
            "Origin": "",
            "Destination": "",
            "Count": 0
          },
          "RestrictedRegroupment": {
            "Regroupments": {}
          },
          "CardTrade": {
            "Selection": null
          },
          "Status": "playing",
          "TurnOwner": "negro",
          "Action": "AddArmy",
          "RoundOrder": [
            "rojo",
            "magenta",
            "verde",
            "azul",
            "amarillo",
            "negro"
          ],
          "Attacker": "",
          "Defender": "",
          "AttackerDice": null,
          "DefenderDice": null,
          "Conquest": false
        },
        "Users": [
          {
            "Name": "bbbbbb",
            "Color": "amarillo"
          },
          {
            "Name": "Name of C",
            "Color": "rojo"
          },
          {
            "Name": "ATruncatedNameBecauseItsTooLong",
            "Color": "verde"
          },
          {
            "Name": "eee",
            "Color": "azul"
          },
          {
            "Name": "f",
            "Color": "magenta"
          },
          {
            "Name": "asd",
            "Color": "negro"
          }
        ]
      }
         
}`)

}
