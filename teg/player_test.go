package teg

import (
	"testing"

	"go.mongodb.org/mongo-driver/bson"
)

func Test_PlayerSerialize(t *testing.T) {
	cardArg := Card{Country: "Argentina"}
	cardBr := Card{Country: "Brasil"}
	p1Obj := destroyObjective{Target: Black}
	p2Obj := destroyObjective{Target: Yellow}
	p1Cards := Deck{Cards: []Card{cardArg}}
	p2Cards := Deck{Cards: []Card{cardBr}}
	p1 := player{Color: Black, Cards: p1Cards, Objective: &p1Obj}
	p2 := player{Color: Yellow, Cards: p2Cards, Objective: &p2Obj}

	p1Message := p1.Serialize(Black)
	if p1Message.Objective != p1.Objective.getText() {
		t.Error("error converting p1 objective")
	}

	if p1Message.Cards.Cards[0].Country != cardArg.Country {
		t.Error("error converting p1 cards")
	}

	p2Message := p2.Serialize(Black)
	if p2Message.Objective != "" {
		t.Error("error converting p2 objective")
	}
	if p2Message.Cards.Cards[0].Country != "" {
		t.Error("error converting p2 cards")
	}
}

func Test_PlayerListRemove(t *testing.T) {
	playerList := newPlayerList(map[string]Color{"1": Black, "2": Yellow})
	toRemove := playerList.GetByColor(Yellow)

	playerList.remove(toRemove)

	if playerList.GetByColor(Yellow) != nil {
		t.Error("should return nil for removed player")
	}
}

func Test_BSONPlayerList(t *testing.T) {
	players := newPlayerList(map[string]Color{"1": Black, "2": Yellow})

	obj := destroyObjectives[3]
	obj2 := conquestObjectives[7]
	blackPlayer := players.GetByColor(Black)
	yellowPlayer := players.GetByColor(Yellow)
	blackPlayer.Objective = obj
	yellowPlayer.Objective = obj2

	result := &playerList{}
	blackPlayer.Chips = 10
	yellowPlayer.Chips = 5
	bytes, err := bson.Marshal(players)

	if err != nil {
		t.Error(err)
	}
	err = bson.Unmarshal(bytes, result)

	if err != nil {
		t.Error(err)
	}

	if result.GetByColor(Black).Chips != blackPlayer.Chips {
		t.Error("chips don't match")
	}

	if result.GetByColor(Black).Objective.getId() != obj.Id {
		t.Error("objectives don't match")
	}

	if result.GetByColor(Yellow).Chips != yellowPlayer.Chips {
		t.Error("chips don't match")
	}

	if result.GetByColor(Yellow).Objective.getId() != obj2.Id {
		t.Error("objectives don't match")
	}

}
