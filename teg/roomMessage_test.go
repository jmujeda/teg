package teg

import "testing"

func Test_Clone(t *testing.T) {

	countries := map[CountryName]Country{
		"Argentina": {
			Army: 1,
		},
	}
	players := map[Color]playerMessage{
		Black: {
			Chips: 1,
		},
	}

	roomMessage := &RoomMessage{
		Game: GameMessage{
			Players:   players,
			Countries: countries,
		},
	}
	clone := roomMessage.Clone()

	argentina := clone.Game.Countries["Argentina"]
	argentina.Army = 2
	clone.Game.Countries["Argentina"] = argentina
	player := clone.Game.Players[Black]
	player.Chips = 2
	clone.Game.Players[Black] = player

	if clone.Game.Countries["Argentina"].Army == roomMessage.Game.Countries["Argentina"].Army {
		t.Error("army should be different")
	}

	if clone.Game.Players[Black].Chips == roomMessage.Game.Players[Black].Chips {
		t.Error("chips should be different")
	}
}
