package teg

import (
	"errors"
	"math/rand/v2"
)

type Logo string

const (
	Cannon Logo = "cannon"
	Ship   Logo = "ship"
	Globe  Logo = "globe"
)

type Card struct {
	Country CountryName
	Logo    Logo
	Claimed bool
}

type Deck struct {
	Cards []Card
}

func NewCountriesDeck() *Deck {
	deck := &Deck{
		[]Card{
			{"Argentina", Globe, false},
			{"Brasil", Ship, false},
			{"Uruguay", Globe, false},
			{"Peru", Cannon, false},
			{"Colombia", Globe, false},
			{"Chile", Globe, false},
		},
	}
	deck.shuffle()
	return deck
}

func (c *Card) mask() Card {
	return Card{}
}

func (d *Deck) setClaimed(claimed Card) {
	for i, card := range d.Cards {
		if card.Country == claimed.Country {
			d.Cards[i].Claimed = true
		}
	}
}

func (d *Deck) remove(needle Card) {
	result := []Card{}
	for i, card := range d.Cards {
		if card.Country == needle.Country {
			result = append(result, d.Cards[:i]...)
			result = append(result, d.Cards[i+1:]...)
		}
	}
	d.Cards = result
}

func (d *Deck) pickOne() (Card, error) {
	len := len(d.Cards)
	if len == 0 {
		return Card{}, errors.New("no cards left in deck")
	}
	pickIndex := rand.IntN(len)
	card := d.Cards[pickIndex]
	d.Cards = append(d.Cards[:pickIndex], d.Cards[pickIndex+1:]...)
	return card, nil
}

func (d *Deck) shuffle() {
	for i := range d.Cards {
		j := rand.IntN(i + 1)
		d.Cards[i], d.Cards[j] = d.Cards[j], d.Cards[i]
	}
}

func (d *Deck) push(card Card) {
	d.Cards = append(d.Cards, card)
}

func (d *Deck) mask() Deck {
	maskedDeck := Deck{Cards: []Card{}}
	for _, card := range d.Cards {
		maskedDeck.push(card.mask())
	}
	return maskedDeck
}

func (d *Deck) contains(needle Card) bool {
	for _, card := range d.Cards {
		if card.Country == needle.Country {
			return true
		}
	}
	return false
}
