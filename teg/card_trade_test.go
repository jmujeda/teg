package teg

import "testing"

func Test_CardTrade(t *testing.T) {
	trade := CardTrade{Selection: []Card{}}
	trade.Add(Card{Country: "Argentina", Logo: Cannon})

	if trade.CanTrade() {
		t.Error("should return false")
	}

	trade.Add(Card{Country: "Brasil", Logo: Cannon})

	if trade.CanTrade() {
		t.Error("should return false")
	}

	trade.Add(Card{Country: "Uruguay", Logo: Cannon})

	if !trade.CanTrade() {
		t.Error("should return true")
	}

	trade2 := CardTrade{Selection: []Card{}}
	trade2.Add(Card{Country: "Argentina", Logo: Cannon})
	trade2.Add(Card{Country: "Brasil", Logo: Cannon})

	trade2.Add(Card{Country: "Brasil", Logo: Ship})
	if trade2.CanTrade() {
		t.Error("should return false")
	}

	err := trade2.Add(Card{Country: "Brasil", Logo: Ship})
	if err == nil {
		t.Error("should return error when adding more than 3 cards")
	}

	trade3 := CardTrade{Selection: []Card{}}
	err = trade3.Remove(Card{Country: "Brasil", Logo: Cannon})
	if err == nil {
		t.Error("should return error if empty")
	}

	trade3.Add(Card{Country: "Argentina", Logo: Cannon})
	err = trade3.Remove(Card{Country: "Brasil", Logo: Cannon})

	if err == nil {
		t.Error("should return card not present")
	}
	err = trade3.Remove(Card{Country: "Argentina", Logo: Cannon})
	if err != nil {
		t.Error("should work")
	}

	trade3.Add(Card{Country: "Argentina", Logo: Cannon})
	trade3.Add(Card{Country: "Brasil", Logo: Ship})
	trade3.Add(Card{Country: "Uruguay", Logo: Cannon})
	trade3.Remove(Card{Country: "Brasil", Logo: Cannon})

	if trade3.Selection[0].Country != "Argentina" || trade3.Selection[1].Country != "Uruguay" {
		t.Error("error removing element")
	}
}
