# TEG

This project is a strategic, turn-based game inspired by the classic board game. Players aim to conquer territories, defeat opponents, and control the map.

## Planned Features (First Release)

- **Multiplayer Support**: Multiple games running simultaneously, with up to 6 players per game.
- **Reconnection Support**: Players can reconnect to a game in progress, ensuring no one is left out due to network issues.
- **Browser-Based Gameplay**: No need to install any software or third-party apps. Just open your browser and start playing!
## Future Features


- **Game Replay**: Watch recorded games to analyze strategies and review past matches.
- **Leaderboards**: Track player progress and see where you stand globally.
- **AI Opponents**: Challenge yourself against intelligent bots and improve your skills.



## About This Project


- **Server Application**: Written in Go, the server manages game state, handles multiplayer connections, and ensures synchronization across clients.
- **Client Application**: Also written in Go, the client is responsible for rendering the user interface and processing user input.
- **Wasmserver**: Players can run the client app directly in their browser thanks to WebAssembly (Wasm). The wasmserver compiles the Go client into WebAssembly, providing a seamless experience without any need for software installation.
- **Database**: Game data, including active games, player states, and history, is stored using a MariaDB database, ensuring persistence and reliability.

This architecture allows the game to scale, supports reconnection, and leverages Go’s performance for both the server and client applications.
