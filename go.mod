module gitlab.com/jmujeda/teg

go 1.22

require (
	github.com/codecat/go-libs v0.0.0-20210906174629-ffa6674c8e05
	github.com/coder/websocket v1.8.12
	github.com/ebitenui/ebitenui v0.6.0
	github.com/gofor-little/env v1.0.18
	github.com/golang-jwt/jwt/v5 v5.2.1
	github.com/google/go-cmp v0.6.0
	github.com/hajimehoshi/ebiten/v2 v2.7.10
	go.mongodb.org/mongo-driver v1.16.1
	golang.org/x/exp v0.0.0-20240808152545-0cdaa3abc0fa
	golang.org/x/image v0.19.0
	golang.org/x/time v0.6.0
)

require (
	github.com/MakeNowJust/heredoc/v2 v2.0.1
	github.com/ebitengine/gomobile v0.0.0-20240802043200-192f051f4fcc // indirect
	github.com/ebitengine/hideconsole v1.0.0 // indirect
	github.com/ebitengine/purego v0.7.1 // indirect
	github.com/fatih/color v1.16.0 // indirect
	github.com/go-text/typesetting v0.1.1-0.20240325125605-c7936fe59984 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/uuid v1.6.0
	github.com/jezek/xgb v1.1.1 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/montanaflynn/stats v0.7.1 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.2 // indirect
	github.com/xdg-go/stringprep v1.0.4 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	golang.org/x/crypto v0.22.0 // indirect
	golang.org/x/sync v0.8.0 // indirect
	golang.org/x/sys v0.24.0 // indirect
	golang.org/x/text v0.17.0 // indirect
)
