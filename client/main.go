package main

import (
	"context"
	"log"
	"os"

	"github.com/coder/websocket"
	"github.com/coder/websocket/wsjson"
	"github.com/hajimehoshi/ebiten/v2"
	"gitlab.com/jmujeda/teg/client/internal"
	"gitlab.com/jmujeda/teg/teg"
)

func main() {
	args := os.Args
	debug := false
	if len(args) > 1 && args[1] == "debug" {
		debug = true
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	config := teg.NewConfig(ctx, debug)

	c, _, err := websocket.Dial(ctx, "ws://127.0.0.1:8080", &websocket.DialOptions{})
	connection := internal.Connection{Conn: c}
	if err != nil {
		log.Print("error connecting ", err.Error())
		log.Panic()

		// ...
	}
	defer c.CloseNow()

	//Connect()
	ebiten.SetWindowSize(config.ClientConfig.Window.Width, config.ClientConfig.Window.Height)
	ebiten.SetWindowTitle("TEG")

	app := internal.NewApp(ctx, connection, config)
	router := internal.NewClientRouter()

	go func() {
		for {
			msg := teg.ClientMessage{}

			err = wsjson.Read(ctx, c, &msg)
			if err == nil {
				config.Logger.Debug(msg)
				command := router.GetCommand(msg.Action)
				if command != nil {
					command(ctx, msg.News, app)
				}
			} else {
				println("error reading message", err.Error())
				return
			}
		}
	}()

	if err := ebiten.RunGame(app); err != nil {
		log.Fatal(err)
	}

	c.Close(websocket.StatusNormalClosure, "")

}
