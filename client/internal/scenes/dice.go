package scenes

import (
	"math/rand"

	"github.com/ebitenui/ebitenui/image"
	"github.com/ebitenui/ebitenui/widget"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"gitlab.com/jmujeda/teg/teg"
)

type diceContainer string

const (
	root     diceContainer = "root"
	attacker diceContainer = "attacker"
	defender diceContainer = "defender"
)

type dice struct {
	model            *model
	containers       map[diceContainer]*widget.Container
	attackerDice     []*widget.Container
	defenderDice     []*widget.Container
	diceImages       map[int]*image.NineSlice
	transitioning    bool
	framesDuration   []int
	frameFreezeCount int
	iteration        int
}

func buildDice(config teg.Dice, model *model) *dice {
	rootContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewRowLayout(
			widget.RowLayoutOpts.Direction(widget.DirectionVertical),
			widget.RowLayoutOpts.Padding(widget.NewInsetsSimple(config.ContainerPadding)),
		)),
		//widget.ContainerOpts.BackgroundImage(image.NewNineSliceColor(color.White)),
	)
	attackerContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewRowLayout(
			widget.RowLayoutOpts.Direction(widget.DirectionHorizontal),
			widget.RowLayoutOpts.Padding(widget.NewInsetsSimple(config.Padding)),
			widget.RowLayoutOpts.Spacing(config.Spacing),
		)),
	)
	defenderContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewRowLayout(
			widget.RowLayoutOpts.Direction(widget.DirectionHorizontal),
			widget.RowLayoutOpts.Padding(widget.NewInsetsSimple(config.Padding)),
			widget.RowLayoutOpts.Spacing(config.Spacing),
		)),
	)

	containers := map[diceContainer]*widget.Container{}

	attackerDice := []*widget.Container{}
	defenderDice := []*widget.Container{}

	for i := 0; i < 3; i++ {
		diceContainer := widget.NewContainer()
		diceContainer.GetWidget().MinHeight = config.Height
		diceContainer.GetWidget().MinWidth = config.Width
		attackerDice = append(attackerDice, diceContainer)
		attackerContainer.AddChild(diceContainer)
	}

	for i := 0; i < 3; i++ {
		diceContainer := widget.NewContainer()
		diceContainer.GetWidget().MinHeight = config.Height
		diceContainer.GetWidget().MinWidth = config.Width
		defenderDice = append(defenderDice, diceContainer)
		defenderContainer.AddChild(diceContainer)
	}
	rootContainer.AddChild(attackerContainer)
	rootContainer.AddChild(defenderContainer)
	containers[root] = rootContainer
	containers[attacker] = attackerContainer
	containers[defender] = defenderContainer

	dice := &dice{
		model:            model,
		containers:       containers,
		diceImages:       buildDiceImages(config),
		attackerDice:     attackerDice,
		defenderDice:     defenderDice,
		transitioning:    true,
		framesDuration:   []int{0, 2, 2, 3, 3, 3, 3, 4, 4, 5, 5, 5, 8, 9, 10, 11, 12, 13, 18, 20},
		frameFreezeCount: 0,
		iteration:        0,
	}
	return dice
}

func (d *dice) showDice() {
	d.containers[root].AddChild(d.containers[attacker])
	d.containers[root].AddChild(d.containers[defender])
}

func (d *dice) hideDice() {
	d.getContainer().RemoveChildren()
}

func (d *dice) onBoardChange() {
	// d.model.State.Game.AttackIntent.AttackerDice = []int{6, 4, 3}
	// d.model.State.Game.AttackIntent.DefenderDice = []int{5, 5, 2}
	if (d.model.State.Game.Turn.Action == teg.Attack || d.model.State.Game.Turn.Action == teg.ConquestRegroup) && len(d.model.State.Game.AttackIntent.AttackerDice) > 0 {
		d.showDice()
		d.transitioning = true
		d.model.setRollingDice(true)
	} else {
		d.hideDice()
		d.transitioning = false
	}
}

func buildDiceImages(config teg.Dice) map[int]*image.NineSlice {
	result := map[int]*image.NineSlice{}
	width := [3]int{0, config.Width, 0}
	height := [3]int{0, config.Height, 0}

	dice_1, _, _ := ebitenutil.NewImageFromFile(config.Dice_1)
	dice_1_9s := image.NewNineSlice(dice_1, width, height)
	result[1] = dice_1_9s
	dice_2, _, _ := ebitenutil.NewImageFromFile(config.Dice_2)
	dice_2_9s := image.NewNineSlice(dice_2, width, height)
	result[2] = dice_2_9s
	dice_3, _, _ := ebitenutil.NewImageFromFile(config.Dice_3)
	dice_3_9s := image.NewNineSlice(dice_3, width, height)
	result[3] = dice_3_9s
	dice_4, _, _ := ebitenutil.NewImageFromFile(config.Dice_4)
	dice_4_9s := image.NewNineSlice(dice_4, width, height)
	result[4] = dice_4_9s
	dice_5, _, _ := ebitenutil.NewImageFromFile(config.Dice_5)
	dice_5_9s := image.NewNineSlice(dice_5, width, height)
	result[5] = dice_5_9s
	dice_6, _, _ := ebitenutil.NewImageFromFile(config.Dice_6)
	dice_6_9s := image.NewNineSlice(dice_6, width, height)
	result[6] = dice_6_9s

	return result
}

func (d *dice) getContainer() *widget.Container {
	return d.containers[root]
}

func (d *dice) update() {

	if d.transitioning {

		if d.frameFreezeCount == d.framesDuration[d.iteration] {

			for i := range d.model.State.Game.AttackIntent.AttackerDice {
				r := rand.Intn(5) + 1
				d.attackerDice[i].BackgroundImage = d.diceImages[r]
			}

			for i := range d.model.State.Game.AttackIntent.DefenderDice {
				r := rand.Intn(5) + 1
				d.defenderDice[i].BackgroundImage = d.diceImages[r]
			}
			d.iteration += 1
			d.frameFreezeCount = 0
		} else {
			d.frameFreezeCount++
		}

		if d.iteration == len(d.framesDuration) {
			d.transitioning = false
			d.model.setRollingDice(false)
			d.iteration = 0

			for i, result := range d.model.State.Game.AttackIntent.AttackerDice {
				d.attackerDice[i].BackgroundImage = d.diceImages[result]
			}

			for i, result := range d.model.State.Game.AttackIntent.DefenderDice {
				d.defenderDice[i].BackgroundImage = d.diceImages[result]
			}
		}

	}

}
