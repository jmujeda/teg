package scenes

import (
	"image/color"
	_ "image/png"

	"github.com/ebitenui/ebitenui"
	"github.com/ebitenui/ebitenui/image"
	"github.com/ebitenui/ebitenui/widget"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	common "gitlab.com/jmujeda/teg/client/internal/scenes/internal"
	"gitlab.com/jmujeda/teg/teg"
)

type newGameHandler func()
type loadGameHandler func()
type joinGameHandler func()

type Menu struct {
	init            bool
	ebitenUI        *ebitenui.UI
	NewGameHandler  newGameHandler
	LoadGameHandler loadGameHandler
	JoinGameHandler joinGameHandler
	config          *teg.Config
	version         string
}

func (m *Menu) SetConfig(config *teg.Config) {
	m.config = config
}

func (m *Menu) Draw(screen *ebiten.Image) {
	m.ebitenUI.Draw(screen)
}

func (m *Menu) Update() {
	if !m.init || m.config.Version != m.version {
		m.initialize()
	}
	m.ebitenUI.Update()
}

func (m *Menu) initialize() {
	m.version = m.config.Version
	img, _, _ := ebitenutil.NewImageFromFile("assets/background_merged.png")
	a := image.NewNineSliceSimple(img, img.Bounds().Dx(), img.Bounds().Dx())
	rootContainer := widget.NewContainer(
		// the container will use a plain color as its background

		widget.ContainerOpts.BackgroundImage(a),
		//the container will use an anchor layout to layout its single child widget
		widget.ContainerOpts.Layout(widget.NewAnchorLayout()),
	)

	innerContainer := widget.NewContainer(
		widget.ContainerOpts.BackgroundImage(image.NewNineSliceColor(color.NRGBA{255, 255, 0, 0})),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.MinSize(100, 100),
		),
		widget.ContainerOpts.Layout(widget.NewRowLayout(
			widget.RowLayoutOpts.Spacing(50),
			widget.RowLayoutOpts.Direction(widget.DirectionVertical),
		)),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(
				widget.AnchorLayoutData{
					HorizontalPosition: widget.AnchorLayoutPositionCenter,
					VerticalPosition:   widget.AnchorLayoutPositionCenter,
				},
			),
		),
	)

	newGameHandler := func(args *widget.ButtonClickedEventArgs) {
		m.NewGameHandler()
	}

	joinGameHandler := func(args *widget.ButtonClickedEventArgs) {
		m.JoinGameHandler()
	}

	loadGameHandler := func(args *widget.ButtonClickedEventArgs) {
		m.LoadGameHandler()
	}
	innerContainer.AddChild(common.CreateButton("Crear", newGameHandler, m.config.ClientConfig.Button))
	innerContainer.AddChild(common.CreateButton("Unirme", joinGameHandler, m.config.ClientConfig.Button))
	innerContainer.AddChild(common.CreateButton("Cargar", loadGameHandler, m.config.ClientConfig.Button))
	rootContainer.AddChild(innerContainer)

	m.ebitenUI = &ebitenui.UI{
		Container: rootContainer,
	}
	m.init = true
}
