package scenes

import (
	_ "image/png"

	"github.com/ebitenui/ebitenui"
	"github.com/ebitenui/ebitenui/image"
	"github.com/ebitenui/ebitenui/widget"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"gitlab.com/jmujeda/teg/client/internal/scenes/internal"
	"gitlab.com/jmujeda/teg/teg"
)

type loginUserHandler func(id string, password string)

type LoginScreen struct {
	init             bool
	ebitenUI         *ebitenui.UI
	LoginUserHandler loginUserHandler
	input            map[string]*widget.TextInput
	config           *teg.Config
	version          string
}

func (l *LoginScreen) SetConfig(config *teg.Config) {
	l.config = config
}

func (l *LoginScreen) Draw(screen *ebiten.Image) {
	l.ebitenUI.Draw(screen)
}

func (l *LoginScreen) Update() {
	if !l.init || l.config.Version != l.version {
		l.initialize()
	}
	l.ebitenUI.Update()
}

func (l *LoginScreen) initialize() {
	l.version = l.config.Version
	l.input = map[string]*widget.TextInput{}

	img, _, _ := ebitenutil.NewImageFromFile("assets/background_merged.png")
	a := image.NewNineSliceSimple(img, img.Bounds().Dx(), img.Bounds().Dx())
	rootContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewAnchorLayout()),
		widget.ContainerOpts.BackgroundImage(a),
	)

	userNameInput := internal.GetTextInput("usuario", false)
	passwordInput := internal.GetTextInput("contraseña", true)

	l.input["user"] = userNameInput
	l.input["password"] = passwordInput

	innerContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewRowLayout(
			widget.RowLayoutOpts.Direction(widget.DirectionVertical),
			//widget.RowLayoutOpts.Padding(widget.Insets{}),
			widget.RowLayoutOpts.Spacing(40),
		)),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(
				widget.AnchorLayoutData{
					HorizontalPosition: widget.AnchorLayoutPositionCenter,
					VerticalPosition:   widget.AnchorLayoutPositionCenter,
				},
			),
		),
	)

	handler := func(args *widget.ButtonClickedEventArgs) {
		id := l.input["user"].GetText()
		password := l.input["password"].GetText()
		l.LoginUserHandler(id, password)
	}

	innerContainer.AddChild(userNameInput)
	innerContainer.AddChild(passwordInput)
	innerContainer.AddChild(internal.CreateButton("Entrar", handler, l.config.ClientConfig.Button))
	rootContainer.AddChild(innerContainer)

	l.ebitenUI = &ebitenui.UI{
		Container: rootContainer,
	}
	l.init = true
}
