package scenes

import (
	"github.com/ebitenui/ebitenui/widget"
)

type navbarButton struct {
	container *widget.Container
	onUpdate  func()
}

func (n *navbarButton) getContainer() *widget.Container {
	return n.container
}

func (n *navbarButton) update() {
	n.onUpdate()
}

func (n *navbarButton) onBoardChange() {
}
