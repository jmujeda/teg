package scenes

import (
	"github.com/ebitenui/ebitenui/widget"
	"gitlab.com/jmujeda/teg/teg"
)

type chat struct {
	container *widget.Container
	state     *teg.RoomMessage
}

func buildChat(state *teg.RoomMessage) *chat {
	container := widget.NewContainer(
		//widget.ContainerOpts.BackgroundImage(image.NewNineSliceColor(color.Gray{Y: 50})),
		widget.ContainerOpts.Layout(widget.NewRowLayout()),
	)
	container.GetWidget().MinHeight = 50
	container.GetWidget().MinWidth = 50
	chat := &chat{
		container: container,
		state:     state,
	}
	return chat
}

func (c *chat) getContainer() *widget.Container {
	return c.container
}

func (c *chat) update() {

}

func (c *chat) onBoardChange() {

}
