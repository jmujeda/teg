package scenes

import (
	"fmt"
	"image/color"

	"github.com/ebitenui/ebitenui/image"
	"github.com/ebitenui/ebitenui/widget"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
	"gitlab.com/jmujeda/teg/client/internal/scenes/internal"
	"gitlab.com/jmujeda/teg/teg"
)

type playerStat struct {
	color     teg.Color
	name      string
	countries int
	cards     int
	trades    int
	turnOwner bool
}

type boardStats struct {
	stats       []playerStat
	config      teg.BoardStats
	fontFace    text.Face
	fontColor   color.Color
	rowHeight   int
	cellPadding widget.Insets
}

func newBoardStats(stats []playerStat, config teg.BoardStats, height int) *boardStats {
	//height = 240
	//rowHeight = 24
	rows := 7
	rowHeight := (height / rows) - int(config.PaddingY)*2
	//fontsize := rowHeight - int(config.PaddingY)
	fontsize := config.FontSize
	boardStats := &boardStats{
		stats:       stats,
		config:      config,
		rowHeight:   rowHeight,
		cellPadding: widget.Insets{Left: int(config.PaddingX), Top: int(config.PaddingY), Bottom: int(config.PaddingY), Right: int(config.PaddingX)},
	}
	face, _ := internal.LoadFont(float64(fontsize))
	boardStats.fontFace = face
	boardStats.fontColor = color.Black
	return boardStats
}

func (b *boardStats) build() *widget.Container {
	backgroundImg, _, _ := ebitenutil.NewImageFromFile("assets/panel-transparent-center-027d.png")
	background9s := image.NewNineSlice(backgroundImg, [3]int{32, 32, 32}, [3]int{32, 32, 32})
	hStretch := []bool{}
	for i := 0; i < len(b.stats); i++ {
		hStretch = append(hStretch, false)
	}
	frameContainer := widget.NewContainer(
		widget.ContainerOpts.BackgroundImage(background9s),
		widget.ContainerOpts.Layout(widget.NewAnchorLayout(
			widget.AnchorLayoutOpts.Padding(widget.NewInsetsSimple(15)),
		)),
	)
	container := widget.NewContainer(
		//widget.ContainerOpts.BackgroundImage(image.NewNineSliceColor(color.NRGBA{255, 255, 255, 100})),
		//widget.ContainerOpts.BackgroundImage(background9s),
		widget.ContainerOpts.Layout(
			widget.NewGridLayout(
				widget.GridLayoutOpts.Columns(6),
				widget.GridLayoutOpts.Stretch([]bool{false, false}, hStretch),
			),
		),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(
				widget.AnchorLayoutData{
					HorizontalPosition: widget.AnchorLayoutPositionCenter,
					VerticalPosition:   widget.AnchorLayoutPositionCenter,
				},
			),
		),
	)
	// container.GetWidget().MinHeight = 50
	container.GetWidget().MinWidth = b.config.Width
	container.AddChild(b.getEmptyCell())
	container.AddChild(b.getEmptyCell())
	container.AddChild(b.getIconCell(b.config.CountriesIcon))
	container.AddChild(b.getIconCell(b.config.CardsIcon))
	container.AddChild(b.getIconCell(b.config.TradesIcon))
	container.AddChild(b.getEmptyCell())

	//for _, playerStat := range b.stats {
	for i := 0; i < 6; i++ {
		if i >= len(b.stats) {
			container.AddChild(b.getEmptyCell())
			container.AddChild(b.getEmptyCell())
			container.AddChild(b.getEmptyCell())
			container.AddChild(b.getEmptyCell())
			container.AddChild(b.getEmptyCell())
			container.AddChild(b.getEmptyCell())

		} else {
			playerStat := b.stats[i]
			container.AddChild(b.getAvatarColumn(playerStat.color))
			container.AddChild(b.getStatColumn(playerStat.name, widget.AnchorLayoutPositionStart))
			container.AddChild(b.getStatColumn(fmt.Sprintf("%d", playerStat.countries), widget.AnchorLayoutPositionCenter))
			container.AddChild(b.getStatColumn(fmt.Sprintf("%d", playerStat.cards), widget.AnchorLayoutPositionCenter))
			container.AddChild(b.getStatColumn(fmt.Sprintf("%d", playerStat.trades), widget.AnchorLayoutPositionCenter))
			if playerStat.turnOwner {
				container.AddChild(b.getIconCell(b.config.TurnOwnerIcon))
			} else {
				container.AddChild(b.getEmptyCell())
			}
		}

	}
	frameContainer.AddChild(container)
	return frameContainer
}

func (b *boardStats) getStatColumn(stat string, align widget.AnchorLayoutPosition) *widget.Container {
	container := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewRowLayout()),
		widget.ContainerOpts.WidgetOpts(widget.WidgetOpts.LayoutData(
			widget.AnchorLayoutData{
				HorizontalPosition: align,
				VerticalPosition:   widget.AnchorLayoutPositionCenter,
			},
		),
		),
	)
	text := widget.NewLabel(widget.LabelOpts.Text(stat, b.fontFace, &widget.LabelColor{Idle: b.fontColor}))
	container.AddChild(text)
	wrapper := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewAnchorLayout(
			widget.AnchorLayoutOpts.Padding(b.cellPadding),
		)),
	)
	wrapper.GetWidget().MinHeight = b.rowHeight
	wrapper.GetWidget().MinWidth = b.rowHeight
	wrapper.AddChild(container)
	return wrapper
}

func (b *boardStats) getAvatarColumn(color teg.Color) *widget.Container {

	img, _, _ := ebitenutil.NewImageFromFile(b.getChipAssetByColor(color))
	wrapper := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewRowLayout(
			widget.RowLayoutOpts.Padding(b.cellPadding),
		)),
	)
	container := widget.NewContainer(
		widget.ContainerOpts.BackgroundImage(
			image.NewNineSlice(img, [3]int{0, 64, 0}, [3]int{0, 64, 0}),
		),
	)
	container.GetWidget().MinHeight = b.rowHeight
	container.GetWidget().MinWidth = b.rowHeight
	wrapper.AddChild(container)
	return wrapper
}

func (b *boardStats) getEmptyCell() *widget.Container {
	container := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewRowLayout(
			widget.RowLayoutOpts.Padding(b.cellPadding),
		),
		),
	)
	container.GetWidget().MinHeight = b.rowHeight
	container.GetWidget().MinWidth = b.rowHeight
	return container
}

func (b *boardStats) getIconCell(path string) *widget.Container {
	img, _, _ := ebitenutil.NewImageFromFile(path)

	wrapper := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewRowLayout(
			widget.RowLayoutOpts.Padding(b.cellPadding),
		)),
	)
	container := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewRowLayout()),
		widget.ContainerOpts.BackgroundImage(
			image.NewNineSlice(img, [3]int{0, 128, 0}, [3]int{0, 128, 0}),
		),
	)
	container.GetWidget().MinHeight = b.rowHeight
	container.GetWidget().MinWidth = b.rowHeight
	wrapper.AddChild(container)
	return wrapper
}

func (b *boardStats) getChipAssetByColor(color teg.Color) string {
	switch color {
	case teg.Black:
		return b.config.Black
	case teg.Blue:
		return b.config.Blue
	case teg.Green:
		return b.config.Green
	case teg.Red:
		return b.config.Red
	case teg.Yellow:
		return b.config.Yellow
	case teg.Magenta:
		return b.config.Magenta
	}
	return ""
}
