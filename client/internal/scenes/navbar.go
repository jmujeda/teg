package scenes

import (
	"github.com/ebitenui/ebitenui/widget"
	"gitlab.com/jmujeda/teg/teg"
)

type navbar struct {
	container            *widget.Container
	config               teg.BoardNavButton
	state                *teg.RoomMessage
	customWidgets        map[customWidgetName]customWidget
	lastStep             int
	onBoardChangeHandler func()
	onUpdateFn           func()
}

func buildNavbar(config teg.BoardNavButton, state *teg.RoomMessage) *navbar {
	container := widget.NewContainer(
		//widget.ContainerOpts.BackgroundImage(image.NewNineSliceColor(color.White)),
		widget.ContainerOpts.Layout(
			widget.NewRowLayout(
				widget.RowLayoutOpts.Direction(widget.DirectionHorizontal),
				widget.RowLayoutOpts.Padding(widget.Insets{Top: 0}),
			),
		),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(
				widget.AnchorLayoutData{
					HorizontalPosition: widget.AnchorLayoutPositionCenter,
					VerticalPosition:   widget.AnchorLayoutPositionCenter,
				},
			),
		),
	)

	navbar := &navbar{
		container:            container,
		config:               config,
		state:                state,
		lastStep:             state.Game.Step,
		customWidgets:        map[customWidgetName]customWidget{},
		onUpdateFn:           func() {},
		onBoardChangeHandler: func() {},
	}

	return navbar
}

func (n *navbar) addButton(button customWidget, name customWidgetName) {
	//button := internal.CreateBoardNavButton(handler, n.config, iconPath)
	n.customWidgets[name] = button
	n.container.AddChild(button.getContainer())
}

// func (n *navbar) addButton(handler func(args *widget.ButtonClickedEventArgs), iconPath teg.ImagePath) {
// 	button := internal.CreateBoardNavButton(handler, n.config, iconPath)
// 	n.container.AddChild(button)
// }

func (n *navbar) getContainer() *widget.Container {
	return n.container
}

func (n *navbar) update() {
	n.onUpdateFn()
	for _, customWidget := range n.customWidgets {
		customWidget.update()
	}
}

func (n *navbar) onBoardChange() {
	n.onBoardChangeHandler()
	for _, customWidget := range n.customWidgets {
		customWidget.onBoardChange()
	}
}
