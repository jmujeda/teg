package scenes

import (
	"fmt"

	"github.com/codecat/go-libs/log"
	"gitlab.com/jmujeda/teg/teg"
)

type model struct {
	State                   *teg.RoomMessage
	NewState                teg.RoomMessage
	BottomNavSelectedWidget customWidgetName
	RollingDice             bool
	teg.AttackIntent
}

func NewModel(State *teg.RoomMessage) *model {
	return &model{
		State:    State,
		NewState: State.Clone(),
	}
}

func (m *model) cloneState() teg.RoomMessage {
	newState := m.State.Clone()
	if m.State.Game.Turn.Action == teg.ConquestRegroup {
		countriesOld := m.NewState.Game.Countries
		newState.Game.Countries = countriesOld
	}
	return newState
}

func (m *model) setRollingDice(rolling bool) {
	m.RollingDice = rolling
	if !rolling && m.State.Game.Turn.Action == teg.ConquestRegroup {
		m.NewState.Game.Countries = m.State.Game.Countries
	}
}

func (m *model) setHasChanges(status bool) {
	m.State.Game.BoardChanged = status
}

func (m *model) hasChanges() bool {
	return m.State.Game.BoardChanged
}

func (m *model) isAddArmyTurn() bool {
	return m.State.Game.Turn.Action == teg.AddArmy || m.State.Game.Turn.Action == teg.Add3Army || m.State.Game.Turn.Action == teg.Add5Army
}

func (m *model) isPlayerTurnOwner() bool {
	return m.State.Game.TurnOwner == m.State.Game.PlayerColor
}

func (m *model) reinforcementsToAdd() int {
	ownColor := m.State.Game.PlayerColor
	ownPlayer, ok := m.NewState.Game.Players[ownColor]
	if ok {
		return ownPlayer.Chips
	}
	log.Debug("player not found")
	return 0
}

func (m *model) getTurnState() turnState {
	if m.State.Game.TurnOwner != m.State.Game.PlayerColor {
		return opponentTurnState
	}
	if m.isAddArmyTurn() {
		return addArmyState
	}
	if m.State.Game.Turn.Action == teg.Attack {
		return attackState
	}
	if m.State.Game.Turn.Action == teg.ConquestRegroup {
		return conquestRegroupState
	}
	if m.State.Game.Turn.Action == teg.Regroup {
		return regroupState
	}
	if m.State.Game.Turn.Action == teg.DrawCard {
		return drawCardState
	}
	return ""
}

func (m *model) addArmy(selectedCountry teg.CountryName) {
	if !m.isPlayerTurnOwner() {
		log.Debug("not player turn")
		return
	}

	ownColor := m.State.Game.PlayerColor
	ownPlayer, ok := m.NewState.Game.Players[ownColor]
	if !ok {
		log.Debug("missing player in player list")
	}

	country, ok := m.NewState.Game.Countries[selectedCountry]
	if !ok {
		log.Debug("clicked country not in country list")
	} else {
		if ownPlayer.Chips > 0 && country.Owner == ownColor {
			ownPlayer.Chips--
			country.Army++
			m.NewState.Game.Players[ownColor] = ownPlayer
			m.NewState.Game.Countries[selectedCountry] = country
		}
	}
}

func (m *model) removeArmy(selectedCountry teg.CountryName) {
	if !m.isPlayerTurnOwner() {
		log.Debug("not player turn")
		return
	}
	ownColor := m.State.Game.PlayerColor
	ownPlayer, ok := m.NewState.Game.Players[ownColor]
	if !ok {
		log.Debug("missing player in player list")
	}
	country, ok := m.NewState.Game.Countries[selectedCountry]
	if !ok {
		log.Debug("clicked country not in country list")
	} else {
		if country.Owner == ownColor && country.Army > m.State.Game.Countries[selectedCountry].Army {
			ownPlayer.Chips++
			country.Army--
			m.NewState.Game.Players[ownColor] = ownPlayer
			m.NewState.Game.Countries[selectedCountry] = country
		}
	}
}

func (m *model) setAttacker(selectedCountry teg.CountryName) {
	if !m.isPlayerTurnOwner() {
		log.Debug("not player turn")
		return
	}
	ownColor := m.State.Game.PlayerColor
	country, ok := m.NewState.Game.Countries[selectedCountry]
	if !ok {
		log.Debug("clicked country not in country list")
	} else {
		if country.Owner == ownColor && country.Army > 1 {
			m.AttackIntent.Attacker = country.Name
			fmt.Println("set attacker: ", country.Name)
		}
	}
}

func (m *model) setDefender(selectedCountry teg.CountryName) bool {
	if !m.isPlayerTurnOwner() {
		log.Debug("not player turn")
		return false
	}
	ownColor := m.State.Game.PlayerColor
	country, ok := m.NewState.Game.Countries[selectedCountry]
	if !ok {
		log.Debug("clicked country not in country list")
		return false
	}

	if m.AttackIntent.Attacker != "" && country.Owner != ownColor && country.IsNeighbour(m.State.Game.Countries[m.AttackIntent.Attacker]) {
		m.AttackIntent.Defender = country.Name
		m.NewState.Game.AttackIntent = m.AttackIntent
		fmt.Println("set defender: ", country.Name)
		return true
	}
	log.Debug("clicked country is not a valid defender")
	return false
}

func (m *model) resetAttackIntent() {
	if !m.isPlayerTurnOwner() {
		log.Debug("not player turn")
		return
	}
	m.AttackIntent = teg.AttackIntent{}
}

func (m *model) setRegroupOrigin(selectedCountry teg.CountryName) {
	if !m.isPlayerTurnOwner() {
		log.Debug("not player turn")
		return
	}
	ownColor := m.State.Game.PlayerColor
	country, ok := m.NewState.Game.Countries[selectedCountry]
	if !ok {
		log.Debug("clicked country not in country list")
	} else {
		army, _ := m.State.Game.GetArmy(country.Name)
		log.Debug("left click: %s army is %d", country.Name, army)
		if country.Owner == ownColor && army > 1 {
			m.NewState.Game.RegroupMove.Origin = country.Name
		}
	}
}

func (m *model) setRegroupDestination(selectedCountry teg.CountryName) bool {
	if !m.isPlayerTurnOwner() {
		log.Debug("not player turn")
		return false
	}
	ownColor := m.State.Game.PlayerColor
	country, ok := m.State.Game.Countries[selectedCountry]
	if !ok {
		log.Debug("clicked country not in country list")
		return false
	} else {
		if m.NewState.Game.RegroupMove.Origin != "" && country.Owner == ownColor && country.IsNeighbour(m.State.Game.Countries[m.NewState.Game.RegroupMove.Origin]) {
			println("mouse released on ", country.Name)
			m.NewState.Game.RegroupMove.Destination = country.Name
			return true
		} else {
			m.NewState.Game.RegroupMove.Origin = ""
		}
	}
	return false
}

func (m *model) resetRegroup() {
	if !m.isPlayerTurnOwner() {
		log.Debug("not player turn")
		return
	}
	m.NewState.Game.RegroupMove = teg.RegroupMove{}
}

func (m *model) canTrade() bool {
	return m.NewState.Game.CardTrade.CanTrade()
}

func (m *model) resetTrade() bool {
	return m.NewState.Game.CardTrade.CanTrade()
}

func (m *model) addTradeCard(card teg.Card) {
	m.NewState.Game.CardTrade.Add(card)
}

func (m *model) removeTradeCard(card teg.Card) {
	m.NewState.Game.CardTrade.Remove(card)
}
