package scenes

import (
	"github.com/codecat/go-libs/log"
	"gitlab.com/jmujeda/teg/teg"
)

type turnState string

const (
	opponentTurnState    turnState = "opponentTurn"
	addArmyState         turnState = "addArmy"
	attackState          turnState = "attack"
	conquestRegroupState turnState = "conquestRegroup"
	regroupState         turnState = "regroup"
	drawCardState        turnState = "drawCard"
)

type controller interface {
	inputHandler
	onBoardChange()
	selectBottomNavWidget(widget customWidgetName)
	cardTradeController
	navbarController
}

type inputHandler interface {
	handleInput(mouseEvent mouseEvent, selectedCountry teg.CountryName)
}

type turnInputHandler interface {
	handleInput(model *model, mouseEvent mouseEvent, selectedCountry teg.CountryName)
}

type boardController struct {
	turnController     turnInputHandler
	turnControllers    map[turnState]turnInputHandler
	model              *model
	AdvanceTurnHandler func(action teg.Action, roomMessage teg.RoomMessage)
}

func (b boardController) selectBottomNavWidget(widget customWidgetName) {
	b.model.BottomNavSelectedWidget = widget
}

func (c boardController) handleInput(mouseEvent mouseEvent, selectedCountry teg.CountryName) {
	c.turnController.handleInput(c.model, mouseEvent, selectedCountry)
}

func (c boardController) advanceTurn(action teg.Action, state teg.RoomMessage) {
	c.AdvanceTurnHandler(action, state)
}

func (c boardController) canTrade() bool {
	return c.model.canTrade()
}

func (c boardController) add(card teg.Card) {
	c.model.addTradeCard(card)
}

func (c boardController) remove(card teg.Card) {
	c.model.NewState.Game.CardTrade.Remove(card)
}

func (c boardController) isCardSelected(card teg.Card) bool {
	return c.model.NewState.Game.CardTrade.HasCard(card)
}

func NewBoardController(model *model, advanceTurn func(action teg.Action, roomMessage teg.RoomMessage)) *boardController {
	turnControllers := map[turnState]turnInputHandler{
		opponentTurnState:    opponentTurnController{},
		addArmyState:         addArmyController{},
		attackState:          attackController{AdvanceTurnHandler: advanceTurn},
		conquestRegroupState: conquestRegroup{},
		regroupState:         regroupController{AdvanceTurnHandler: advanceTurn},
		drawCardState:        drawCardController{},
	}
	boardController := &boardController{model: model, turnControllers: turnControllers, AdvanceTurnHandler: advanceTurn}
	boardController.turnController = turnControllers[model.getTurnState()]
	return boardController
}

func (c *boardController) onBoardChange() {
	var ok bool
	c.turnController, ok = c.turnControllers[c.model.getTurnState()]
	if !ok {
		log.Debug("boardController update returning opponentTurn controller")
		c.turnController = c.turnControllers[opponentTurnState]
	}
}

type opponentTurnController struct{}
type addArmyController struct{}
type attackController struct {
	AdvanceTurnHandler func(action teg.Action, roomMessage teg.RoomMessage)
}
type conquestRegroup struct{}
type drawCardController struct{}
type regroupController struct {
	AdvanceTurnHandler func(action teg.Action, roomMessage teg.RoomMessage)
}

func (c opponentTurnController) handleInput(model *model, mouseEvent mouseEvent, selectedCountry teg.CountryName) {
}

func (c drawCardController) handleInput(model *model, mouseEvent mouseEvent, selectedCountry teg.CountryName) {
}

func (c addArmyController) handleInput(model *model, mouseEvent mouseEvent, selectedCountry teg.CountryName) {
	if mouseEvent == leftClick {
		model.addArmy(selectedCountry)
	} else if mouseEvent == rightClick {
		model.removeArmy(selectedCountry)
	}
}

func (c attackController) handleInput(model *model, mouseEvent mouseEvent, selectedCountry teg.CountryName) {

	//b.animateAdvanceButton()
	if mouseEvent == leftClick {
		model.setAttacker(selectedCountry)
	} else if mouseEvent == leftRelease {
		ok := model.setDefender(selectedCountry)
		if ok {
			c.AdvanceTurnHandler(model.NewState.Game.Action, model.NewState)
		}
		model.resetAttackIntent()
	} else if mouseEvent != leftPressed {
		model.resetAttackIntent()
	}
}

func (c conquestRegroup) handleInput(model *model, mouseEvent mouseEvent, selectedCountry teg.CountryName) {
}

func (c regroupController) handleInput(model *model, mouseEvent mouseEvent, selectedCountry teg.CountryName) {

	if mouseEvent == leftClick {
		model.setRegroupOrigin(selectedCountry)
	} else if mouseEvent == leftRelease {
		ok := model.setRegroupDestination(selectedCountry)
		if ok {
			c.AdvanceTurnHandler(model.NewState.Game.Action, model.NewState)
		}
	} else if mouseEvent != leftPressed {
		model.resetRegroup()
	}
}
