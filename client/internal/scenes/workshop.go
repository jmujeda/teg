package scenes

import (
	"fmt"
	_ "image/png"

	"github.com/codecat/go-libs/log"
	"github.com/ebitenui/ebitenui"
	"github.com/ebitenui/ebitenui/image"
	"github.com/ebitenui/ebitenui/widget"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	teg "gitlab.com/jmujeda/teg/teg"
)

type Workshop struct {
	init      bool
	ebitenUI  *ebitenui.UI
	State     *teg.RoomMessage
	Step      int
	Points    []Point
	Argentina *CountryPolygon
	Countries []string
	Current   int
	mode      string
	config    *teg.Config
}

func (w *Workshop) SetConfig(config *teg.Config) {
	w.config = config
}

type Point struct {
	X, Y int
}

func (w *Workshop) Draw(screen *ebiten.Image) {
	w.ebitenUI.Draw(screen)
}

func (w *Workshop) Update() {
	if !w.init {
		w.Points = []Point{}
		w.Countries = []string{"Argentina", "Brasil", "Chile", "Peru", "Uruguay", "Colombia"}
		w.initialize()
		w.Current = 0

		// POLYGON FOR COUNTRY BORDERS AND CENTER FOR COUNTRY CHIPS POSITION
		w.mode = "CENTER"
		//w.mode = "POLYGON"
		println("select", w.mode, "for", fmt.Sprint(w.Countries))
	}

	w.workshop()

	w.ebitenUI.Update()

}

func (w *Workshop) initialize() {
	path := "assets/pepiboard3.png"
	img, _, err := ebitenutil.NewImageFromFile(path)

	if err != nil {
		log.Error("error loading image from file %s: %s", path, err)
	}
	a := image.NewNineSliceSimple(img, img.Bounds().Dx(), img.Bounds().Dx())

	rootContainer := widget.NewContainer(
		widget.ContainerOpts.BackgroundImage(a),
		widget.ContainerOpts.Layout(
			widget.NewRowLayout(
				widget.RowLayoutOpts.Direction(widget.DirectionHorizontal),
				widget.RowLayoutOpts.Padding(widget.NewInsetsSimple(100)),
				widget.RowLayoutOpts.Spacing(10),
			),
		),
	)
	w.ebitenUI = &ebitenui.UI{
		Container: rootContainer,
	}
	w.init = true
}

func (w *Workshop) workshop() {

	if w.mode == "POLYGON" {
		if w.Current < len(w.Countries) {
			if inpututil.IsMouseButtonJustPressed(ebiten.MouseButton0) {
				x, y := ebiten.CursorPosition()
				point := Point{x, y}
				w.Points = append(w.Points, point)
			}
			if inpututil.IsMouseButtonJustPressed(ebiten.MouseButton2) {
				w.Argentina = &CountryPolygon{vertices: w.Points}
				println(fmt.Sprintf("\"%s\": {", w.Countries[w.Current]))
				println("vertices: []Point{")
				for i := range w.Points {
					println("{X: ", w.Points[i].X, ", Y: ", w.Points[i].Y, "},")
				}
				println("},")
				println("},")

				w.Points = []Point{}
				w.Current++
			}
		} else {
			if inpututil.IsMouseButtonJustPressed(ebiten.MouseButton0) {
				x, y := ebiten.CursorPosition()
				if inCountry(x, y, *w.Argentina) {
					println("ADENTRO")
				} else {
					println("AFUERA")
				}
			}
		}
	} else if w.mode == "CENTER" {
		if inpututil.IsMouseButtonJustPressed(ebiten.MouseButton0) {
			x, y := ebiten.CursorPosition()
			println(fmt.Sprintf("\"%s\": {X: %d, Y: %d},", w.Countries[w.Current], x, y))
			w.Current++

		}
	}

}

func inCountry(x int, y int, country CountryPolygon) bool {
	vertices := country.vertices
	inside := false

	for i, j := 0, len(vertices)-1; i < len(vertices); i, j = i+1, i {
		if (vertices[i].Y > y) != (vertices[j].Y > y) && x < (vertices[j].X-vertices[i].X)*(y-vertices[i].Y)/(vertices[j].Y-vertices[i].Y)+vertices[i].X {
			inside = !inside
		}
	}
	return inside
}

type CountryPolygon struct {
	vertices []Point
}

func (c CountryPolygon) GetXVertex() []int {
	xs := make([]int, len(c.vertices))
	for _, point := range c.vertices {
		xs = append(xs, point.X)
	}
	return xs
}

func (c CountryPolygon) GetYVertex() []int {
	ys := make([]int, len(c.vertices))
	for _, point := range c.vertices {
		ys = append(ys, point.Y)
	}
	return ys
}

// int pnpoly(int nvert, float *vertx, float *verty, float testx, float testy)
// {
//   int i, j, c = 0;
//   for (i = 0, j = nvert-1; i < nvert; j = i++) {
//     if ( ((verty[i]>testy) != (verty[j]>testy)) &&
//      (testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
//        c = !c;
//   }
//   return c;
// }

// Argentina
// 332 682
// 349 560
// 396 572
// 387 638
// 345 688
