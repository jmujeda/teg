package internal

import (
	"bytes"
	"image/color"
	"log"
	"strconv"

	"github.com/ebitenui/ebitenui/image"
	"github.com/ebitenui/ebitenui/widget"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
	"gitlab.com/jmujeda/teg/teg"
	"golang.org/x/image/font/gofont/goregular"
)

func CreateButton(text string, handler widget.ButtonClickedHandlerFunc, buttonData teg.ButtonData) *widget.Button {
	face, _ := LoadFont(30)
	buttonImage, _ := LoadButtonImage(buttonData)
	clr := uint8(0x33)
	button := widget.NewButton(
		widget.ButtonOpts.Image(buttonImage),

		// specify the button's text, the font face, and the color
		widget.ButtonOpts.Text(text, face, &widget.ButtonTextColor{
			Idle:     color.NRGBA{clr, clr, clr, 0xff},
			Disabled: color.NRGBA{0xdf, 0xf4, 0xff, 0xff},
		}),
		widget.ButtonOpts.WidgetOpts(widget.WidgetOpts.MinSize(200, 60)),

		// specify that the button's text needs some padding for correct display
		// widget.ButtonOpts.TextPadding(widget.Insets{
		// 	Left:   50,
		// 	Right:  50,
		// 	Top:    10,
		// 	Bottom: 10,
		// }),

		// add a handler that reacts to clicking the button
		widget.ButtonOpts.ClickedHandler(handler),
	)
	if button == nil {
		println("ERROR")
	}
	return button
}

func CreateBoardNavButton(handler widget.ButtonClickedHandlerFunc, buttonSpec teg.BoardNavButton, iconPath teg.ImagePath) *widget.Container {

	buttonContainer := widget.NewContainer(
		widget.ContainerOpts.AutoDisableChildren(),
		widget.ContainerOpts.Layout(widget.NewAnchorLayout()),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(
				widget.AnchorLayoutData{
					HorizontalPosition: widget.AnchorLayoutPositionCenter,
					VerticalPosition:   widget.AnchorLayoutPositionCenter,
				},
			),
		),
	)

	buttonImage, _ := LoadButtonImage(buttonSpec.ButtonData)

	button := widget.NewButton(
		widget.ButtonOpts.Image(buttonImage),
		// add a handler that reacts to clicking the button
		widget.ButtonOpts.ClickedHandler(handler),
	)

	iconContainer := widget.NewContainer(
		widget.ContainerOpts.AutoDisableChildren(),
		widget.ContainerOpts.Layout(widget.NewRowLayout()),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(
				widget.AnchorLayoutData{
					HorizontalPosition: widget.AnchorLayoutPositionCenter,
					VerticalPosition:   widget.AnchorLayoutPositionCenter,
				},
			),
		),
	)

	iconData := teg.ButtonData{
		ImagePath: teg.ImagePath{
			Idle:     iconPath.Idle,
			Hover:    iconPath.Hover,
			Disabled: iconPath.Disabled,
		},
		Size:   buttonSpec.IconSize,
		Corner: "0",
	}
	icon, _ := LoadButtonImage(iconData)
	button2 := widget.NewButton(
		widget.ButtonOpts.Image(icon),
	)
	iconContainer.AddChild(button2)

	buttonContainer.AddChild(button)
	buttonContainer.AddChild(iconContainer)
	return buttonContainer
}

func LoadButtonImage(buttonData teg.ButtonData) (*widget.ButtonImage, error) {

	idleImg, _, err := ebitenutil.NewImageFromFile(buttonData.Idle)

	if err != nil {
		return nil, err
	}
	hoverImg, _, err := ebitenutil.NewImageFromFile(buttonData.Hover)
	if err != nil {
		return nil, err
	}

	disabledImg, _, err := ebitenutil.NewImageFromFile(buttonData.Disabled)
	if err != nil {
		return nil, err
	}
	var width [3]int
	var height [3]int

	size, _ := strconv.Atoi(buttonData.Size)
	corner, _ := strconv.Atoi(buttonData.Corner)

	width = [3]int{corner, size, corner}
	height = [3]int{corner, size, corner}

	idle9s := image.NewNineSlice(idleImg, width, height)
	hover9s := image.NewNineSlice(hoverImg, width, height)
	disabled9s := image.NewNineSlice(disabledImg, width, height)

	//	idle := image.NewNineSliceColor(color.NRGBA{R: 130, G: 130, B: 140, A: 255})
	//hover := image.NewNineSliceColor(color.NRGBA{R: 100, G: 100, B: 120, A: 255})

	//pressed := image.NewNineSliceColor(color.NRGBA{R: 100, G: 100, B: 120, A: 255})
	//disabled := image.NewNineSliceColor(color.NRGBA{R: 100, G: 100, B: 120, A: 150})

	return &widget.ButtonImage{
		Idle:     idle9s,
		Hover:    hover9s,
		Pressed:  hover9s,
		Disabled: disabled9s,
	}, nil
}

func LoadFont(size float64) (text.Face, error) {
	s, err := text.NewGoTextFaceSource(bytes.NewReader(goregular.TTF))
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	return &text.GoTextFace{
		Source: s,
		Size:   size,
	}, nil
}
