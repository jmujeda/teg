package scenes

import (
	"image/color"

	"github.com/ebitenui/ebitenui"
	"github.com/ebitenui/ebitenui/image"
	"github.com/ebitenui/ebitenui/widget"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"gitlab.com/jmujeda/teg/client/internal/scenes/internal"
	teg "gitlab.com/jmujeda/teg/teg"
)

type leaveLobbyHandler func()
type startGameHandler func()

type Room struct {
	init              bool
	ebitenUI          *ebitenui.UI
	LeaveLobbyHandler leaveLobbyHandler
	StartGameHandler  startGameHandler
	State             *teg.RoomMessage
	list              *widget.List
	listCount         int
	config            *teg.Config
	version           string
}

func (r *Room) SetConfig(config *teg.Config) {
	r.config = config
}

func (r *Room) Draw(screen *ebiten.Image) {
	r.ebitenUI.Draw(screen)
}

func (r *Room) Update() {
	if !r.init || r.config.Version != r.version {
		r.initialize()
	}
	playersCount := len(r.State.Users)
	//log.Info(fmt.Sprint(r.State.GetRoommatesList()))
	if r.listCount != playersCount {
		r.list.SetEntries(r.getListEntries())
		r.listCount = playersCount
	}
	r.ebitenUI.Update()
}

func (r *Room) initialize() {
	r.version = r.config.Version
	//game.Ui.Container.RemoveChildren()
	img, _, _ := ebitenutil.NewImageFromFile("assets/background_merged.png")
	a := image.NewNineSliceSimple(img, img.Bounds().Dx(), img.Bounds().Dx())

	rootContainer := widget.NewContainer(
		widget.ContainerOpts.BackgroundImage(a),
		widget.ContainerOpts.Layout(widget.NewAnchorLayout()),
	)

	r.list = internal.GetListWidget(nil)

	innerContainer := widget.NewContainer(
		widget.ContainerOpts.BackgroundImage(image.NewNineSliceColor(color.NRGBA{255, 255, 0, 0})),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.MinSize(100, 100),
		),
		widget.ContainerOpts.Layout(widget.NewRowLayout(
			widget.RowLayoutOpts.Spacing(50),
			widget.RowLayoutOpts.Direction(widget.DirectionHorizontal),
		)),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(
				widget.AnchorLayoutData{
					HorizontalPosition: widget.AnchorLayoutPositionCenter,
					VerticalPosition:   widget.AnchorLayoutPositionCenter,
				},
			),
		),
	)
	buttonsContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(
			widget.NewRowLayout(
				widget.RowLayoutOpts.Direction(widget.DirectionVertical),
				widget.RowLayoutOpts.Spacing(50),
			),
		),
	)
	startGameHandler := func(args *widget.ButtonClickedEventArgs) {
		r.StartGameHandler()
	}

	leaveLobbyHandler := func(args *widget.ButtonClickedEventArgs) {
		r.LeaveLobbyHandler()
	}
	b := internal.CreateButton("Comenzar", startGameHandler, r.config.ClientConfig.Button)

	buttonsContainer.AddChild(b)
	buttonsContainer.AddChild(internal.CreateButton("Salir", leaveLobbyHandler, r.config.ClientConfig.Button))
	innerContainer.AddChild(r.list)
	innerContainer.AddChild(buttonsContainer)
	rootContainer.AddChild(innerContainer)

	r.ebitenUI = &ebitenui.UI{
		Container: rootContainer,
	}
	r.init = true
}

func (r *Room) getListEntries() []any {
	players := r.State.GetRoommatesList()
	entries := make([]any, 0, len(players))
	for _, name := range players {
		entries = append(entries, internal.ListWidgetItem{Label: name})
	}
	return entries
}

// func loadFont(size float64) (font.Face, error) {
// 	ttfFont, err := truetype.Parse(goregular.TTF)
// 	if err != nil {
// 		return nil, err
// 	}

// 	return truetype.NewFace(ttfFont, &truetype.Options{
// 		Size:    size,
// 		DPI:     72,
// 		Hinting: font.HintingFull,
// 	}), nil
// }
