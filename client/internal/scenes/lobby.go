package scenes

import (
	"image/color"

	"github.com/ebitenui/ebitenui"
	"github.com/ebitenui/ebitenui/image"
	"github.com/ebitenui/ebitenui/widget"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"gitlab.com/jmujeda/teg/client/internal/scenes/internal"
	"gitlab.com/jmujeda/teg/teg"
)

type createGameHandler func(roomName string, password string)
type enterGameHandler func(roomName string, password string)
type backToMenuHandler func()

type Lobby struct {
	init              bool
	Create            bool
	input             map[string]*widget.TextInput
	ebitenUI          *ebitenui.UI
	CreateGameHandler createGameHandler
	EnterGameHandler  enterGameHandler
	BackToMenuHandler backToMenuHandler
	config            *teg.Config
	version           string
}

func (l *Lobby) SetConfig(config *teg.Config) {
	l.config = config
}

func (l *Lobby) Draw(screen *ebiten.Image) {
	l.ebitenUI.Draw(screen)
}

func (l *Lobby) Update() {
	if !l.init || l.version != l.config.Version {
		l.initialize()
	}
	l.ebitenUI.Update()
}

func (l *Lobby) initialize() {
	l.version = l.config.Version
	l.input = map[string]*widget.TextInput{}

	img, _, _ := ebitenutil.NewImageFromFile("assets/background_merged.png")
	a := image.NewNineSliceSimple(img, img.Bounds().Dx(), img.Bounds().Dx())
	rootContainer := widget.NewContainer(
		// the container will use a plain color as its background

		widget.ContainerOpts.BackgroundImage(a),
		//the container will use an anchor layout to layout its single child widget
		widget.ContainerOpts.Layout(widget.NewAnchorLayout()),
	)

	innerContainer := widget.NewContainer(
		widget.ContainerOpts.BackgroundImage(image.NewNineSliceColor(color.NRGBA{255, 255, 0, 0})),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.MinSize(100, 100),
		),
		widget.ContainerOpts.Layout(widget.NewRowLayout(
			widget.RowLayoutOpts.Spacing(50),
			widget.RowLayoutOpts.Direction(widget.DirectionVertical),
		)),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(
				widget.AnchorLayoutData{
					HorizontalPosition: widget.AnchorLayoutPositionCenter,
					VerticalPosition:   widget.AnchorLayoutPositionCenter,
				},
			),
		),
	)

	textInput := internal.GetTextInput("Sala", false)
	secureTextInput := internal.GetTextInput("Contraseña", true)

	createGameHandler := func(args *widget.ButtonClickedEventArgs) {
		roomName := l.input["roomName"].GetText()
		password := l.input["roomPassword"].GetText()
		l.CreateGameHandler(roomName, password)
	}

	enterGameHandler := func(args *widget.ButtonClickedEventArgs) {
		roomName := l.input["roomName"].GetText()
		password := l.input["roomPassword"].GetText()
		l.EnterGameHandler(roomName, password)
	}

	backToMenuHandler := func(args *widget.ButtonClickedEventArgs) {
		l.BackToMenuHandler()
	}

	l.input["roomName"] = textInput
	l.input["roomPassword"] = secureTextInput
	innerContainer.AddChild(textInput)
	innerContainer.AddChild(secureTextInput)
	var enterLobby *widget.Button
	if l.Create {
		enterLobby = internal.CreateButton("Crear", createGameHandler, l.config.ClientConfig.Button)
	} else {
		enterLobby = internal.CreateButton("Unirme", enterGameHandler, l.config.ClientConfig.Button)
	}

	back := internal.CreateButton("Volver", backToMenuHandler, l.config.ClientConfig.Button)

	innerContainer.AddChild(enterLobby)
	innerContainer.AddChild(back)
	rootContainer.AddChild(innerContainer)

	l.ebitenUI = &ebitenui.UI{
		Container: rootContainer,
	}

	l.init = true
}
