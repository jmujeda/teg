package scenes

import (
	"fmt"

	"github.com/ebitenui/ebitenui/widget"
	"gitlab.com/jmujeda/teg/client/internal/scenes/internal"
	"gitlab.com/jmujeda/teg/teg"
)

type navbarController interface {
	advanceTurn(action teg.Action, state teg.RoomMessage)
	canTrade() bool
}

type navbarFactory struct {
	model                   *model
	config                  *teg.Config
	controller              navbarController
	bottomNavWidgetSelector func(widgetName customWidgetName)
}

func newNavbarFactory(model *model, config *teg.Config, controller navbarController, bottomNavWidgetSelector func(widgetName customWidgetName)) navbarFactory {
	return navbarFactory{
		model:                   model,
		config:                  config,
		controller:              controller,
		bottomNavWidgetSelector: bottomNavWidgetSelector,
	}
}

func (f navbarFactory) createTopNavbar() *navbar {
	advanceContainer := internal.CreateBoardNavButton(f.endTurnCommand(), f.config.ClientConfig.BoardNavButton, f.config.ClientConfig.BoardNavButton.Icons.Advance)
	advanceButton := &navbarButton{
		container: advanceContainer,
		onUpdate: func() {
			if f.model.State.Game.TurnOwner != f.model.State.Game.PlayerColor {
				advanceContainer.GetWidget().Disabled = true
			} else {
				// if f.model.isAddArmyTurn() && f.model.reinforcementsToAdd() > 0 {
				// }
				advanceContainer.GetWidget().Disabled = false
			}
		},
	}
	objectiveContainer := internal.CreateBoardNavButton(f.selectBottomNavWidget(objectiveWidget), f.config.ClientConfig.BoardNavButton, f.config.ClientConfig.BoardNavButton.Icons.Objective)
	objectiveButton := &navbarButton{
		container: objectiveContainer,
		onUpdate:  func() {},
	}

	chatContainer := internal.CreateBoardNavButton(f.selectBottomNavWidget(chatWidget), f.config.ClientConfig.BoardNavButton, f.config.ClientConfig.BoardNavButton.Icons.Chat)
	chatButton := &navbarButton{
		container: chatContainer,
		onUpdate:  func() {},
	}

	cardsContainer := internal.CreateBoardNavButton(f.selectBottomNavWidget(cardsWidget), f.config.ClientConfig.BoardNavButton, f.config.ClientConfig.BoardNavButton.Icons.Cards)
	cardsButton := &navbarButton{
		container: cardsContainer,
		onUpdate:  func() {},
	}

	tradeContainer := internal.CreateBoardNavButton(f.tradeCards(), f.config.ClientConfig.BoardNavButton, f.config.ClientConfig.BoardNavButton.Icons.TradeIcon)
	tradeButton := &navbarButton{
		container: tradeContainer,
		onUpdate: func() {
			if f.controller.canTrade() {
				tradeContainer.GetWidget().Disabled = false
			} else {
				tradeContainer.GetWidget().Disabled = true
			}
		},
	}

	navbar := buildNavbar(f.config.ClientConfig.BoardNavButton, f.model.State)
	navbar.addButton(chatButton, chatWidget)
	navbar.addButton(objectiveButton, objectiveWidget)
	navbar.addButton(cardsButton, cardsWidget)
	navbar.addButton(tradeButton, tradeWidget)
	navbar.addButton(advanceButton, advanceWidget)

	return navbar
}

func (f navbarFactory) selectBottomNavWidget(widgetName customWidgetName) func(args *widget.ButtonClickedEventArgs) {
	return func(args *widget.ButtonClickedEventArgs) {
		f.bottomNavWidgetSelector(widgetName)
	}
}

func (f navbarFactory) createConquestRegroupNavbar() *navbar {

	r1Container := internal.CreateBoardNavButton(f.regroupHandler(1), f.config.ClientConfig.BoardNavButton, f.config.ClientConfig.BoardNavButton.Icons.Regroup1)
	regroup1Button := &navbarButton{
		container: r1Container,
		onUpdate:  func() {},
	}
	r2Container := internal.CreateBoardNavButton(f.regroupHandler(2), f.config.ClientConfig.BoardNavButton, f.config.ClientConfig.BoardNavButton.Icons.Regroup2)
	regroup2Button := &navbarButton{
		container: r2Container,
		onUpdate:  func() {},
	}
	r3Container := internal.CreateBoardNavButton(f.regroupHandler(3), f.config.ClientConfig.BoardNavButton, f.config.ClientConfig.BoardNavButton.Icons.Regroup3)
	regroup3Button := &navbarButton{
		container: r3Container,
		onUpdate:  func() {},
	}

	conquestRegroupNavbar := buildNavbar(f.config.ClientConfig.BoardNavButton, f.model.State)
	conquestRegroupNavbar.addButton(regroup1Button, regroup1Widget)
	conquestRegroupNavbar.addButton(regroup2Button, regroup2Widget)
	conquestRegroupNavbar.addButton(regroup3Button, regroup3Widget)
	conquestRegroupNavbar.onUpdateFn = func() {
		if f.model.State.Game.Turn.Action == teg.ConquestRegroup && !f.model.RollingDice {
			conquestRegroupNavbar.container.GetWidget().Visibility = widget.Visibility_Show
		} else {
			conquestRegroupNavbar.container.GetWidget().Visibility = widget.Visibility_Hide
		}
	}

	conquestRegroupNavbar.container.GetWidget().Visibility = widget.Visibility_Hide

	return conquestRegroupNavbar
}

func (f navbarFactory) endTurnCommand() func(args *widget.ButtonClickedEventArgs) {
	fmt.Println("endturn")
	return func(args *widget.ButtonClickedEventArgs) {
		var action teg.Action
		switch f.model.State.Game.Action {
		case teg.Add3Army, teg.Add5Army, teg.AddArmy:
			action = teg.AddArmy
		default:
			action = teg.EndTurnAction
		}
		f.controller.advanceTurn(action, f.model.NewState)
	}
}

func (f navbarFactory) tradeCards() func(args *widget.ButtonClickedEventArgs) {
	return func(args *widget.ButtonClickedEventArgs) {
		f.controller.advanceTurn(teg.Trade, f.model.NewState)
	}
}

func (f navbarFactory) regroupHandler(amount int) func(args *widget.ButtonClickedEventArgs) {
	return func(args *widget.ButtonClickedEventArgs) {
		f.model.NewState.Game.RegroupMove = teg.RegroupMove{
			Origin:      f.model.NewState.Game.RegroupMove.Origin,
			Destination: f.model.NewState.Game.RegroupMove.Destination,
			Count:       amount,
		}
		f.controller.advanceTurn(f.model.NewState.Game.Action, f.model.NewState)
	}
}
