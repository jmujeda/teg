package scenes

import (
	"errors"
	"fmt"
	"image/color"
	_ "image/png"

	"github.com/codecat/go-libs/log"
	"github.com/ebitenui/ebitenui"
	"github.com/ebitenui/ebitenui/image"
	"github.com/ebitenui/ebitenui/input"
	"github.com/ebitenui/ebitenui/widget"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
	"gitlab.com/jmujeda/teg/client/internal/scenes/internal"
	"gitlab.com/jmujeda/teg/teg"
)

type customWidget interface {
	getContainer() *widget.Container
	update()
	onBoardChange()
}

type customWindow interface {
	add(adder windowAdder)
	update()
	onBoardChange()
}

type mouseEvent string

const (
	leftClick   mouseEvent = "leftClick"
	leftPressed mouseEvent = "leftPressed"
	rightClick  mouseEvent = "rightClick"
	leftRelease mouseEvent = "leftRelease"
)

type customWidgetName string

const (
	chatWidget            customWidgetName = "chatWidget"
	objectiveWidget       customWidgetName = "objectiveWidget"
	cardsWidget           customWidgetName = "cardsWidget"
	navbarWidget          customWidgetName = "navbarWidget"
	advanceWidget         customWidgetName = "advanceWidget"
	diceWidget            customWidgetName = "diceWidget"
	moveArmyWidget        customWidgetName = "moveArmyWidget"
	conquestRegroupWidget customWidgetName = "conquestRegroupWidget"
	regroup1Widget        customWidgetName = "regroup1Widget"
	regroup2Widget        customWidgetName = "regroup2Widget"
	regroup3Widget        customWidgetName = "regroup3Widget"
	tradeWidget           customWidgetName = "tradeWidget"
)

var chipSize = float64(30)

var fontSize = 20

var face, _ = internal.LoadFont(float64(fontSize))

type Board struct {
	init          bool
	ebitenUI      *ebitenui.UI
	Borders       map[teg.CountryName]CountryPolygon
	Centers       map[teg.CountryName]Point
	chips         map[teg.CountryName]*ebiten.Image
	boardImage    *ebiten.Image
	config        *teg.Config
	version       string
	customWidgets map[customWidgetName]customWidget
	Controller    controller
	Model         *model
	customWindows []customWindow
}

func (b *Board) SetConfig(config *teg.Config) {
	b.config = config
}

func (b *Board) Draw(screen *ebiten.Image) {
	b.drawCountryChips(screen)
	b.ebitenUI.Draw(screen)
}

func (b *Board) drawCountryChips(screen *ebiten.Image) {

	img := b.boardImage

	op := &ebiten.DrawImageOptions{}
	scale := float64(1)
	op.GeoM.Scale(scale, scale)
	screen.DrawImage(img, op)
	for name, country := range b.Model.NewState.Game.Countries {
		armyColor := teg.ColorRGBA[teg.Color(country.Owner)]

		//chip := ebiten.NewImage(chipSize, chipSize)
		chip := b.chips[country.Name]
		chip.Fill(color.Transparent)

		army, _ := b.Model.NewState.Game.GetArmy(name)
		position := b.Centers[name]

		chipOp := &ebiten.DrawImageOptions{}

		chipOp.GeoM.Translate(float64(position.X)*scale, float64(position.Y)*scale)

		var fontColor color.Color
		if teg.Color(country.Owner) == teg.Black {
			fontColor = color.White
		} else {
			fontColor = color.Black
		}
		vector.DrawFilledCircle(chip, float32(chipSize/2), float32(chipSize/2), 12, armyColor, false)

		color := ebiten.ColorScale{}

		opts := &text.DrawOptions{
			DrawImageOptions: ebiten.DrawImageOptions{
				ColorScale: color,
			},
		}

		opts.ColorScale.ScaleWithColor(fontColor)
		opts.GeoM.Translate(chipSize/3, chipSize/6)
		text.Draw(chip, fmt.Sprint(army), face, opts)

		screen.DrawImage(chip, chipOp)
	}
}

func (b *Board) Update() {
	if !b.init || b.config.Version != b.version {
		b.initialize()
		b.Borders = loadPolygons()
		b.Centers = loadCenters()
		b.chips = loadChips()
		path := "assets/pepiboard3.png"
		img, _, err := ebitenutil.NewImageFromFile(path)
		if err != nil {
			log.Error("error loading image from file %s: %s", path, err)
		} else {
			b.boardImage = img
		}
	}

	if b.Model.hasChanges() {
		b.Model.NewState = b.Model.cloneState()
		b.Controller.onBoardChange()
		for _, widget := range b.customWidgets {
			widget.onBoardChange()
		}
	}

	for _, widget := range b.customWidgets {
		widget.update()
	}

	for _, customWindow := range b.customWindows {
		customWindow.add(b.ebitenUI)
	}

	if b.Model.State.Game.Status == teg.Finished {
		println("FINISHED")
	}
	var mouseEvent mouseEvent
	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButton0) {
		mouseEvent = leftClick
	} else if inpututil.IsMouseButtonJustPressed(ebiten.MouseButton2) {
		mouseEvent = rightClick
	} else if inpututil.IsMouseButtonJustReleased(ebiten.MouseButton0) {
		mouseEvent = leftRelease
	} else if input.MouseButtonPressed(ebiten.MouseButton0) {
		mouseEvent = leftPressed
	}
	clickedCountry, err := getClickedCountry(b.Borders)
	if mouseEvent != "" && err == nil {
		b.Controller.handleInput(mouseEvent, clickedCountry)
	}
	b.ebitenUI.Update()
	b.Model.setHasChanges(false)
}

func (b *Board) getPlayerStats() []playerStat {
	stats := []playerStat{}

	for _, color := range b.Model.State.Game.RoundOrder {
		playerStat := playerStat{}
		playerStat.color = color
		playerStat.cards = len(b.Model.State.Game.Players[color].Cards.Cards)
		playerStat.trades = b.Model.State.Game.Players[color].CardTrades
		countries := 0

		for _, country := range b.Model.State.Game.Countries {
			if country.Owner == color {
				countries++
			}
		}
		playerStat.countries = countries
		playerStat.name = b.Model.State.Game.Players[color].Id
		if len(playerStat.name) > 25 {
			playerStat.name = playerStat.name[0:25]
		}

		playerStat.turnOwner = b.Model.State.Game.TurnOwner == color
		stats = append(stats, playerStat)

	}

	return stats
}

func (b *Board) initialize() {

	backgroundImg, _, _ := ebitenutil.NewImageFromFile("assets/panel-transparent-center-027d.png")
	//backgroundImg, _, _ := ebitenutil.NewImageFromFile("assets/panel-border-027.png")

	background9s := image.NewNineSlice(backgroundImg, [3]int{32, 32, 32}, [3]int{32, 32, 32})

	bottomNavReplaceableContainer := widget.NewContainer(
		widget.ContainerOpts.BackgroundImage(background9s),
		widget.ContainerOpts.Layout(widget.NewAnchorLayout()),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(
				widget.GridLayoutData{
					MaxHeight: b.config.ClientConfig.BottomNav.Height,
				},
			),
		),
	)

	var bottomNavWidgetSelector func(name customWidgetName) = func(name customWidgetName) {
		replaceChild(bottomNavReplaceableContainer, b.customWidgets[name].getContainer())
	}

	navbarFactory := newNavbarFactory(b.Model, b.config, b.Controller, bottomNavWidgetSelector)
	topNavbar := navbarFactory.createTopNavbar()
	conquestRegroupNavbar := navbarFactory.createConquestRegroupNavbar()

	b.customWidgets = map[customWidgetName]customWidget{}
	cardsCustomWidget := buildCards(b.Controller, b.config, b.Model.State, b.config.ClientConfig.Window.Height-40, bottomNavWidgetSelector)
	b.customWidgets[diceWidget] = buildDice(b.config.ClientConfig.Dice, b.Model)
	b.customWidgets[cardsWidget] = cardsCustomWidget
	b.customWidgets[chatWidget] = buildChat(b.Model.State)
	b.customWidgets[objectiveWidget] = buildObjective(b.Model.State, b.config.ClientConfig.BottomNav.Height-40)
	b.customWidgets[navbarWidget] = topNavbar
	b.customWidgets[conquestRegroupWidget] = conquestRegroupNavbar
	playerStats := b.getPlayerStats()
	boardStats := newBoardStats(playerStats, b.config.ClientConfig.BoardStats, b.config.ClientConfig.Window.Height/4)
	b.version = b.config.Version
	rootContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(
			widget.NewGridLayout(
				widget.GridLayoutOpts.Columns(1),
				widget.GridLayoutOpts.Stretch([]bool{true}, []bool{false, true, false}),
			),
		),
	)
	b.customWindows = []customWindow{}
	b.customWindows = append(b.customWindows, cardsCustomWidget)
	topButtonsAnchor := widget.NewContainer(
		widget.ContainerOpts.Layout(
			widget.NewAnchorLayout(),
		),
		widget.ContainerOpts.WidgetOpts(widget.WidgetOpts.LayoutData(
			widget.GridLayoutData{HorizontalPosition: widget.GridLayoutPositionCenter},
		)),
	)

	middleContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(
			widget.NewAnchorLayout(),
		),
	)
	middleInnerContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(
			widget.NewGridLayout(
				widget.GridLayoutOpts.Columns(1),
				widget.GridLayoutOpts.Spacing(0, 10),
				widget.GridLayoutOpts.Padding(widget.NewInsetsSimple(10)),
			),
		),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(
				widget.AnchorLayoutData{
					HorizontalPosition: widget.AnchorLayoutPositionStart,
					VerticalPosition:   widget.AnchorLayoutPositionEnd,
				},
			),
		),
	)

	middleInnerContainer.AddChild(b.customWidgets[diceWidget].getContainer())
	middleInnerContainer.AddChild(b.customWidgets[conquestRegroupWidget].getContainer())
	middleContainer.AddChild(middleInnerContainer)

	bottomNavContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(
			widget.NewGridLayout(
				widget.GridLayoutOpts.Columns(2),
				widget.GridLayoutOpts.Stretch([]bool{false, true}, []bool{false}),
			),
		),
		//widget.ContainerOpts.BackgroundImage(image.NewNineSliceColor(color.NRGBA{0, 0, 100, 10})),
	)

	bottomNavReplaceableContainer.AddChild(b.customWidgets[chatWidget].getContainer())
	bottomNavContainer.AddChild(boardStats.build())
	bottomNavContainer.AddChild(bottomNavReplaceableContainer)

	topButtonsAnchor.AddChild(b.customWidgets[navbarWidget].getContainer())

	//bottomNavAnchorer.AddChild(bottomNavContainer)

	rootContainer.AddChild(topButtonsAnchor)
	rootContainer.AddChild(middleContainer)
	//rootContainer.AddChild(b.myCardsContainer)
	rootContainer.AddChild(bottomNavContainer)
	b.ebitenUI = &ebitenui.UI{
		Container: rootContainer,
	}

	b.init = true

}

func replaceChild(container *widget.Container, child *widget.Container) {
	container.RemoveChildren()
	container.AddChild(child)
}

func loadChips() map[teg.CountryName]*ebiten.Image {
	chips := map[teg.CountryName]*ebiten.Image{}
	centers := loadCenters()
	for name := range centers {
		chips[name] = ebiten.NewImage(int(chipSize), int(chipSize))
	}
	return chips
}

func loadCenters() map[teg.CountryName]Point {
	centers := map[teg.CountryName]Point{
		"Argentina": {X: 440, Y: 565},
		"Brasil":    {X: 500, Y: 470},
		"Chile":     {X: 390, Y: 573},
		"Peru":      {X: 410, Y: 488},
		"Uruguay":   {X: 490, Y: 540},
		"Colombia":  {X: 406, Y: 435},
	}
	return centers

}

func loadPolygons() map[teg.CountryName]CountryPolygon {
	borders := map[teg.CountryName]CountryPolygon{
		"Argentina": {
			vertices: []Point{
				{X: 413, Y: 649},
				{X: 432, Y: 530},
				{X: 480, Y: 538},
				{X: 468, Y: 571},
				{X: 482, Y: 600},
				{X: 428, Y: 656},
			},
		},
		"Brasil": {
			vertices: []Point{
				{X: 472, Y: 529},
				{X: 451, Y: 473},
				{X: 465, Y: 424},
				{X: 579, Y: 469},
				{X: 555, Y: 537},
				{X: 495, Y: 520},
			},
		},
		"Chile": {
			vertices: []Point{
				{X: 402, Y: 531},
				{X: 429, Y: 531},
				{X: 412, Y: 649},
				{X: 399, Y: 646},
			},
		},
		"Peru": {
			vertices: []Point{
				{X: 402, Y: 528},
				{X: 465, Y: 524},
				{X: 445, Y: 474},
				{X: 384, Y: 488},
			},
		},
		"Uruguay": {
			vertices: []Point{
				{X: 487, Y: 586},
				{X: 473, Y: 567},
				{X: 490, Y: 534},
				{X: 532, Y: 534},
				{X: 530, Y: 569},
			},
		},
		"Colombia": {
			vertices: []Point{
				{X: 439, Y: 463},
				{X: 381, Y: 479},
				{X: 369, Y: 447},
				{X: 429, Y: 413},
				{X: 462, Y: 425},
			},
		},
	}
	return borders
}

func getClickedCountry(countries map[teg.CountryName]CountryPolygon) (teg.CountryName, error) {
	x, y := ebiten.CursorPosition()

	for name, country := range countries {
		if inCountry(x, y, country) {
			return name, nil
		}
	}

	return "", errors.New("no country clicked")

}
