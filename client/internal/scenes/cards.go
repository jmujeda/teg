package scenes

import (
	img "image"
	"image/color"

	"github.com/ebitenui/ebitenui/image"
	"github.com/ebitenui/ebitenui/input"
	"github.com/ebitenui/ebitenui/widget"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"gitlab.com/jmujeda/teg/client/internal/scenes/internal"
	"gitlab.com/jmujeda/teg/teg"
)

const CARD_SPACING int = 10

type windowAdder interface {
	AddWindow(window *widget.Window) widget.RemoveWindowFunc
}

type cardTradeController interface {
	add(card teg.Card)
	canTrade() bool
	remove(card teg.Card)
	isCardSelected(card teg.Card) bool
}

type cards struct {
	container    *widget.Container
	logos        map[teg.Logo]*ebiten.Image
	cards        teg.Deck
	state        *teg.RoomMessage
	config       *teg.Config
	windowHeight int
	controller   cardTradeController
	showCardsFn  func(name customWidgetName)
	window       *widget.Window
	showWindow   bool
}

func buildCards(controller cardTradeController, config *teg.Config, state *teg.RoomMessage, windowHeight int, displayFn func(name customWidgetName)) *cards {
	logos := map[teg.Logo]*ebiten.Image{}
	container := createCardsContainer()
	cannon, _, _ := ebitenutil.NewImageFromFile(config.ClientConfig.Card.Cannon)
	ship, _, _ := ebitenutil.NewImageFromFile(config.ClientConfig.Card.Ship)
	globe, _, _ := ebitenutil.NewImageFromFile(config.ClientConfig.Card.Globe)
	logos[teg.Cannon] = cannon
	logos[teg.Ship] = ship
	logos[teg.Globe] = globe

	cards := &cards{
		container:    container,
		cards:        state.Game.Players[state.Game.PlayerColor].Cards,
		state:        state,
		logos:        logos,
		config:       config,
		windowHeight: windowHeight,
		controller:   controller,
		showCardsFn:  displayFn,
	}
	return cards
}

func createCardsContainer() *widget.Container {
	container := widget.NewContainer(
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(
				widget.AnchorLayoutData{
					HorizontalPosition: widget.AnchorLayoutPositionCenter,
					VerticalPosition:   widget.AnchorLayoutPositionCenter,
				},
			),
		),
		//widget.ContainerOpts.BackgroundImage(image.NewNineSliceColor(color.White)),
		widget.ContainerOpts.Layout(widget.NewRowLayout(
			widget.RowLayoutOpts.Padding(widget.NewInsetsSimple(20)),
			widget.RowLayoutOpts.Spacing(CARD_SPACING),
		)),
	)
	return container
}

func (c *cards) getContainer() *widget.Container {
	return c.container
}

func (c *cards) update() {
}

func (c *cards) onBoardChange() {
	previousCardsCount := len(c.cards.Cards)
	currentCards := c.state.Game.Players[c.state.Game.PlayerColor].Cards.Cards
	if previousCardsCount != len(currentCards) {
		c.container.RemoveChildren()
		c.cards = c.state.Game.Players[c.state.Game.PlayerColor].Cards
		c.addCards()
		if previousCardsCount < len(c.state.Game.Players[c.state.Game.PlayerColor].Cards.Cards) {
			//drew card or stole from defeated player
			c.showNewCardsWindow(currentCards[previousCardsCount:])
		}
	}
}

func (c *cards) showNewCardsWindow(cards []teg.Card) {
	c.showWindow = true

	width := c.config.ClientConfig.Card.Width*len(cards) + CARD_SPACING*(len(cards)-1)
	height := c.config.ClientConfig.Card.Height
	c.window = widget.NewWindow(
		widget.WindowOpts.Modal(),
		widget.WindowOpts.Contents(c.createWindowContent(cards)),
		//widget.WindowOpts.TitleBar(titleBar, 30),
		//widget.WindowOpts.Draggable(),
		//widget.WindowOpts.Resizeable(),
		widget.WindowOpts.MinSize(width, height),
		//widget.WindowOpts.MaxSize(700, 400),
		widget.WindowOpts.CloseMode(widget.CLICK),
	)
	windowSize := input.GetWindowSize()
	r := img.Rect(0, 0, width, height)
	r = r.Add(img.Point{windowSize.X/2 - (width / 2), windowSize.Y/2 - (height / 2)})
	c.window.SetLocation(r)
	//c.showCardsFn(c.name)

}

func (c *cards) createWindowContent(cards []teg.Card) *widget.Container {
	cardContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewRowLayout(
			widget.RowLayoutOpts.Direction(widget.DirectionHorizontal),
			widget.RowLayoutOpts.Spacing(CARD_SPACING),
		)),
	)
	for _, card := range cards {
		cardHandler := func(args *widget.ButtonClickedEventArgs) {}
		button := c.CreateCardButton(card.Country, card.Logo, cardHandler)
		cardContainer.AddChild(button)
	}
	return cardContainer
}

func (c *cards) addCards() {
	backgroundImg, _, _ := ebitenutil.NewImageFromFile("assets/icon_checkmark.png")
	//backgroundImg, _, _ := ebitenutil.NewImageFromFile("assets/panel-border-027.png")

	background9s := image.NewNineSlice(backgroundImg, [3]int{0, 40, 0}, [3]int{0, 36, 0})

	for _, card := range c.cards.Cards {

		cardContainer := widget.NewContainer(
			widget.ContainerOpts.Layout(widget.NewAnchorLayout()),
		)

		checkContainer := widget.NewContainer(
			widget.ContainerOpts.WidgetOpts(
				widget.WidgetOpts.MinSize(40, 36),
			),
			widget.ContainerOpts.BackgroundImage(background9s),
			widget.ContainerOpts.Layout(widget.NewRowLayout()),
			widget.ContainerOpts.WidgetOpts(
				widget.WidgetOpts.LayoutData(widget.AnchorLayoutData{
					HorizontalPosition: widget.AnchorLayoutPositionEnd,
					VerticalPosition:   widget.AnchorLayoutPositionStart,
				}),
			),
		)
		checkContainer.GetWidget().Visibility = widget.Visibility_Hide

		cardHandler := func(args *widget.ButtonClickedEventArgs) {
			if c.controller.isCardSelected(card) {
				checkContainer.GetWidget().Visibility = widget.Visibility_Hide
				//c.state.Game.CardTrade.Remove(card)
				c.controller.remove(card)
				//b.tradeCardsButton.GetWidget().Disabled = true
			} else {
				checkContainer.GetWidget().Visibility = widget.Visibility_Show
				//c.state.Game.CardTrade.Add(card)
				c.controller.add(card)
			}
		}
		button := c.CreateCardButton(card.Country, card.Logo, cardHandler)
		cardContainer.AddChild(button)
		cardContainer.AddChild(checkContainer)
		c.container.AddChild(cardContainer)

	}
}

func (c *cards) CreateCardButton(country teg.CountryName, icon teg.Logo, handler widget.ButtonClickedHandlerFunc) *widget.Button {
	cardHeight := c.windowHeight/4 - 40
	face, _ := internal.LoadFont(float64(cardHeight / 10))

	img := c.logos[icon]
	buttonImage, _ := loadCardImage(img)

	//scaleFactor := (cardHeight) / c.config.Height
	//height := img.Bounds().Dy() - 200
	//width := img.Bounds().Dx() - 200
	height := c.config.ClientConfig.Card.Height
	width := c.config.ClientConfig.Card.Width
	button := widget.NewButton(
		widget.ButtonOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(widget.AnchorLayoutData{
				HorizontalPosition: widget.AnchorLayoutPositionStart,
				VerticalPosition:   widget.AnchorLayoutPositionStart,
			}),
		),
		// widget.ButtonOpts.WidgetOpts(widget.WidgetOpts.LayoutData(
		// 	widget.RowLayoutData{MaxHeight: cardHeight - 12, MaxWidth: (c.config.Width * scaleFactor) / 100}),
		// ),
		widget.ButtonOpts.Image(buttonImage),

		// // specify the button's text, the font face, and the color
		widget.ButtonOpts.Text(string(country), face, &widget.ButtonTextColor{
			Idle:     color.NRGBA{0x00, 0x00, 0x00, 0xff},
			Disabled: color.NRGBA{0xdf, 0xf4, 0xff, 0xff},
		}),

		// // specify that the button's text needs some padding for correct display
		widget.ButtonOpts.TextPadding(widget.Insets{
			Left:   0,
			Right:  0,
			Top:    height - 40,
			Bottom: 0,
		}),

		// add a handler that reacts to clicking the button
		widget.ButtonOpts.ClickedHandler(handler),
	)
	button.GetWidget().MinWidth = width
	button.GetWidget().MinHeight = height
	if button == nil {
		println("ERROR")
	}
	return button
}

func loadCardImage(img *ebiten.Image) (*widget.ButtonImage, error) {

	a := image.NewNineSlice(img, [3]int{0, img.Bounds().Dx(), 0}, [3]int{0, img.Bounds().Dy(), 0})
	//	a := image.NewNineSliceSimple(img, 50, img.Bounds().Dx())

	//idle := image.NewNineSlice(img, [3]int{20, 20, 20}, [3]int{40, 40, 40})

	return &widget.ButtonImage{
		Idle:     a,
		Hover:    a,
		Pressed:  a,
		Disabled: a,
	}, nil
}

func (c *cards) add(adder windowAdder) {

	if c.window != nil && c.showWindow {
		adder.AddWindow(c.window)
		c.showWindow = false
	}
}
