package scenes

import (
	"image/color"

	"github.com/ebitenui/ebitenui/image"
	"github.com/ebitenui/ebitenui/widget"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"gitlab.com/jmujeda/teg/client/internal/scenes/internal"
	"gitlab.com/jmujeda/teg/teg"
)

type objective struct {
	container *widget.Container
	state     *teg.RoomMessage
	current   string
	text      *widget.TextArea
}

func buildObjective(state *teg.RoomMessage, height int) *objective {
	backgroundImg, _, _ := ebitenutil.NewImageFromFile("assets/card-bkg-h2.png")

	face, _ := internal.LoadFont(float64(height / 14))

	container := widget.NewContainer(
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(
				widget.AnchorLayoutData{
					HorizontalPosition: widget.AnchorLayoutPositionCenter,
					VerticalPosition:   widget.AnchorLayoutPositionCenter,
				},
			),
		),
		widget.ContainerOpts.BackgroundImage(image.NewNineSlice(backgroundImg, [3]int{0, 1180, 0}, [3]int{0, 539, 0})),
		widget.ContainerOpts.Layout(widget.NewRowLayout(
			widget.RowLayoutOpts.Padding(widget.NewInsetsSimple(0)),
		)),
	)
	text := widget.NewTextArea(
		widget.TextAreaOpts.ContainerOpts(
			widget.ContainerOpts.Layout(widget.NewRowLayout(widget.RowLayoutOpts.Padding(widget.NewInsetsSimple(0)))),
			widget.ContainerOpts.WidgetOpts(
				widget.WidgetOpts.MinSize(0, height-40),
				widget.WidgetOpts.LayoutData(
					widget.RowLayoutData{
						MaxWidth:  400,
						MaxHeight: 200,
					},
				),
			),
		),
		widget.TextAreaOpts.FontFace(face),
		widget.TextAreaOpts.FontColor(color.Black),
		widget.TextAreaOpts.TextPadding(widget.NewInsetsSimple(40)),
		widget.TextAreaOpts.ScrollContainerOpts(
			widget.ScrollContainerOpts.Image(&widget.ScrollContainerImage{
				Idle: image.NewNineSliceColor(color.NRGBA{100, 100, 100, 0}),
				Mask: image.NewNineSliceColor(color.NRGBA{100, 100, 100, 255}),
			}),
		), widget.TextAreaOpts.SliderOpts(widget.SliderOpts.Direction(widget.DirectionHorizontal)),
		//This sets the images to use for the sliders
		widget.TextAreaOpts.SliderOpts(
			widget.SliderOpts.Images(
				// Set the track images
				&widget.SliderTrackImage{
					Idle:  image.NewNineSliceColor(color.NRGBA{200, 200, 200, 255}),
					Hover: image.NewNineSliceColor(color.NRGBA{200, 200, 200, 255}),
				},
				// Set the handle images
				&widget.ButtonImage{
					Idle:    image.NewNineSliceColor(color.NRGBA{255, 100, 100, 255}),
					Hover:   image.NewNineSliceColor(color.NRGBA{255, 100, 100, 255}),
					Pressed: image.NewNineSliceColor(color.NRGBA{255, 100, 100, 255}),
				},
			),
		),
	)
	//text.AppendText(state.Game.Players[state.Game.PlayerColor].Objective)
	text.AppendText("ocupar 2 países de oceanía, 2 países de áfrica, 2 países de américa del sur, 3 países de europa, 4 de américa del norte y 3 de asia.")

	container.AddChild(text)
	// container.GetWidget().MinHeight = 50
	//container.GetWidget().MinWidth = 5000
	objective := &objective{
		container: container,
		state:     state,
		current:   state.Game.Players[state.Game.PlayerColor].Objective,
		text:      text,
	}

	return objective
}

func (o *objective) getContainer() *widget.Container {
	return o.container
}

func (o *objective) update() {

}

func (o *objective) onBoardChange() {
	if o.current != o.state.Game.Players[o.state.Game.PlayerColor].Objective {
		o.current = o.state.Game.Players[o.state.Game.PlayerColor].Objective
	}
}
