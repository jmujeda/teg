package scenes

import (
	"github.com/hajimehoshi/ebiten/v2"
	teg "gitlab.com/jmujeda/teg/teg"
)

type SceneManager struct {
	Current Scene
	next    Scene
	Config  *teg.Config
}

func (s *SceneManager) SetNext(scene Scene) {
	s.next = scene
	s.next.SetConfig(s.Config)
}

func (s *SceneManager) Update() {
	s.Current.Update()
}

func (s *SceneManager) Draw(screen *ebiten.Image) {
	s.Current.Draw(screen)
}

func (s *SceneManager) SelectScene() {
	if s.next != nil {
		s.Current = s.next
		s.next = nil
	}
}

type Scene interface {
	Update()
	Draw(screen *ebiten.Image)
	SetConfig(config *teg.Config)
}
