package scenes

import (
	"fmt"
	"image/color"
	"strings"
	"time"

	"github.com/ebitenui/ebitenui"
	"github.com/ebitenui/ebitenui/image"
	"github.com/ebitenui/ebitenui/widget"
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
	"gitlab.com/jmujeda/teg/client/internal/scenes/internal"
	"gitlab.com/jmujeda/teg/teg"
)

type LoadGame struct {
	init                bool
	ebitenUI            *ebitenui.UI
	config              *teg.Config
	State               *teg.UserGamesMessage
	gameList            *widget.List
	avatars             map[teg.Color]*image.NineSlice
	playerListContainer *widget.Container
	fontFace            text.Face
	LoadGameHandler     func(id teg.GameId)
	GoBackHandler       func()
	selectedGame        teg.GameId
}

func (l *LoadGame) SetConfig(config *teg.Config) {
	l.config = config
}

func (l *LoadGame) Update() {
	if !l.init {
		l.initialize()
	}
	if l.State.Updated && len(l.State.UserGames) > 0 {
		l.loadUserGames()
		l.State.Updated = false
	}
	l.ebitenUI.Update()
}

func (l *LoadGame) loadUserGames() {
	l.gameList.SetEntries(l.getListEntries())
}

func (l *LoadGame) getListEntries() []any {
	entries := make([]any, 0, len(l.State.UserGames))

	for _, game := range l.State.UserGames {
		entries = append(entries, internal.ListWidgetItem{Id: string(game.Id), Label: fmt.Sprintf("%s | ID: %s", formatDate(game.Date), shortId(game.Id))})
	}
	return entries
}

func shortId(id teg.GameId) string {
	return strings.Split(string(id), "-")[0]
}

func formatDate(date time.Time) string {
	return fmt.Sprintf("%d/%d/%d  %d:%d", date.Day(), date.Month(), date.Year(), date.Hour(), date.Minute())
}

func (l *LoadGame) Draw(screen *ebiten.Image) {
	l.ebitenUI.Draw(screen)
}

func (l *LoadGame) loadAvatars() {
	l.avatars = map[teg.Color]*image.NineSlice{}
	l.avatars[teg.Black] = getNinceSlice(l.config.ClientConfig.BoardStats.Black)
	l.avatars[teg.Yellow] = getNinceSlice(l.config.ClientConfig.BoardStats.Yellow)
	l.avatars[teg.Blue] = getNinceSlice(l.config.ClientConfig.BoardStats.Blue)
	l.avatars[teg.Green] = getNinceSlice(l.config.ClientConfig.BoardStats.Green)
	l.avatars[teg.Red] = getNinceSlice(l.config.ClientConfig.BoardStats.Red)
	l.avatars[teg.Magenta] = getNinceSlice(l.config.ClientConfig.BoardStats.Magenta)
}

func getNinceSlice(path string) *image.NineSlice {
	img, _, err := ebitenutil.NewImageFromFile(path)
	if err != nil {
		panic("Failed to load image from " + string(path))
	}
	return image.NewNineSlice(img, [3]int{0, 64, 0}, [3]int{0, 64, 0})
}

func (l *LoadGame) initialize() {
	height := l.config.ClientConfig.Window.Height / 2
	width := l.config.ClientConfig.Window.Width / 2

	l.fontFace, _ = internal.LoadFont(float64(l.config.ClientConfig.BoardStats.FontSize + 2))
	onSelectItemCommand := func(args *widget.ListEntrySelectedEventArgs) {
		l.updatePlayerList(args.Entry.(internal.ListWidgetItem))
	}
	l.gameList = internal.GetListWidget(onSelectItemCommand)
	l.loadAvatars()
	img, _, _ := ebitenutil.NewImageFromFile("assets/background_merged.png")
	a := image.NewNineSliceSimple(img, img.Bounds().Dx(), img.Bounds().Dx())

	leftColumnContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewRowLayout()),
	)

	centeredContainer := widget.NewContainer(
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(widget.RowLayoutData{Stretch: true}),
		),
		widget.ContainerOpts.Layout(
			widget.NewRowLayout(
				widget.RowLayoutOpts.Direction(widget.DirectionVertical),
				widget.RowLayoutOpts.Spacing(0),
			),
		),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(
				widget.AnchorLayoutData{
					HorizontalPosition: widget.AnchorLayoutPositionCenter,
					VerticalPosition:   widget.AnchorLayoutPositionCenter,
				},
			),
			widget.WidgetOpts.MinSize(width, height),
		),
	)
	topContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(
			widget.NewGridLayout(
				widget.GridLayoutOpts.Columns(2),
				widget.GridLayoutOpts.Spacing(40, 0),
				widget.GridLayoutOpts.Stretch([]bool{false, true}, []bool{false, false}),
			),
		),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(widget.RowLayoutData{Stretch: true}),
		),
	)

	bottomContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewAnchorLayout(
			widget.AnchorLayoutOpts.Padding(widget.NewInsetsSimple(40)),
		)),

		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(widget.RowLayoutData{Stretch: true}),
		),
	)

	rightColumnContainer := widget.NewContainer(
		widget.ContainerOpts.BackgroundImage(image.NewNineSliceColor(color.NRGBA{255, 255, 255, 50})),
		widget.ContainerOpts.Layout(
			widget.NewAnchorLayout(widget.AnchorLayoutOpts.Padding(widget.NewInsetsSimple(5))),
		),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(widget.RowLayoutData{Stretch: true}),
		),
	)

	l.playerListContainer = widget.NewContainer(
		widget.ContainerOpts.Layout(
			widget.NewGridLayout(
				widget.GridLayoutOpts.Spacing(5, 5),
				widget.GridLayoutOpts.Columns(2),
				widget.GridLayoutOpts.Padding(widget.NewInsetsSimple(10)),
				widget.GridLayoutOpts.Stretch([]bool{false, true}, []bool{false, false, false, false, false, false}),
			),
		),
	)
	buttonListContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(
			widget.NewRowLayout(
				widget.RowLayoutOpts.Direction(widget.DirectionVertical),
			),
		),
	)

	buttonsContainer := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewRowLayout(
			widget.RowLayoutOpts.Direction(widget.DirectionHorizontal),
			widget.RowLayoutOpts.Spacing(20),
		)),
		widget.ContainerOpts.WidgetOpts(
			widget.WidgetOpts.LayoutData(
				widget.AnchorLayoutData{
					VerticalPosition:   widget.AnchorLayoutPositionCenter,
					HorizontalPosition: widget.AnchorLayoutPositionCenter,
				},
			),
		),
	)

	loadGameHandler := func(args *widget.ButtonClickedEventArgs) {
		l.LoadGameHandler(l.selectedGame)
	}

	goBackHandler := func(args *widget.ButtonClickedEventArgs) {
		l.GoBackHandler()
	}

	loadGameButton := internal.CreateButton("Cargar", loadGameHandler, l.config.ClientConfig.Button)

	goBackButton := internal.CreateButton("Volver", goBackHandler, l.config.ClientConfig.Button)

	buttonsContainer.AddChild(goBackButton)
	buttonsContainer.AddChild(loadGameButton)

	rootContainer := widget.NewContainer(
		// the container will use a plain color as its background

		widget.ContainerOpts.BackgroundImage(a),
		//the container will use an anchor layout to layout its single child widget
		widget.ContainerOpts.Layout(widget.NewAnchorLayout()),
	)
	leftColumnContainer.AddChild(l.gameList)

	topContainer.AddChild(leftColumnContainer)
	topContainer.AddChild(rightColumnContainer)
	bottomContainer.AddChild(buttonsContainer)
	centeredContainer.AddChild(topContainer)
	centeredContainer.AddChild(bottomContainer)
	rightColumnContainer.AddChild(l.playerListContainer)
	rightColumnContainer.AddChild(buttonListContainer)
	rootContainer.AddChild(centeredContainer)
	l.ebitenUI = &ebitenui.UI{
		Container: rootContainer,
	}
	l.init = true

}

func (l *LoadGame) updatePlayerList(args internal.ListWidgetItem) {
	l.selectedGame = teg.GameId(args.Id)
	l.playerListContainer.RemoveChildren()

	game, ok := l.State.UserGames[teg.GameId(args.Id)]
	fmt.Println(game, ok)

	if ok {
		for color, player := range game.Players {
			l.playerListContainer.AddChild(l.getAvatarColumn(color))
			l.playerListContainer.AddChild(l.getNameColumn(player))
		}
	}
}

func (l *LoadGame) getAvatarColumn(color teg.Color) *widget.Container {
	wrapper := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewRowLayout()),
	)
	img := widget.NewContainer(
		widget.ContainerOpts.BackgroundImage(
			l.avatars[color],
		),
	)
	img.GetWidget().MinHeight = 25
	img.GetWidget().MinWidth = 25
	wrapper.AddChild(img)
	return wrapper
}

func (l *LoadGame) getNameColumn(player string) *widget.Container {
	container := widget.NewContainer(
		widget.ContainerOpts.Layout(widget.NewRowLayout()),
	)

	text := widget.NewLabel(
		widget.LabelOpts.Text(player, l.fontFace, &widget.LabelColor{Idle: color.Black}),
		widget.LabelOpts.TextOpts(widget.TextOpts.Insets(widget.NewInsetsSimple(4))),
	)
	container.AddChild(text)
	return container
}
