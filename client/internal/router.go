package internal

import (
	"context"

	"gitlab.com/jmujeda/teg/teg"
)

type Command func(ctx context.Context, news teg.ServerMessage, app *App)

type Router struct {
	commands map[teg.Action]Command
}

func (r *Router) GetCommand(action teg.Action) Command {
	return r.commands[action]
}

func NewClientRouter() Router {
	return Router{
		commands: map[teg.Action]Command{
			teg.JoinRoom:     updateLobbyCommand,
			teg.LoadGame:     initBoardCommand,
			teg.CreateRoom:   updateLobbyCommand,
			teg.LeaveRoom:    leaveLobbyCommand,
			teg.UpdateRoom:   updateLobbyCommand,
			teg.Pong:         pongReceivedCommand,
			teg.InitBoard:    initBoardCommand,
			teg.StartGame:    initBoardCommand,
			teg.UpdateBoard:  updateBoardCommand,
			teg.LoginAccount: loginAccountCommand,
			teg.ListGames:    listGamesCommand,
		},
	}
}
