package internal

import (
	"context"

	"github.com/codecat/go-libs/log"
	"github.com/coder/websocket"
	"github.com/coder/websocket/wsjson"
	"gitlab.com/jmujeda/teg/teg"
)

type Connection struct {
	Conn *websocket.Conn
	auth string
}

func (c Connection) send(ctx context.Context, msg teg.ClientMessage) error {
	msg.News.Token = c.auth
	err := wsjson.Write(ctx, c.Conn, msg)
	if err != nil {
		log.Error("error marshaling data: %s", err.Error())
	}

	return err
}
