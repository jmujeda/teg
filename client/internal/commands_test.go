package internal

import (
	"context"
	"testing"

	"github.com/google/go-cmp/cmp"
	teg "gitlab.com/jmujeda/teg/teg"
)

func Test_updateLobbyCommand(t *testing.T) {
	app := &App{}
	news := teg.ServerMessage{Room: teg.RoomMessage{Name: "myRoom"}}
	updateLobbyCommand(context.Background(), news, app)
	if !cmp.Equal(app.Room, news.Room) {
		t.Error("test failed")
	}
}
