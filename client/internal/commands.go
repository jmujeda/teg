package internal

import (
	"context"
	"fmt"

	"github.com/codecat/go-libs/log"
	"gitlab.com/jmujeda/teg/client/internal/scenes"
	"gitlab.com/jmujeda/teg/teg"
)

type messager interface {
	send(ctx context.Context, msg teg.ClientMessage) error
}

func updateLobbyCommand(ctx context.Context, news teg.ServerMessage, app *App) {
	if app.Room.Name == "" {
		enterRoomCommand(ctx, news, app)
	} else {
		app.Room = news.Room
	}
}

func leaveLobbyCommand(ctx context.Context, news teg.ServerMessage, app *App) {
	if news.Error == "" {
		app.Room = news.Room
		app.SceneManager.SetNext(&scenes.Menu{
			NewGameHandler:  GetNewGameHandler(ctx, app),
			LoadGameHandler: GetLoadGameMenuHandler(ctx, app),
			JoinGameHandler: GetJoinGameHandler(ctx, app),
		})
	}
}

func pongReceivedCommand(ctx context.Context, news teg.ServerMessage, app *App) {
	log.Info("pong received")
}

func enterRoomCommand(ctx context.Context, news teg.ServerMessage, app *App) {
	app.Room = news.Room
	app.SceneManager.SetNext(&scenes.Room{
		LeaveLobbyHandler: GetLeaveLobbyHandler(ctx, app.SceneManager, app.Connection),
		State:             &app.Room,
		StartGameHandler:  GetStartGameHandler(ctx, app.Connection),
	})
	log.Info("joining room %s", app.Room.Name)
	log.Info("players list: %s", app.Room.GetRoommatesList())
}

func initBoardCommand(ctx context.Context, news teg.ServerMessage, app *App) {
	app.Room = news.Room
	app.Room.Game.BoardChanged = true
	model := scenes.NewModel(&app.Room)
	controller := scenes.NewBoardController(model, GetEndTurnHandler(app.ctx, app.Connection))
	app.SceneManager.SetNext(&scenes.Board{
		Model:      model,
		Controller: controller,
	})

	app.config.Logger.Debug(news.Room)
}

func updateBoardCommand(ctx context.Context, news teg.ServerMessage, app *App) {
	app.Room = news.Room
	if news.Error != "" {
		fmt.Println(news.Error)
	} else {
		app.Room.Game.BoardChanged = true
	}
	app.config.Logger.Debug(news.Room)
}

func loginAccountCommand(ctx context.Context, news teg.ServerMessage, app *App) {
	if news.Error == "" {
		app.setAuth(news.Token)
		app.SceneManager.SetNext(&scenes.Menu{
			NewGameHandler:  GetNewGameHandler(ctx, app),
			LoadGameHandler: GetLoadGameMenuHandler(ctx, app),
			JoinGameHandler: GetJoinGameHandler(ctx, app),
		})
	}
}

func listGamesCommand(ctx context.Context, news teg.ServerMessage, app *App) {
	app.UserGames = news.UserGames
}

func GetLoginUserHandler(ctx context.Context, sceneManager SceneUpdater, messager messager) func(id string, password string) {
	return func(id string, password string) {
		println("send login request")
		messager.send(ctx, teg.ClientMessage{
			Action: teg.LoginAccount,
			News:   teg.ServerMessage{StartMenuData: teg.AccountData{Id: id, Password: password}},
		})
	}

}

func GetNewGameHandler(ctx context.Context, app *App) func() {
	return func() {
		println("create button clicked")
		app.SceneManager.SetNext(&scenes.Lobby{
			Create:            true,
			CreateGameHandler: GetCreateGameHandler(ctx, app),
			EnterGameHandler:  GetEnterGameHandler(ctx, app),
			BackToMenuHandler: GetBackToMenuHandler(ctx, app),
		})
	}
}

func GetJoinGameHandler(ctx context.Context, app *App) func() {
	return func() {
		println("join button clicked")
		app.SceneManager.SetNext(&scenes.Lobby{
			Create:            false,
			CreateGameHandler: GetCreateGameHandler(ctx, app),
			EnterGameHandler:  GetEnterGameHandler(ctx, app),
			BackToMenuHandler: GetBackToMenuHandler(ctx, app),
		})
	}
}

func GetLoadGameMenuHandler(ctx context.Context, app *App) func() {
	return func() {
		app.Connection.send(ctx, teg.ClientMessage{
			Action: teg.ListGames,
			News:   teg.ServerMessage{},
		})
		app.SceneManager.SetNext(&scenes.LoadGame{
			State:           &app.UserGames,
			LoadGameHandler: GetLoadGameHandler(ctx, app),
			GoBackHandler:   GetBackToMenuHandler(ctx, app),
		})
		//sceneUpdater.SetNext(&scenes.Workshop{})

		// println("load button clicked")
	}
}

func GetLoadGameHandler(ctx context.Context, app *App) func(id teg.GameId) {
	return func(id teg.GameId) {
		app.Connection.send(ctx, teg.ClientMessage{
			Action: teg.LoadGame,
			News:   teg.ServerMessage{StartMenuData: teg.AccountData{Id: string(id)}},
		})
	}
}

func GetCreateGameHandler(ctx context.Context, app *App) func(roomName string, password string) {
	return func(roomName string, password string) {
		println("send create room message")
		app.Connection.send(ctx, teg.ClientMessage{
			Action: teg.CreateRoom,
			News:   teg.ServerMessage{StartMenuData: teg.AccountData{Id: roomName, Password: password}},
		})
	}
}

func GetEnterGameHandler(ctx context.Context, app *App) func(roomName string, password string) {
	return func(roomName string, password string) {
		println("send join room message")
		app.Connection.send(ctx, teg.ClientMessage{
			Action: teg.JoinRoom,
			News:   teg.ServerMessage{StartMenuData: teg.AccountData{Id: roomName, Password: password}},
		})
	}
}

func GetBackToMenuHandler(ctx context.Context, app *App) func() {
	return func() {
		app.SceneManager.SetNext(&scenes.Menu{
			NewGameHandler:  GetNewGameHandler(ctx, app),
			LoadGameHandler: GetLoadGameMenuHandler(ctx, app),
			JoinGameHandler: GetJoinGameHandler(ctx, app),
		})
	}
}

func GetLeaveLobbyHandler(ctx context.Context, sceneUpdater SceneUpdater, messager messager) func() {
	return func() {
		messager.send(ctx, teg.ClientMessage{
			Action: teg.LeaveRoom,
		})
	}
}

func GetStartGameHandler(ctx context.Context, messager messager) func() {
	return func() {
		messager.send(ctx, teg.ClientMessage{
			Action: teg.StartGame,
		})
	}
}

func GetEndTurnHandler(ctx context.Context, messager messager) func(action teg.Action, roomMessage teg.RoomMessage) {
	return func(action teg.Action, roomMessage teg.RoomMessage) {
		messager.send(ctx, teg.ClientMessage{
			//Action: roomMessage.Game.Turn.Action,
			Action: action,
			News:   teg.ServerMessage{Room: roomMessage},
		})
	}
}

func GetAttackIntentHandler(ctx context.Context, messager messager) func(roomMessage teg.RoomMessage) {
	return func(roomMessage teg.RoomMessage) {
		messager.send(ctx, teg.ClientMessage{
			Action: roomMessage.Game.Turn.Action,
			News:   teg.ServerMessage{Room: roomMessage},
		})
	}
}

func GetMoveArmyHandler(ctx context.Context, messager messager) func(roomMessage teg.RoomMessage) {
	return func(roomMessage teg.RoomMessage) {
		messager.send(ctx, teg.ClientMessage{
			Action: roomMessage.Game.Turn.Action,
			News:   teg.ServerMessage{Room: roomMessage},
		})
	}
}
