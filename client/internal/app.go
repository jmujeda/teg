package internal

import (
	"context"
	"fmt"
	_ "image/png"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"gitlab.com/jmujeda/teg/client/internal/scenes"
	teg "gitlab.com/jmujeda/teg/teg"
)

type SceneUpdater interface {
	Update()
	SelectScene()
	SetNext(scene scenes.Scene)
	Draw(screen *ebiten.Image)
}

// App implements ebiten.App interface.
type App struct {
	SceneManager SceneUpdater
	Connection   Connection
	Room         teg.RoomMessage
	UserGames    teg.UserGamesMessage
	router       Router
	Name         string
	ctx          context.Context
	config       *teg.Config
}

func NewApp(ctx context.Context, connection Connection, config *teg.Config) *App {
	sceneManager := &scenes.SceneManager{Config: config}

	sceneManager.SetNext(&scenes.LoginScreen{
		LoginUserHandler: GetLoginUserHandler(ctx, sceneManager, connection),
	})

	game := &App{
		SceneManager: sceneManager,
		Connection:   connection,
		router:       NewClientRouter(),
		ctx:          ctx,
		config:       config,
	}

	return game
}

func (g *App) setAuth(token string) {
	g.Connection.auth = token
}

// Update proceeds the game state.
// Update is called every tick (1/60 [s] by default).
func (g *App) Update() error {
	g.SceneManager.SelectScene()
	g.SceneManager.Update()

	return nil
}

// Draw draws the game screen.
// Draw is called every frame (typically 1/60[s] for 60Hz display).
func (g *App) Draw(screen *ebiten.Image) {
	g.SceneManager.Draw(screen)
	msg := fmt.Sprintf("TPS: %0.2f\nFPS: %0.2f", ebiten.ActualTPS(), ebiten.ActualFPS())

	ebitenutil.DebugPrint(screen, msg)
}

// Layout takes the outside size (e.g., the window size) and returns the (logical) screen size.
// If you don't have to adjust the screen size with the outside size, just return a fixed size.
func (g *App) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	//return 320, 240
	return outsideWidth, outsideHeight
}
